from os import makedirs
from os.path import join
from pathlib import Path

import yaml


def getFirstValue(dictionary):
    return list(dictionary.values())[0]


def createTaggerTable(cdi_data):
    table = "| Tagger | Supported jet collections | MC efficiency maps |\n"
    table += "| ------ | ------------------------- | ------------------ |\n"
    for tagger, tagger_data in cdi_data["taggers"].items():
        jet_collections = ", ".join(tagger_data.keys())
        mc_efficiency_maps = ", ".join(getFirstValue(getFirstValue(tagger_data))["flavours"]["B"]["mc_had_ref"].keys())
        table += f"| {tagger} | {jet_collections} | {mc_efficiency_maps} |\n"
    table += "\n\n"
    return table


def createTaggerWPTable(tagger_data, prefix=""):
    table = prefix + "| Jet collection | Working point | Cut value | fc value | ftau value |\n"
    table += prefix + "| -------------- | ------------- | --------- | -------- | ---------- |\n"
    for jc, jc_data in tagger_data.items():
        for wp, wp_data in jc_data.items():
            try:
                cutvalue = f"{wp_data['cutvalue']:.4f}"
            except KeyError:
                cutvalue = ""
            try:
                fraction = f"{wp_data['fraction']:.3f}"
            except KeyError:
                fraction = ""
            try:
                ftau = f"{wp_data['fraction_tau']:.3f}"
            except KeyError:
                ftau = ""
            table += prefix + f"| {jc} | {wp} | {cutvalue} | {fraction} | {ftau} |\n"
    return table


def createTaggerInfo(cdi_data, config):
    output = "Tagger information\n\n"
    for tagger, tagger_data in cdi_data["taggers"].items():
        output += f"""
??? info "{tagger}"


    Supported jet collections:\n
"""
        output += "    - " + "\n    - ".join(tagger_data.keys())
        output += """\n
    Working points:\n
"""
        output += createTaggerWPTable(tagger_data, prefix="    ")

    return output


def loop_over_cdis(data, config, title):
    cdi_base_path = Path(config["cdi_basepath"])

    # loop over runs
    md = ""
    for run, cdis in data.items():
        md += f"## {run}\n\n"

        # loop over cdis
        for cdi, cdi_data in sorted(cdis.items(), reverse=True):
            md += f"### {cdi}\n\n"

            # add the full path
            full_path = cdi_base_path / run / cdi
            assert full_path.exists()
            md += f"```\n{full_path}\n```\n\n"


def createCDIMarkDown(data, config=None):
    cdi_base_path = Path(config["cdi_basepath"])
    md = "# CDI file information\n\n"

    # loop over runs
    for run, cdis in data.items():
        md += f"## {run}\n\n"

        # loop over cdis
        for cdi, cdi_data in sorted(cdis.items(), reverse=True):
            md += f"### {cdi}\n\n"

            # add the full path
            full_path = cdi_base_path / run / cdi
            assert full_path.exists()
            md += f"```\n{full_path}\n```\n\n"

            # add taggers
            md += createTaggerTable(cdi_data)
            md += createTaggerInfo(cdi_data, config)
            md += "\n\n"

    return md


def createSFPlotsMarkDown(data, config=None):
    cdi_base_path = Path(config["cdi_basepath"])
    md = "# CDI Scale-factor and Efficiency Map Plots\n\n"

    # loop over runs
    for run, cdis in data.items():
        md += f"## {run}\n\n"

        # loop over cdis
        for cdi, cdi_data in sorted(cdis.items(), reverse=True):
            md += f"### {cdi}\n\n"

            # add the full path
            full_path = cdi_base_path / run / cdi
            assert full_path.exists()
            md += f"```\n{full_path}\n```\n\n"

            # add taggers
            md += "**Available taggers**\n"
            for tagger in cdi_data["taggers"]:
                md += f'??? example "{tagger}"\n'
                for jet_col in cdi_data["taggers"][tagger]:
                    md += f'    ??? example "{jet_col}"\n'
                    for wp in cdi_data["taggers"][tagger][jet_col]:
                        if wp == "Continuous":
                            continue
                        md += f'        ??? help "{wp}"\n'
                        set_of_dsids = ["default"]
                        for flavour in cdi_data["taggers"][tagger][jet_col][wp]["flavours"]:
                            for dsid in cdi_data["taggers"][tagger][jet_col][wp]["flavours"][flavour]["maplist"]:
                                if dsid.split("_")[0] not in set_of_dsids:
                                    set_of_dsids.append(dsid.split("_")[0])

                        # add plots
                        for dsid in set_of_dsids:
                            md += f'            === "{dsid}"\n'
                            for flavour in cdi_data["taggers"][tagger][jet_col][wp]["flavours"]:
                                suffix = ""
                                if dsid != "default":
                                    suffix += "_default_" + dsid + "_CalibrationBinned_Eff"
                                md_path = Path(f"plots/{run}/{cdi}/{tagger}/{jet_col}/{wp}/{flavour}{suffix}.png")
                                check_path = Path(
                                    f"output/plots/{run}/{cdi}/{tagger}/{jet_col}/{wp}/{flavour}{suffix}.png"
                                )
                                if not check_path.exists():
                                    print("skipping missing plot:", check_path)
                                    continue
                                printline = f"                 ![{flavour}_{dsid}]({md_path})" + '{width="300"}'
                                md += printline
                            md += "\n\n"
    return md


def main():
    with open(join("data", "config.yaml")) as file:
        config = yaml.load(file, Loader=yaml.FullLoader)
    with open(join("output", "yaml", "cdi_content.yaml")) as file:
        data = yaml.load(file, Loader=yaml.FullLoader)
    makedirs(join("output", "md"), exist_ok=True)

    # group data by run
    grouped_data = {}
    for cdi_key, value in data.items():
        split_cdi = cdi_key.split("/")
        assert len(split_cdi) == 2
        com_enery = split_cdi[0]
        cdi_name = split_cdi[1]
        if com_enery not in grouped_data:
            grouped_data[com_enery] = {}
        grouped_data[com_enery][cdi_name] = value
    data = grouped_data

    md_content = createCDIMarkDown(data, config)
    with open(join("output", "md", "cdi_overview.md"), "w") as file:
        file.write(md_content)

    sf_md_content = createSFPlotsMarkDown(data, config)
    with open(join("output", "md", "cdi_plot_overview.md"), "w") as file:
        file.write(sf_md_content)


if __name__ == "__main__":
    main()
