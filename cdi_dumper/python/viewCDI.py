import ROOT

ROOT.xAOD.Init().ignore()
ROOT.gROOT.ProcessLine(".L utils/btagtools.C")
ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(0)
from ROOT import TFile  # noqa
from ROOT import getbtaginfo  # noqa

taggers = ["DL1r"]
jet_cols = ["AntiKt4EMPFlowJets_BTagging201903"]
WPs = ["FixedCutBEff_60", "FixedCutBEff_70", "FixedCutBEff_77", "FixedCutBEff_85"]

pt_low = 20
pt_high = 300

mc_ids = ["410470", "700122"]

cdi_path = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2021-04-16_v1.root"  # noqa
cdi = TFile(cdi_path)


for tagger in taggers:
    for jet_col in jet_cols:
        for wp in WPs:
            for mc_id in mc_ids:
                effs = getbtaginfo(cdi_path, tagger, wp, jet_col, mc_id, pt_low, pt_high)

                print(tagger, jet_col, wp, mc_id)
                print("actual efficiencies")
                eff_light_low = round(effs[0], 4)
                eff_light_high = round(effs[1], 4)
                eff_c_low = round(effs[2], 4)
                eff_c_high = round(effs[3], 4)
                eff_b_low = round(effs[4], 4)
                eff_b_high = round(effs[5], 4)
                print("light", eff_light_low, eff_light_high)
                print("c", eff_c_low, eff_c_high)
                print("b", eff_b_low, eff_b_high)
