# Script to plot histograms of smoothed/unsmoothed data from the CDI file
# original author: hayden.alexander.smith@cern.ch marko.stamenkovic@cern.ch
# adapted by paul.philipp.gadow@cern.ch

# TODO: can steal for Continuous plots here: https://gitlab.cern.ch/zfeng/comb_CDI/-/blob/master/Validation/run_PContinuous_plots.py

from argparse import ArgumentParser
from os import makedirs
from os.path import basename, dirname, join

import ROOT
import yaml

ROOT.gROOT.SetBatch(ROOT.kTRUE)
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.xAOD.Init().ignore()

ROOT.gROOT.ProcessLine(".L utils/AtlasStyle.C")
ROOT.gROOT.ProcessLine(".L utils/AtlasLabels.C")
ROOT.SetAtlasStyle()


# Changes to CDI interface to be easily accessible in python
def GetKeyNames(self, path="", key=""):  # Method to retrieve name of available objects in CDI
    if path:
        self.cd(path)
    return [
        key.GetName() for key in ROOT.gDirectory.GetListOfKeys()
    ]  # Method to retreive object with specific path + name in CDI file


def GetKeyObject(self, path="", key=""):
    if path:
        self.cd(path)
    return ROOT.gDirectory.Get(key)


ROOT.TFile.GetKeyNames = GetKeyNames
ROOT.TFile.GetKeyObject = GetKeyObject


def getitem(self, key):  # Override CalibrationDataHistogramContainer in order to use it as object['name']
    return self.GetValue(key)


ROOT.TMap.__getitem__ = getitem
ROOT.Analysis.CalibrationDataHistogramContainer.__getitem__ = getitem
# End of changes to CDI interface


def getArgumentParser():
    parser = ArgumentParser()
    parser.add_argument("--cdi", default=None, nargs="*", help="Path to smoothed CDI file")
    parser.add_argument("--cdi_raw", default=None, nargs="*", help="Path to raw CDI file")
    parser.add_argument("--do_maps", action="store_true", help="Dump efficiency map plots.")
    return parser


def makeEfficiencyPlot(f_cdi, f_cdi_raw, path, output_path, tagger, jet, wp, flavour):
    c = ROOT.TCanvas()
    # Get TH2 objects from smoothed CDI file
    container = f_cdi.GetKeyObject(path, "default_SF")
    sf = container[
        "result"
    ]  # Contains the SF TH2 binned in pt - eta (need to project it one 1D to get SFs plots in 0 < abs(eta) < 2.5)
    tot_sys = container["systematics"]  # Contains total systematic uncertainty - TH2 binned in pt - eta
    container["extrapolation"]  # Contains few bins of the extrapolation

    # Get TH2 objects from unsmoothed CDI (data)
    plot_raw_CDI = True
    try:
        container_1 = f_cdi_raw.GetKeyObject(path, "default_SF")
        sf_1 = container_1["result"]
        tot_sys_1 = container_1["systematics"]
        # extrap_1 = container_1['extrapolation']
    except (AttributeError, TypeError):
        plot_raw_CDI = False

    # Get all smoothed histograms (central, sys_up, sys_down)
    h = sf.ProjectionY("smoothed")
    h_sys = tot_sys.ProjectionY("systematics")
    h_sys_up = h.Clone("h_sys_up")
    h_sys_down = h.Clone("h_sys_down")
    h_sys_up.Add(h_sys)
    h_sys_down.Add(h_sys, -1)

    # Get all unsmoothed histograms (data) (central point, sys_up, sys_down)
    if plot_raw_CDI:
        h_1 = sf_1.ProjectionY("unsmoothed")
        h_sys_1 = tot_sys_1.ProjectionY("systematics_1")

        for i in range(1, h_1.GetNbinsX() + 1):
            stat_error = h_1.GetBinError(i)
            tot_error = ROOT.TMath.Sqrt(stat_error**2 + h_sys_1.GetBinContent(i) ** 2)
            h_1.SetBinError(i, tot_error)

    # Plotting options
    maxi = 1.2 * h_sys_up.GetMaximum()
    mini = 0.95 * h_sys_down.GetMinimum()

    h.GetYaxis().SetRangeUser(mini, maxi)
    h.GetYaxis().SetTitle("b-tagging efficiency SF")
    h.GetXaxis().SetTitle("jet p_{T} [GeV]")
    h.SetLineColor(ROOT.kGreen + 3)
    h.SetLineWidth(2)
    h.GetXaxis().SetTitleSize(0.05)
    h.GetXaxis().SetTitleOffset(1.35)
    h.GetXaxis().SetLabelSize(0.05)
    h.GetXaxis().SetLabelOffset(0.01)
    h.GetYaxis().SetLabelSize(0.05)
    h.GetYaxis().SetTitleOffset(1.35)
    h.GetYaxis().SetTitleSize(0.05)
    h.Draw()

    h_sys_up.SetLineColor(ROOT.kBlack)
    h_sys_up.SetFillColor(ROOT.kGreen + 2)
    h_sys_up.SetFillStyle(3001)
    h_sys_down.SetLineColor(ROOT.kBlack)
    h_sys_down.SetFillColorAlpha(0, 1)
    h_sys_down.SetFillStyle(3001)

    h_sys_up.Draw("same")
    h_sys_down.Draw("same")

    if plot_raw_CDI:
        h_1.SetMarkerStyle(ROOT.kFullCircle)
        h_1.SetLineColor(ROOT.kBlack)
        h_1.SetMarkerColor(ROOT.kBlack)
        h_1.Draw("E same")

    leg = ROOT.TLegend(0.7, 0.7, 0.9, 0.9)
    leg.AddEntry(h_sys_up, "Smoothed SF", "f")
    if plot_raw_CDI:
        leg.AddEntry(h_1, "Unsmoothed SF", "LEP")
    leg.SetBorderSize(1)
    leg.SetFillColor(0)
    leg.SetLineWidth(0)
    leg.SetTextSize(0.042)
    leg.Draw()

    ROOT.ATLASLabel(0.2, 0.88, "Internal")
    ROOT.myText(0.2, 0.82, 1, "#sqrt{s}= 13 TeV, 139 fb^{-1}")
    ROOT.myText(
        0.2,
        0.76,
        1,
        "%s, %s-jets, %s" % (tagger, flavour.lower(), wp.replace("FixedCutBEff_", "")) + "%" + " Single Cut OP",
    )
    ROOT.myText(0.2, 0.70, 1, jet)

    ROOT.gPad.RedrawAxis()

    makedirs(dirname(output_path), exist_ok=True)
    c.Print(output_path)


def makeEfficiencyMapPlot(f_cdi, path, output_path, tagger, jet, wp, flavour, dsid, effmap):
    obj = f"{dsid}/{effmap}"
    result = f_cdi.GetKeyObject(path, obj)["result"]
    c = ROOT.TCanvas()
    result.Draw("colz")
    c.SetRightMargin(0.2)
    c.Print(output_path)


def makeMCMCEfficiencyMapPlot(
    f_cdi,
    path,
    output_path,
    tagger,
    jet,
    wp,
    flavour,
    dsid,
    mcmc="default",
    mcmc_effmap="CalibrationBinned_Eff",
):
    obj_dsid = f"{dsid}_{mcmc_effmap}"
    obj_mcmc = f"{mcmc}_{mcmc_effmap}"
    result = f_cdi.GetKeyObject(path, obj_dsid)["result"]
    mcmc_result = f_cdi.GetKeyObject(path, obj_mcmc)["result"]
    mcmc_result.Divide(result)
    c = ROOT.TCanvas()
    c.SetRightMargin(0.2)
    mcmc_result.Draw("colz")
    c.Print(output_path)


def main():
    args = getArgumentParser().parse_args()
    with open(join("data", "config.yaml")) as file:
        config = yaml.load(file, Loader=yaml.FullLoader)
    with open(join("output", "yaml", "cdi_content.yaml")) as file:
        data = yaml.load(file, Loader=yaml.FullLoader)

    if args.cdi and args.cdi_raw:
        cdi_names = {basename(cdi): [cdi, raw_cdi] for cdi, raw_cdi in zip(args.cdi, args.cdi_raw)}
    elif args.cdi:
        cdi_names = {basename(cdi): [cdi, None] for cdi in args.cdi}
    else:
        cdi_names = {cdi: [cdi_data["cdi_path"], None] for cdi, cdi_data in data.items()}

    for cdi_name, cdi_paths in cdi_names.items():
        cdi_data = data[cdi_name]

        f_cdi = ROOT.TFile(cdi_paths[0], "READ")
        f_cdi_raw = ROOT.TFile(cdi_paths[1], "READ") if cdi_paths[1] else None

        # make efficiency plots and efficiency map plots
        dsids = config["mc_ids"].keys()
        effmap = "CalibrationBinned_Eff"
        mcmc = "default"

        for tagger, jet_collections in cdi_data["taggers"].items():
            for jet, wpoints in jet_collections.items():
                for wp, flavours in wpoints.items():
                    # only process fixed eff. working points defined in config file
                    if wp not in config["working_points"]:
                        print(f"{wp} is not in {cdi_name}/{tagger}/{jet}")
                        continue
                    for flavour in flavours["flavours"].keys():
                        # only process flavours defined in config file
                        if flavour not in config["flavours"]:
                            continue
                        # make efficiency plot
                        path = f"{tagger}/{jet}/{wp}/{flavour}/"
                        output_path = f"output/plots/{cdi_name}/{tagger}/{jet}/{wp}/{flavour}.png"
                        try:
                            makeEfficiencyPlot(
                                f_cdi,
                                f_cdi_raw,
                                path,
                                output_path,
                                tagger,
                                jet,
                                wp,
                                flavour,
                            )
                        except TypeError:
                            pass

                        # make 2D MC efficiency map
                        if not args.do_maps:
                            continue
                        for dsid in dsids:
                            output_path = f"output/plots/{cdi_name}/{tagger}/{jet}/{wp}/{flavour}_{dsid}_{effmap}.png"
                            try:
                                makeEfficiencyMapPlot(
                                    f_cdi,
                                    path,
                                    output_path,
                                    tagger,
                                    jet,
                                    wp,
                                    flavour,
                                    dsid,
                                    effmap,
                                )
                            except TypeError:
                                pass

                            if dsid != mcmc:
                                output_path = (
                                    f"output/plots/{cdi_name}/{tagger}/{jet}/{wp}/{flavour}_{mcmc}_{dsid}_{effmap}.png"
                                )
                                try:
                                    makeMCMCEfficiencyMapPlot(
                                        f_cdi,
                                        path,
                                        output_path,
                                        tagger,
                                        jet,
                                        wp,
                                        flavour,
                                        dsid,
                                        mcmc,
                                    )
                                except TypeError:
                                    pass


if __name__ == "__main__":
    main()
