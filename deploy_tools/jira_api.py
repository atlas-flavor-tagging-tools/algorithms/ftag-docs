import os

from jira import JIRA


def get_jira_client():
    JIRA_PAT = os.environ.get("JIRA_PAT")
    if not JIRA_PAT:
        raise Exception("Error: JIRA_PAT variable must be specified")
    return JIRA("https://its.cern.ch/jira", token_auth=JIRA_PAT)


def aqps_dashboard(which_dash):
    jira = get_jira_client()

    proj = 'project = "ATLAS Flavour Tagging"'
    comp = " AND component = QualificationTask"
    stat = ' AND status = "In Progress"'
    stat_closed = ' AND status = "Closed"'
    stat_open = ' AND status = "Open"'
    subgcal = " AND component = Calibrations AND component != Xbb"
    subgalgo = " AND component = Algorithms"

    if which_dash == "cal_aqps":
        myfilter = proj + comp + subgcal + stat
    elif which_dash == "cal_aqps_closed":
        myfilter = proj + comp + subgcal + stat_closed
    elif which_dash == "cal_aqps_open":
        myfilter = proj + comp + subgcal + stat_open
    elif which_dash == "algo_aqps":
        myfilter = proj + comp + subgalgo + stat
    elif which_dash == "algo_aqps_closed":
        myfilter = proj + comp + subgalgo + stat_closed
    elif which_dash == "algo_aqps_open":
        myfilter = proj + comp + subgalgo + stat_open

    dashboard = jira.search_issues(jql_str=myfilter)
    return dashboard


def jira_issue(ticket, subgroup):
    jira = get_jira_client()
    issue = jira.issue(ticket)
    if issue.fields.assignee:
        assignee = issue.fields.assignee.displayName
    else:
        assignee = "N/A"
    summary = issue.fields.summary
    # for the calibration group use the actual start field (customfield_13000)
    # this however stopped working
    # for the algo group use the start date (created)
    act_start = issue.fields.created
    cernjira = "https://its.cern.ch/jira/browse/"
    issue_url = cernjira + str(ticket)
    url = issue_url

    return assignee, summary, act_start, url
