from datetime import datetime
from pathlib import Path

import jira_api

TARGET_CALIB_MD = Path(__file__).parents[1] / "docs" / "calibrations" / "cal_aqps.md"
TARGET_ALGO_MD = Path(__file__).parents[1] / "docs" / "algorithms" / "aqps_and_tasks" / "algo_aqp.md"


def main():
    build_table("cal")
    build_table("algo")


def build_table(subgroup):
    # first getting the tickets list of the open APQs
    dashboard_aqps_progr = jira_api.aqps_dashboard(f"{subgroup}_aqps")
    dashboard_aqps_open = jira_api.aqps_dashboard(f"{subgroup}_aqps_open")
    out_progr_md = get_ongoing_aqps_table(dashboard_aqps_progr, subgroup)
    out_open_md = get_ongoing_aqps_table(dashboard_aqps_open, subgroup)

    # then getting the tickets list of the finished APQs
    dashboard_aqps_closed = jira_api.aqps_dashboard(f"{subgroup}_aqps_closed")
    out_closed_md = ""
    for ticket in dashboard_aqps_closed:
        assignee, summary, _, url = jira_api.jira_issue(ticket, subgroup)
        ticket_id = str(ticket)
        out_closed_md += f"| {assignee} | {summary} | [{ticket_id}]({url}) |\n"

    if subgroup == "cal":
        out_path = TARGET_CALIB_MD
    elif subgroup == "algo":
        out_path = TARGET_ALGO_MD

    with open(out_path, "a") as target_file:
        if subgroup == "cal":
            url_jira = "[FTAG Calibration AQPs JIRA dashboard](https://its.cern.ch/jira/secure/Dashboard.jspa?selectPageId=24703)\n"
            target_file.write(url_jira)
        target_file.write("## Ongoing AQPs\n")
        target_file.write("| Student | Summary | JIRA | Start date |\n")
        target_file.write("| ------- | ------- | ---- | ---------- |\n")
        target_file.write(out_progr_md)
        target_file.write("\n\n")
        target_file.write("## Starting AQPs\n")
        target_file.write("| Student | Summary | JIRA | Start date |\n")
        target_file.write("| ------- | ------- | ---- | ---------- |\n")
        target_file.write(out_open_md)
        target_file.write("\n\n")
        target_file.write("## Recently completed AQPs\n")
        target_file.write("| Student | Summary | JIRA |\n")
        target_file.write("| ------- | ------- | ---- |\n")
        target_file.write(out_closed_md)


def get_ongoing_aqps_table(dashboard_cal_aqps, subgroup):
    out_md = ""
    for ticket in dashboard_cal_aqps:
        assignee, summary, act_start, url = jira_api.jira_issue(ticket, subgroup)
        ticket_id = str(ticket)
        display_date = get_display_date(act_start)
        out_md += f"| {assignee} | {summary} | [{ticket_id}]({url}) | {display_date} |\n"
        starting_date = get_starting_date(act_start)
        today = datetime.today()
        delta_time = today - starting_date
        delta_time_days = delta_time.days
        progress_percentage = int(delta_time_days * 100 / 365)
        if progress_percentage > 100:
            progress_percentage = 100
        out_md += f'[={progress_percentage}% "{progress_percentage}%"]{{: .thin}}\n'
    return out_md


def get_starting_date(act_start):
    if act_start is None:
        return datetime.today()
    else:
        act_start = datetime.fromisoformat(act_start)
        return act_start.replace(tzinfo=None)


def get_display_date(act_start):
    if act_start is None:
        return "N/A"
    else:
        act_start = datetime.fromisoformat(act_start)
        act_date_str = str(act_start.month) + "-" + str(act_start.month) + "-" + str(act_start.year)
        return act_date_str


if __name__ == "__main__":
    main()
