import json
import os
import urllib.request
from itertools import groupby
from pathlib import Path

from mkdocs_md import Markdown

METADATA_URL = "https://umami-tagger-metadata.web.cern.ch/metadata/"
GROUP_DATA = Path("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/BTagging/")
TARGET_MD = Path(__file__).parent.parent / "docs" / "algorithms" / "taggers" / "tagger_metadata.md"

kfold_str = (
    "This is a k-fold model, the metadata below is for a single arbitrary fold "
    "(though the different folds should have the same metadata!). To run k-fold"
    " inference, you need to use the "
    "[`MultifoldGNN.h`](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/FlavorTagDiscriminants/MultifoldGNN.h) "  # noqa: E501
    "tool in Athena."
)


def get_table_data(data):
    kv_pairs = {}

    if data.get("onnx_model_version", "v0") == "v0":
        kv_pairs["Name"] = list(data["outputs"].keys())[0]
        kv_pairs["Outputs"] = dict(list(data["outputs"].values())[0])["labels"]
    elif data["onnx_model_version"] == "v1":
        m = data["metadata.yaml"]
        # todo, there is a better way to do this in future, but keep it for GN2v01
        kv_pairs["Name"] = m["timestamp"].split("_")[0]
        classes_key = "jet_classes" if "jet_classes" in m else "object_classes"
        kv_pairs["Outputs"] = list(m[classes_key].values())
    ckpt_path = data.get("ckpt_path", None)
    if ckpt_path:
        ckpt_path = Path(ckpt_path).name
    kv_pairs["Checkpoint"] = ckpt_path
    kv_pairs["Tracks"] = data["input_sequences"][0]["name"]
    kv_pairs["Jet Inputs"] = [d["name"] for d in data["inputs"][0]["variables"]]

    kv_pairs["Export Hash"] = str(data.get("salt_export_hash"))
    metadata = data.get("metadata.yaml", None)
    if metadata:
        kv_pairs["Salt Hash"] = str(metadata.get("git_hash", metadata.get("salt_hash")))
        kv_pairs["Timestamp"] = metadata["timestamp"]
        kv_pairs["Pytorch Version"] = metadata["torch_version"]
        kv_pairs["Lightning Version"] = metadata["lightning_version"]
        kv_pairs["Cuda Version"] = metadata["cuda_version"]
        kv_pairs["Batch size"] = metadata["batch_size"] * metadata["num_gpus"]
        kv_pairs["Max Epochs"] = data["config.yaml"]["trainer"]["max_epochs"]
        kv_pairs["Params"] = metadata["trainable_params"]
        kv_pairs["Num Train"] = metadata.get("num_jets_train") or metadata.get("num_train")
        kv_pairs["Num Val"] = metadata.get("num_jets_val") or metadata.get("num_val")

    return {"Key": list(kv_pairs.keys()), "Value": list(kv_pairs.values())}


def get_metadata_url(path):
    url = METADATA_URL + str(path.relative_to(GROUP_DATA)).replace(".onnx", ".json")
    print(">>> Fetching metadata from", url)
    with urllib.request.urlopen(str(url)) as file:
        data = json.load(file)
    return url, data


def get_section(paths: list[str]) -> Markdown:
    # if more than one path, get the longest common prefix
    path = Path(paths[0])
    kfold_path = None
    if len(paths) > 1:
        kfold_path = Path(os.path.commonprefix(paths) + "*.onnx")
        if "fold" not in str(path):
            print(f"Found what looks like a k-fold model, but no 'fold' in path: {kfold_path}")
            return ""

    url, data = get_metadata_url(path)

    md = Markdown()
    md += md.header(f"{path.relative_to(GROUP_DATA).parent}\n\n", level=4)
    if kfold_path:
        md += f"{kfold_str}\n"
        path = kfold_path
    md += "Full path:" + md.code(str(path))
    md += md.table(get_table_data(data))

    full_json = md.code(json.dumps(data, indent=2), language="json", line_numbers=True)
    md += md.admonition(
        "quote",
        "Full JSON Metadata " + md.link("[link]", url),
        full_json,
        expanded=False,
    )
    return md


def group_paths(paths: list) -> dict[str, list[str]]:
    trigger = [x for x in paths if "trig" in str(x)]
    xbb = [x for x in paths if "antikt10ufo" in str(x)]
    offline = [x for x in paths if x not in (trigger + xbb)]
    return {"Offline": offline, "Trigger": trigger, "Xbb": xbb}


def main() -> None:
    if not GROUP_DATA.exists():
        msg = f"Path {GROUP_DATA} does not exist."
        raise FileNotFoundError(msg)
    paths = list(GROUP_DATA.rglob("**/*.onnx"))
    md = ""
    for name, group in group_paths(paths).items():
        print("Processing group", name)

        # sort paths in this group and then group by parent directory
        group = sorted(group, reverse=True)
        group = [list(g) for _, g in groupby(group, lambda x: Path(x).parent)]

        md += f"### {name}\n\n"
        for paths in sorted(group, reverse=True):
            print("> Processing model", paths)
            md += get_section(paths)

    with TARGET_MD.open("a") as f:
        f.write(md)


if __name__ == "__main__":
    main()
