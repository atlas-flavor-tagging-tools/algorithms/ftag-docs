# group directory where tagger files are stored
export BASE_DIR=/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/BTagging/
export TARGET_DIR=/eos/home-u/umami/tagger-metadata/www/metadata/

# function to get metadata from onnx file 
function process_onnx() (
    set -ue
    FPATH=$1

    # create output dir
    OUTDIR=${FPATH#"${BASE_DIR}"}
    OUTDIR="${TARGET_DIR}/$(dirname $OUTDIR)"
    mkdir -p $OUTDIR

    # run command (assume salt is installed)
    OUTPUT=$(get_onnx_metadata $FPATH)
    # run command inside image
    # OUTPUT=$(singularity run \
    #     --bind /cvmfs \
    #     /cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/atlas-flavor-tagging-tools/algorithms/salt:latest/ \
    #     get_onnx_metadata $FPATH)

    # get name
    readarray -t out_array <<< "$OUTPUT"
    NAME=${out_array[0]}
    PREFIX="Model name: "
    NAME=${NAME#"$PREFIX"}

    # hacky way to get the relevant metadata
    METADATA=$(echo "$OUTPUT" | tail -n +2 )

    # save to file
    OUT_FILE="${OUTDIR}/$(basename -s .onnx $FPATH).json"
    echo $METADATA > $OUT_FILE

    # print to stdout
    echo $OUT_FILE
)
export -f process_onnx

# function to copy lwtnn files
function process_lwtnn() (
    set -ue
    FPATH=$1

    # create output dir
    OUTDIR=${FPATH#"${BASE_DIR}"}
    OUTDIR="${TARGET_DIR}/$(dirname $OUTDIR)"
    mkdir -p $OUTDIR

    OUT_FILE=${OUTDIR}/$(basename $FPATH)

    # copy relevant bits from the lwtnn file
    jq 'del(.nodes) | del(.layers)' $FPATH > $OUT_FILE

    # print to stdout
    echo $OUT_FILE
)
export -f process_lwtnn

# dump metadata for onnx files
find $BASE_DIR -name "*.onnx" -type f -exec bash -c 'process_onnx "$0"' {} \;

# dump metadata for lwtnn files
find $BASE_DIR -name "network.json" -type f -exec bash -c 'process_lwtnn "$0"' {} \;
