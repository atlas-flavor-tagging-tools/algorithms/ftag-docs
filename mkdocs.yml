site_name: ATLAS Flavour Tagging Documentation
site_description: null
site_author: FTAG Group
site_url: https://ftag.docs.cern.ch/
copyright: Copyright &copy; 2002-2025 CERN for the benefit of the ATLAS collaboration
repo_name: GitLab
repo_url: https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/ftag-docs/
edit_uri: tree/master/docs


theme:
  name: material
  features:
    - navigation.instant
    - navigation.tracking
    - navigation.tabs
    - navigation.tabs.sticky
    - navigation.sections
    - navigation.indexes
    - navigation.footer
    - navigation.top
    - content.code.copy
    - content.action.edit
  palette:
    - scheme: default
      toggle:
        icon: material/brightness-7
        name: Switch to dark mode
    - scheme: slate
      toggle:
        icon: material/brightness-4
        name: Switch to light mode


nav:
  - Home:
      - index.md
      - Publications: publications.md
      - Workshops: workshops.md
      - Getting Involved:
          - How to FTAG: getting_involved/how_to_ftag.md
          - Qualification Projects: getting_involved/aqp.md
          - Open Tasks: getting_involved/open_tasks.md
      - FTAG meetings:
          - Plenary meetings: meetings/plenary.md
          - Algo meetings: meetings/algorithms.md
          - Software meetings: meetings/software.md
          - Calibration meetings: meetings/calibration.md
          - Xbb meetings: meetings/xbb.md
      - External Links:
          - Xbb docs: https://xbb-docs.docs.cern.ch

  - Recommendations:
      - CDI for physics analyses: recommendations/index.md
      - Expected Performance:
          - r22 Performance: recommendations/algs/r22-preliminary.md
          - r21 Performance: recommendations/algs/2019-recommendations.md
      - Small-R jet Tools for analysis:
          - Overview: recommendations/tools/smallR.md
          - BTaggingEfficiencyTool: calibrations/cdi/interface/btaggingefficiencytool.md
          - BTaggingSelectionTool: calibrations/cdi/interface/btaggingselectiontool.md
          - BTaggingTruthTaggingTool: calibrations/cdi/interface/btaggingtruthtaggingtool.md
          - BTaggingEigenVectorRecompositionTool: calibrations/cdi/interface/btaggingeigenvectorrecompositiontool.md
      - Large-R jet Tools for analysis:
          - Overview: recommendations/tools/largeR.md

  - Algorithms:
      - algorithms/index.md
      - Algo meetings: meetings/algorithms.md
      - Minutes summary: meetings/algorithms-minutes.md
      - Software & Tools: software/algorithms-software.md
      - Taggers:
          - Inputs: algorithms/taggers/inputs.md
          - Training: algorithms/taggers/training.md
          - Architectures:
              - Overview: algorithms/taggers/overview.md
              - GN3: algorithms/taggers/GN3.md
              - GN2: algorithms/taggers/GN2.md
              - GN1: algorithms/taggers/GN1.md
              - DL1: algorithms/taggers/dl1.md
              - DIPS: algorithms/taggers/dips.md
              - IP-based algorithms: algorithms/taggers/ip.md
              - SV-based algorithms: algorithms/taggers/sv.md
              - UMAMI: algorithms/taggers/umami.md
          - Available taggers:
              - Available taggers: algorithms/taggers/tagger_metadata.md
              - Legacy Taggers (internal): algorithms/taggers/available_taggers.md
          - Deploying a tagger: algorithms/taggers/deploy.md
      - Samples:
          - algorithms/samples/index.md
          - List of H5 Samples: algorithms/samples/h5_sample_list.md
          - List of Derivation Samples: algorithms/samples/daod_sample_list.md
          - List of AOD Samples: algorithms/samples/aod_sample_list.md
      - Truth Labelling:
          - Jet Labels: algorithms/labelling/jet_labels.md
          - Track Labels: algorithms/labelling/track_labels.md
      - Tasks & AQPs:
          - Qualification Tasks: algorithms/aqps_and_tasks/algo_aqp.md
          - Ongoing Activites:
              - Overview: algorithms/activities/overview.md
              - Track classification tools: algorithms/activities/track_classification.md
              - MC/MC efficiency maps: algorithms/activities/mcmc.md
              - High-level alg optimisation: algorithms/activities/high-level-opt.md
              - Low-level alg optimisation: algorithms/activities/low-level-opt.md
              - Soft electron tagging: algorithms/activities/set.md
              - Soft muon tagging: algorithms/activities/smt.md
              - c-tagging: algorithms/activities/ctagging.md
              - Exclusive c-tagging using c-meson decays: algorithms/activities/dmesonreco.md
              - Large Radius Tracking: algorithms/activities/lrt.md
              - Trackless b-tagging: algorithms/activities/trackless.md
              - Upgrade: algorithms/activities/upgrade.md
              - Tracking Systematics: algorithms/activities/tracking-systematics.md
              - GNN Vertexing: algorithms/activities/gnn-vertexing.md

  - Calibrations:
      - calibrations/index.md
      - Calibration meetings: meetings/calibration.md
      - Mainstream Calibrations: calibrations/maincalib.md
      - Alternative Calibrations: calibrations/altcalib.md
      - Info for calibration teams: calibrations/tech.md
      - Archive samples on tape: calibrations/tape.md
      - Ongoing AQPs: calibrations/cal_aqps.md
      - CDI:
          - calibrations/cdi/index.md
          - Tagger efficiency and performance: calibrations/cdi/efficiency-maps.md
          - Fixed-cut scale factors: calibrations/cdi/scale-factors.md
          - Pseudo-continuous tagging: calibrations/cdi/pcbt.md
          - Using different MC generators: calibrations/cdi/mctomc.md
          - CDI file structure:
              - Inspecting CDI files: calibrations/cdi/structure/filestructure.md
              - Creating your own CDI file: calibrations/cdi/structure/filecreation.md
          - Systematic uncertainties:
              - Using systematics in analysis: calibrations/cdi/systematics/systematic-strategies.md
              - Eigenvector decomposition: calibrations/cdi/systematics/eigenvectordecomp.md
              - Validating EV reduction: calibrations/cdi/systematics/ev-validation.md

  - Software:
      - software/index.md
      - Software meetings: meetings/software.md
      - FTAG in Athena: software/athena.md
      - Derivations: samples/daod.md
      - GPU resources: software/resources.md
      - Algorithms software: software/algorithms-software.md
      - Tutorials:
          - software/tutorials/index.md
          - Dataset dumper tutorial: https://training-dataset-dumper.docs.cern.ch/tutorial-tdd/
          - Athena algorithms tutorial: https://training-dataset-dumper.docs.cern.ch/tutorial-algorithms/
          - Athena jet flavour tagging tutorial: software/tutorials/tutorial-dl2.md
          - Preprocessing tutorial: software/tutorials/tutorial-upp.md
          - Puma tutorial: https://umami-hep.github.io/puma/examples/tutorial-plotting/
          - Salt tutorial (Open Data): https://ftag-salt.docs.cern.ch/tutorial/
          - Salt tutorial (Xbb): https://ftag-salt.docs.cern.ch/tutorial-Xbb/
          - Code development tutorial: software/tutorials/tutorial-coding.md
          - High-pT Extrapolation Uncertainty Tutorial: software/tutorials/tutorial-high_pT_extrapolation.md
          - (Old) Umami tutorial: software/tutorials/tutorial-umami.md

  - Sample Handling:
      - samples/index.md
      - Derivations: samples/daod.md
      - Lifetime Management: samples/deletion.md
      - GRID Storage: samples/grid.md
      - Making an MC Request: samples/mc_requests.md


markdown_extensions:
  - footnotes
  - admonition
  - codehilite
  - pymdownx.details
  - pymdownx.inlinehilite
  - pymdownx.smartsymbols
  - pymdownx.emoji
  - pymdownx.progressbar
  - pymdownx.blocks.html
  - pymdownx.magiclink:
      user: atlas
      repo: athena
      repo_url_shorthand: true
      provider: gitlab
      custom:
        gitlab:
          host: gitlab.cern.ch
          label: gitlab
          type: gitlab
  - attr_list
  - md_in_html
  - pymdownx.arithmatex:
      generic: true
  - pymdownx.tabbed:
      alternate_style: true
      slugify: !!python/object/apply:pymdownx.slugs.slugify
        kwds:
          case: lower
  - toc:
      permalink: "#"
      toc_depth: 4
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - wikilinks:
      base_url: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/


plugins:
  - search
  - markdownextradata
  - git-revision-date-localized:
      enable_creation_date: true
      type: date
      strict: false
  - autorefs
  - redirects:
      redirect_maps:
        # This page had a high chance of someone linking to it. Since
        # we moved it we're adding a redirect.
        recommendations/calib/index.md: recommendations/index.md


validation:
  omitted_files: warn
  absolute_links: warn
  unrecognized_links: warn


extra_javascript:
  - javascripts/mathjax.js
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js

extra_css:
  - stylesheets/extra.css
