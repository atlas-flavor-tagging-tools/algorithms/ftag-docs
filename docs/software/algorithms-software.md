The development of flavour tagging algorithms towards Run-3 is mostly carried out within the Salt software framework.
It is based on HDF5 files which can be obtained from FTAG1 or FTAG2 derivations using the training dataset dumper ("the dumpster").

A typical workflow for algorithm development consists of:

1. identifying the adequate FTAG1 derivation samples
2. running the training dataset dumper on the derivation samples by submitting jobs to the grid
3. replicating the output HDF5 ntuples to a permanent storage, e.g. the localgroupdisk of an institute
4. running preprocessing on the HDF5 ntuples to create hybrid samples for training as well as validation and test samples for evaluation, using the [umami-preprocessing](https://github.com/umami-hep/umami-preprocessing) software
5. training an algorithm on hybrid samples in [Salt](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/salt/)
6. evaluating the performance of the trained algorithm in [Puma](https://github.com/umami-hep/puma) using test samples

The software can be obtained from the GitLab/GitHub projects and can be used on typical linux infrastructure, provided e.g. by CERN lxplus.
The collaborative development of the software projects also takes place using the GitLab/GitHub infrastructure for raising issues and opening merge requests. The software is tested with a suite of unit and integration tests to ensure that adding functionality doesn't break it.

## Main Frameworks

An overview of the main FTAG software frameworks is given in the table below.

| Package | Docs                                                     | Description                                                                                                |
|---------|----------------------------------------------------------|------------------------------------------------------------------------------------------------------------|
| [`training-dataset-dumper`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/) | [link](https://training-dataset-dumper.docs.cern.ch/)    | The training dataset dumper is used to create HDF5 ntuples from (D)xAODs such as FTAG1.  |
| [`atlas-ftag-tools`](https://github.com/umami-hep/atlas-ftag-tools/)                                    | [link](https://github.com/umami-hep/atlas-ftag-tools/blob/main/ftag/example.ipynb)   | A collection of useful tools for working with h5 samples.                      |
| [`upp`](https://github.com/umami-hep/umami-preprocessing/)                                             | [link](https://umami-hep.github.io/umami-preprocessing//)                  | Preprocessing of input data for salt.                     |
| [`salt`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/salt)                            | [link](https://ftag-salt.docs.cern.ch/)                  | Training of GN2 and related models.                                                                        |
| [`puma`](https://github.com/umami-hep/puma)                                                            | [link](https://umami-hep.github.io/puma/main/index.html) | Core plotting framework.                                                                                   |


## Other software

Below is information about other, less mainstream, software efforts in the group.

#### Data/MC & Efficiency Maps

For more info on calibration frameworks see the [calibration twiki](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingCalibrationsSubgroup).

| Package | Docs                                                     | Description                                                                                                |
|---------|----------------------------------------------------------|------------------------------------------------------------------------------------------------------------|
| [`ftag-data-mc`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/ftag-data-mc) | [link](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/ftag-data-mc/-/blob/r22/README.md) | Data/MC comparisons are obtained with `AnalysisTop` based scripts in the `ftag-data-mc` project. |
| [`efficiency-maps`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/efficiency-maps) | [link](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/efficiency-maps/-/blob/master/README.md) | MC/MC efficiency maps for various MC generators are obtained using the scripts in the `efficiency-maps` project. |
| [`eff-par-with-nn`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/eff-par-with-nn) | [link](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/eff-par-with-nn/-/blob/master/README.md) | Generate efficiency maps with a neural-network-based interpolation such that the efficiency is parametrised in a continuous domain. |


#### Misc Scripts

A collection of other potentially useful scripts 

- [Dan's scripts](https://gitlab.cern.ch/dguest/single-btag-validation)
- [Bing's scripts](https://gitlab.cern.ch/biliu/FTAGQuickCheck)

#### Plot Galleries

Scripts to generate plot galleries as websites (warning: old/untested!).

- [https://gitlab.cern.ch/davidc/plot_comparator](https://gitlab.cern.ch/davidc/plot_comparator)
- [https://github.com/philippgadow/plotgallery](https://github.com/philippgadow/plotgallery)
- [https://gist.github.com/philippgadow/ea80894fad8872de9990dc4d9d8efbdb](https://gist.github.com/philippgadow/ea80894fad8872de9990dc4d9d8efbdb)
- [https://gitlab.cern.ch/svanstro/plotsite/](https://gitlab.cern.ch/svanstro/plotsite/)

???info "information on how to create personal CERN webpages"

    Guide to request a CERN website: https://cernbox-manual.web.cern.ch/cernbox-manual/en/web/personal_website_content.html#create_personal_space

    https://webeos.docs.cern.ch/create_site/

    Gitlab pages docs: [https://how-to.docs.cern.ch](https://how-to.docs.cern.ch/)

#### Deprecated Software

Software projects associated with the flavour tagging algorithms subgroup from previous Run-2 algorithm development include:

- [`umami`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami/) old package for preprocessing, training (DIPS/DL1), evaluation and high level puma plotting.
- [Performance framework](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/Run2BtagOptimisationFramework) (used extensively for Run-2 development and HL-LHC studies)
- [DL1 framework](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/DL1_framework)
- [RNNIP framework](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/rnnip)
- [DL1 hyperparameter optimisation](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/dl1-hyperparameter-optimisation)
