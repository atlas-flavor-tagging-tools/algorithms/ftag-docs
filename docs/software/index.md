# FTAG Software Documentation

## General Information

### Conveners
- [Romain Bouquet](https://atlas-glance.cern.ch/atlas/membership/members/profile?id=12022)
- [Ligang Xia](https://atlas-glance.cern.ch/atlas/membership/members/profile?id=11000)

to be reached via <atlas-cp-flavtag-software-conveners@cern.ch>

[FTAG jira dashboard](https://its.cern.ch/jira/secure/Dashboard.jspa?selectPageId=22701){ .md-button .md-button--primary }
[FTAG ART Results](https://bigpanda.cern.ch/art/tasks/?package=FlavourTaggingTests&branch=main/Athena/x86_64-el9-gcc13-opt,24.0/Athena/x86_64-el9-gcc13-opt&nlastnightlies=7&view=packages){ .md-button .md-button--primary }

### E-groups & Mailing List
The group mailing list is: <atlas-cp-flavtag-software@cern.ch>

If you want to follow group activities, and access certain resources, it is highly recommended to 
subscribe to the following egroups using the [CERN egroups webpage](https://e-groups.cern.ch/e-groups/EgroupsSearch.do).

- `atlas-cp-flavtag-software`


### Meetings
The meetings are taking place every Tuesday at 13h00 CERN time: [https://indico.cern.ch/category/9120/](https://indico.cern.ch/category/9120/).

A meeting overview is available [here](../meetings/software.md).


### Mattermost
You can join our Mattermost channel [here](https://mattermost.web.cern.ch/signup_user_complete/?id=1wicts5csjd49kg7uymwwt9aho&md=link&sbr=su).

It is used to discuss a variety of topics and is the easiest way to ask questions.

General information about the usage of ATLAS software is provided in these training resources.

- [ATLAS software documentation](https://atlassoftwaredocs.web.cern.ch)
- [ATLAS software tutorial](https://atlassoftwaredocs.web.cern.ch/analysis-software/ASWTutorial/)
- [ATLAS analysis tutorial](https://atlassoftwaredocs.web.cern.ch/analysis-software/AnalysisSWTutorial/)