# High-$p_T$-Extrapolation Tutorial
This page documents the high-pT Monte-Carlo based extrapolation of flavour tagging algorithms out to jet $p_T$ of 3TeV.

Documented in the [PUB note](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2021-003/){:target="_blank"}, extrapolation of flavour-tagging calibration to high-$p_T$ has been a component of flavour tagging calibration for analyses involved with flavour tagging of jet at large transverse momenta. The goal of this tutorial is for users to first understand the procedure and then to reproduce, step-by-step, an extrapolated calibration for DL1dv01, with one systematic from each of the following categories: jet, tracking, parton shower and final state radiation (FSR).

This tutorial was produced for the FTAG workshop 2023 by Thomas Dingley [thomas.dingley@cern.ch](mailto:thomas.dingley@cern.ch). If you come across any bugs or issues, or have feedback, please feel free to contact!

## Methodology 
The mainstream flavour tagging calibration allows for jet-tagging up to 400GeV. The $t\bar{t}$ data sample used runs low on statistics above this pT range, and as shown below, the statistical uncertainties inflate.

Flavour tagging algorithms have differing performance in Monte-Carlo (MC) and data. Correcting the MC to data differences with scale-factors is an important stage of an analysis to enable valid comparisons between MC and data. Calibrating these algorithms is therefore an important stage of an algorithms deployment. The calibration defines efficiency scale-factors: $$ SF_b = {\epsilon_b^{data} \over \epsilon_b^{MC}} $$
An example of such a calibration is shown below, where the SF is shown as a function of jet $p_T$:

![mainstreamcalibplot](assets/dl1SFplot.png)

The statstical uncertainty of this calibration increases steadily as the transverse momenta approaches 400 GeV, the source of this can be seen below, where the $t\bar{t}$ data sample used runs low on statistics.

![ttbar sample](assets/ttbarpt.png)

There are, however, analyses in which jets extend easily into the TeV region of phase-space. The need for scale-factors therefore remains. 

### Extrapolation procedure
The MC-based extrapolation is as follows:
An obervation of rough $p_T$ independence for our SF's, alongside $SF\approx1$, can be seen across the mainstream calibration region, this obervation is taken as an assumption up to high transverse momenta (3TeV in this case): 
$$ SF_b(p_T) = \begin{cases}
    SF_b & \text{if } p_T \leq 400 \text{GeV} \\
    1 & \text{if } x >400 \text{GeV}
\end{cases}
$$

The purpose of this framework is, then, to estimate the uncertainty component involved with this extrapolated scale-factor by investigating various sources of systematic uncertainty. 

Systematics are applied to a dedicated $Z'$ sample, and systematic uncertainties are quantified as follows: $$ \delta \epsilon_{i,s} = \frac{\epsilon_{i,s}-\epsilon_{i}}{\epsilon_{i}} $$

The full uncertainty on the extrapolated scale-factor is then given as: 
$$ \sigma^2_{extrap}  \left (p_T;p_{T;\text{ref}} \right ) = \sum_{s \in S}\max_{<p_T} \left [ \delta\epsilon_{i,s}\left (p_T\right ) - \delta\epsilon_{i,s}\left (p_T;\text{ref}\right ) \right ]^2 $$
In the extrapolation domain, the uncertainty componennts are not expected to decrease other than statistical fluctuations - the $\max_{<p_T}$ requirement is therefore enforced to protect against this.


## Getting Started
The high-pT-extrapolation framework involves two repositories at this early stage, the [high-$p_T$-extrapolation](https://gitlab.cern.ch/atlas-ftag-calibration/high-pT-extrapolation/-/tree/release22){:target="_blank"} repository alongside the [training-dataset-dumper](https://gitlab.cern.ch/tdingley/trainingDD-tom/-/tree/main?ref_type=heads){:target="_blank"}. This framework is a work-in-progress.

Training-Dataset-Dumper:
Currently, this workflow utilises a recent fork (insert date) of the TDD. Parameters pertaining to jet & track systematics can be varied and flavour tagging algorithms re-run (retag the sample) to produce a systematically varied sample. 
From here, with the varied samples, the uncertainty can be computed:$$ \delta \epsilon_{i,s} = \frac{\epsilon_{i,s}-\epsilon_{i}}{\epsilon_{i}} $$.

Before we clone each repository, let's make a directory for the tutorial:
```
mkdir xtrap-tutorial && cd xtrap-tutorial
```

### Training Dataset Dumper 
The [training-dataset-dumper](https://gitlab.cern.ch/tdingley/tdd-extrapolation/-/tree/FTAG_Workshop?ref_type=heads){:target="_blank"} is flavour tagging software that allows, for the purpose of this tutorial, systematic variations of parameters within the MC sample and the re-application of flavour tagging algorithms (retagging). Please clone the fork of the TDD, linked above:

```
# on lxplus:
setupATLAS
lsetup git
mkdir tdd_xtrap && cd tdd_xtrap
git clone --recursive -b FTAG_Workshop https://gitlab.cern.ch/tdingley/tdd-extrapolation.git
```
For a more thorough tutorial on the TDD, please see the [TDD tutorial](https://ftag-docs.docs.cern.ch/software/tutorials/tutorial-tdd/){:target="_blank"}. 

### High-pT-Extrapolation
The [high-$p_T$-extrapolation](https://gitlab.cern.ch/atlas-ftag-calibration/high-pT-extrapolation/-/tree/release22){:target="_blank"} framework: software developed from the R21 branch to produce tagged histograms now using the TDD for R22 systematics, evaluate uncertainties for each systematic, produce input CDI .txt files for the calibration alongside plotting functionality. For this tutorial please use the following command:
On lxplus:
```
git clone --recursive -b release22 https://gitlab.cern.ch/tdingley/high-pt-extrapolation-workshop
```
In future, for the main repository please sign up to the: `atlas-cp-flavtag-calibrations@cern.ch` mailing list here: `https://e-groups.cern.ch/e-groups/EgroupsSearchForm.do` - this give permissions to access the release22 branch. 

With the repository setup, we move to NTuple production - using the training-dataset-dumper.
## NTuple Production
### Samples
For the extrapolation, dedicated $Z'$ MC samples are produced, with a flat-$p_T$ spectrum up to high-$p_T$. For this tutorial you can use files stored within `/eos/user/t/tdingley/xtrap-data`. **For future use**, please download the following:
```
# within xtrap-tutorial directory
mkdir data && cd data
setupATLAS
lsetup rucio
# flat extended Z' sample used for estimating track, jet and FSR systematics
rucio download --nrandom 1 mc20_13TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e7954_s3681_r13144_p5770
# Z' sample with MadGraph interfaced to Pythia8
rucio download --nrandom 1 mc20_13TeV.500568.MGPy8EG_NNPDF23ME_A14_ZPrime.deriv.DAOD_FTAG1.e7954_s3126_r13144_p5770
# Z' sample with MadGraph interfaced to Herwig7
rucio download --nrandom 1 mc20_13TeV.500567.MGH7EG_NNPDF23ME_Zprime.deriv.DAOD_FTAG1.e7954_s3681_r13144_p5770
cd ..
```

Before moving on - **make a clean shell** to avoid conflicting setups!
### Setting up the TDD
Navigate to the cloned TDD directory and setup the directory:
```
cd tdd_xtrap
setupATLAS
mkdir build && cd build
asetup Athena,23.0.34 # recent athena release, for the most current we can also source ../tdd-extrapolation/setup/athena-latest.sh
cmake ../tdd-extrapolation/
make
source x*/setup.sh
```
As this repository is based predominantly on grid-productions, for this tutorial we will be using rather cumbersome directory names:
```
# navigate back to tutorial directory
cd ../../
# make directories for the tutorial output files to be stored
mkdir outputs && mkdir outputs/tracking && mkdir outputs/tracking/user.name.800030.TRK_RES_D0_MEAS.EMPFlow_output.h5 && mkdir outputs/tracking/user.name.800030.nominal.EMPFlow_output.h5
mkdir outputs/jet && mkdir outputs/jet/user.name.800030.JET_JER_EffectiveNP_1.EMPFlowSys_output.h5 && mkdir outputs/jet/user.name.800030.nominal.EMPFlowSys_output.h5
mkdir outputs/partonshower && mkdir outputs/partonshower/user.name.500567.herwig7.EMPFlowSys_output.h5 && mkdir outputs/partonshower/user.name.500568.pythia8.EMPFlowSys_output.h5
```

The purpose of this tutorial is to estimate systematic uncertainties as: $$ \delta \epsilon_{i,s} = \frac{\epsilon_{i,s}-\epsilon_{i}}{\epsilon_{i}} $$ where $\epsilon_{i,s}$ is the efficiency of the algorithm for jet flavour $i \in (b,\text{ } c, \text{ light})$ and systematic uncertainty, $s$, and $\epsilon_i$ is the nominal (no systematic variation) efficiency for this sample. Both nominal and varied samples are therefore required. 
### Systematically Varied NTuple Production
#### Track Systematics
We start with a more illustrative example of the TDD's capabilities: a track systematic varied NTuple. The impact parameter resolution (along with other impact parameter variables) are important inputs to DL1d, this is the systematic which we will compute. Within [InDetTrackSystematics](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/TrackingID/InDetTrackSystematicsTools/InDetTrackSystematicsTools/InDetTrackSystematics.h){:target="_blank"} there is a list of all variations and their corresponding naming conventions. For the D0 IP resolution we call`TRK_RES_D0_MEAS`.
```
cd tdd_xtrap/tdd-extrapolation
# within the tdd-extrapolation directory
ca-dump-retag /eos/user/t/tdingley/xtrap-data/mc20ZPrime_extended/DAOD_FTAG1.34313388._000181.pool.root.1 -c configs/EMPFlow.json -t TRK_RES_D0_MEAS -o ../../outputs/tracking/user.name.800030.TRK_RES_D0_MEAS.EMPFlow_output.h5/TRK_RES_D0_MEAS.h5 -m 1000
```
The function `ca-dump-retag` calls the function `retag.py` which collects lists of systematics to apply, in this case the track systematic `TRK_RES_D0_MEAS` with the flag `-t`. The varied NTuple is created as a hdf5 (.h5) file with the tagger discriminants as decorations for each jet in the sample.

#### Jet Systematics
For jet systematics we run the same function, with slight alterations:
```
ca-dump-retag /eos/user/t/tdingley/xtrap-data/mc20ZPrime_extended/DAOD_FTAG1.34313388._000181.pool.root.1 -c configs/EMPFlowSys.json -j AntiKt4EMPFlowJetsSys -J JET_JER_EffectiveNP_1 -S 1.0 -m 1000 -o ../../outputs/jet/user.name.800030.JET_JER_EffectiveNP_1.EMPFlowSys_output.h5/JET_JER_EffectiveNP_1.h5
```


#### [Optional / WIP] Final State Radiation
Accessing relevant MC event weights relating to the renormalisation scale, $\mu_R$, requires AnalysisTop. For the purposes of the tutorial this may be skipped as the rest of the tutorial does not depend on this. This section is a work-in-progress. 

### Nominal NTuple production
To do this within the TDD we still run re-tagging but make no alterations to the parameters within the sample:
```
ca-dump-retag /eos/user/t/tdingley/xtrap-data/mc20ZPrime_extended/DAOD_FTAG1.34313388._000181.pool.root.1 -c configs/EMPFlow.json -o ../../outputs/tracking/user.name.800030.nominal.EMPFlow_output.h5/nominal.h5 -m 1000
```
Nominal ntuple production calls no addition flags, such that the component accumulator doesn't vary parameters of the MC. This produces a nominal ntuple for the `EMPFlow.json` configuration. 

A nominal sample is required for all systematic variations, in the form in which they are produced. `EMPFlow.json` is the configuration for tracking / parton shower ntuple production, we therefore require a nominal sample for the jet systematic configuration: `EMPFlowSys.json`.
Run now with a new configuration file:
```
ca-dump-retag /eos/user/t/tdingley/xtrap-data/mc20ZPrime_extended/DAOD_FTAG1.34313388._000181.pool.root.1 -c configs/EMPFlowSys.json -j AntiKt4EMPFlowJetsSys -J JET_nominal -S 1.0 -m 1000 -o ../../outputs/jet/user.name.800030.nominal.EMPFlowSys_output.h5/nominal.h5
```

#### Parton Shower
Uncertainties relating to the choice of Monte-Carlo generator are estimated by a 2-point systematic, with Pythia8 as the nominal and Herwig7 as the varied samples. The estimation of this systematic within the extrapolation framework is achieved by running the nominal TDD retagging for the two separate samples. For example:
```
# create parton shower directories within outputs:
mkdir outputs/partonshower && mkdir outputs/partonshower/user.name.500567.herwig7.EMPFlowSys_output.h5 5&& mkdir outputs/partonshower/user.name.500568.pythia8.EMPFlowSys_output.h5
# Pythia8
ca-dump-retag /eos/user/t/tdingley/xtrap-data/mc20_pythia8/DAOD_FTAG1.34313361._000226.pool.root.1 -c configs/EMPFlow.json -o ../../outputs/partonshower/user.name.500568.pythia8.EMPFlowSys_output.h5/pythia8.h5 -m 1000
# Herwig7
ca-dump-retag /eos/user/t/tdingley/xtrap-data/mc20_herwig7/DAOD_FTAG1.34313412._000214.pool.root.1 -c configs/EMPFlow.json -o ../../outputs/partonshower/user.name.500567.herwig7.EMPFlowSys_output.h5/herwig7.h5 -m 1000
```
Now that both a varied and nominal sample have been produced for each systematic category, we can move to tagged root histogram production - using the `high-pT-extrapolation-workshop` directory.

## Efficiency Production
This section of the tutorial involves the [high-$p_T$-extrapolation](https://gitlab.cern.ch/atlas-ftag-calibration/high-pT-extrapolation/-/tree/release22){:target="_blank"} framework. Open up a clean shell once more and setup for efficiency production:
```
# in xtrap-tutorial directory
cd high-pt-extrapolation-workshop
cd TDD_EfficiencyProducer
source setup.sh
```
Once setup, root histograms can be produced for each h5 file obtained previously within NTuple production. This code applies the algorithm discriminant for the relevant tagger and produces tagged histograms in correspondance with each fixed-cut working point (WP $\in [60,70,77,85]$%):

```
mkdir histograms
python condor_effProd.py ../../outputs/tracking -o histograms -S tracking -F b
python condor_effProd.py ../../outputs/jet -o histograms -S jet -F b
python condor_effProd.py ../../outputs/partonshower -o histograms -S modelling -F b
```

These functions produce histograms within folders named `[T/J/M]UNC`, containing root files. When run with the full dataset, not only one file, this will contain a separate root file for each file in the $Z'$ sample. 
## Uncertainty Computation & Plotting
With histograms produced for nominal and systematically varied samples, the uncertainties can therefore be computed. First create a **clean environment** and navigate to: `xtrap-tutorial/high-pT-extrapolation`
Usually as there are a number of files & systematics they will be combined into one root histogram:
```
source setup.sh
python CDITools/CombineEfficiencies.py TDD_EfficiencyProducer/histograms
```
Now we have all the root files together, separated into systematic category, we can produce CDI input files - with the uncertainty binned in $p_T$. To customize binning go within the efficiency_producer code and change the edges_{b,c,l} array. 
```
python CDITools/RunCDIInputProductionCampaign.py --indir TDD_EfficiencyProducer/histograms/combined/01 --outdir TDD_EfficiencyProducer/histograms/CDI
```
Inspect the contents of the outputs within the CDI folder produced in `TDD_EfficiencyProducer/histograms/CDI`. You should see percentage uncertainties for each systematic, binned in pT. 
To create plots of the relative uncertainty:
$$ \sigma_{rel} = \delta\epsilon_{i,s}\left (p_T\right ) - \delta\epsilon_{i,s}\left (p_T;\text{ref}\right ) $$
```
python CDITools/RunCDIPlottingCampaign.py --indirs TDD_EfficiencyProducer/histograms/CDI/ --refpt 400 # 250 for charm, 300 for light jets
```
This will create plots, such as:
![exampleplot](assets/high_pT_extrapolation_example_plot.png)

<MAKE EXAMPLE OUTPUTS FOR THE USERS TO REPRODUCE>


## [WIP] Grid NTUple Production 
This tutorial focusses on producing CDI inputs for a single file within a large MC sample. Running the full dataset is significantly more efficient when run on the grid. The tutorial is a work in progress.

