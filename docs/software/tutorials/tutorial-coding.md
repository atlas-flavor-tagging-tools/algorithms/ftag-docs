# Good coding practices

## Introduction

This tutorial provides you with suggestions on good coding practice using test-driven development.

You can find the presentation material and the recordings on the [indico page](https://indico.cern.ch/event/1145743/).

In this tutorial you will learn how to:

1. Clone and set up environment for [good code practices tutorial](https://gitlab.cern.ch/mguth/good-code-practices)
2. Setting up pre-commit hook
3. Write a generator for a binary sequence
4. Object Oriented Programming - Implement a 2D Vector
5. Investigate example below with debugger
6. Unit tests
7. Integration tests

The tutorial is meant to be followed in a self-guided manner. You will be prompted to do certain tasks by telling you what the desired outcome will be, without telling you how to do it.
In case you are stuck, you can click on the "hint" toggle box to get a hint. If you tried for more than 10 min at a problem, feel free to toggle also the solution with a worked example.

In case you encounter some errors, please reach out on the [FTAG tutorials mattermost channel](https://mattermost.web.cern.ch/aft-algs/channels/ftag-tutorials) (click [here](https://mattermost.web.cern.ch/signup_user_complete/?id=1wicts5csjd49kg7uymwwt9aho&md=link&sbr=su) to sign up) and open a merge request to fix the tutorial [here](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/ftag-docs).

## Prerequisites

A basic python knowlegde is required, please have a look at the [prerequisites.md](https://gitlab.cern.ch/mguth/good-code-practices/-/blob/master/prerequisites.md).

## Tutorial tasks

### 1. Fork, clone and set up the environment for the [good code practices tutorial](https://gitlab.cern.ch/mguth/good-code-practices)

Before you can start with the other tasks, you need to do this one first.
The expected outcome of this task is that you will have

1. a personal fork of the [`good-code-practices`](https://gitlab.cern.ch/mguth/good-code-practices) GitLab project,
2. cloned it to your work area on your machine using `git`,
3. set up a development branch for the tutorial called `my_tutorial_branch`,
4. set up the environment for the tutorial

Go to the GitLab project page of the `good-code-practices` to begin with the task: <https://gitlab.cern.ch/mguth/good-code-practices>



??? info "Hint: how can I create a fork of a project?"
    In case you are stuck how to create your personal fork of the project, you can find some general information on git and the forking concept [here in the GitLab documentation](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html).




??? info "Hint: how can I create a new branch?"
    You can create a new branch and change to it with `git` using the following command:
    ```bash
    git checkout -b my_tutorial_branch
    ```



??? info "Hint: how can I set up the environment?"
    You can use the provided docker image `gitlab-registry.cern.ch/mguth/good-code-practices/base-python:latest`
    Running with Docker:
    ```bash
    docker run -it -v $(pwd):$(pwd) -w $(pwd)  gitlab-registry.cern.ch/mguth/good-code-practices/base-python:latest bash
    ```
    Running with Singularity:
    First pull the image to a location where you have some space available
    ```bash
    singularity pull good-code-practices.sif docker://gitlab-registry.cern.ch/mguth/good-code-practices/base-python:latest
    ```
    then you can start the singularity image
    ```bash
    singularity shell good-code-practices.sif
    ```

    If you are using lxplus, please have a look at the [umami docs](https://umami.docs.cern.ch/setup/installation/#launching-containers-using-singularity-lxplusinstitute-cluster) how to set up `singularity` there.



### 2. Setting up pre-commit hook
We want to use the linter `flake8` as a pre-commit hook to check each time when you commit changes that your code is consistent with the `flake8` conventions.

If you want to know more about `pre-commit` hooks, you can have a look at [their website](https://pre-commit.com).


??? info "Hint"
    [The `pre-commit` website](https://pre-commit.com) provides a good quick start.
    You can also have a look in the [flake8 docs](https://flake8.pycqa.org/en/latest/user/using-hooks.html).



??? warning "Solution"
    First, you need to set `pre-commit` up in your repository via (you need to be at the root level of the repository)
    ```bash
    pre-commit install
    ```
    Then you need to create a new file `.pre-commit-config.yaml` in the root level of your repository and add the following content
    ```yaml
    repos:
      - repo: local
        hooks:
          - id: flake8
            name: flake8
            stages: [commit]
            language: system
            entry: flake8
            types: [python]
    ```
    To check if the hook is working fine, you can run over all files
    ```bash
    pre-commit run --all-files
    ```
    This will actually raise some errors like `imported but unused`, since there are files which are pre-prepared for the next tasks you need to fill in (and fix :) ).
    Now, each time you commit your code it will check if it's consistent with `flake8`.


Note: we just picked one pre-commit hook, e.g. in umami, we have some more, have a look [here](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami/-/blob/master/.pre-commit-config.yaml).

You can check out what `pylint` is doing as well. It checks your syntax even more strictly and provides you with useful hints.


### 3. Write a generator for a binary sequence

The method should take a `limit` parameter. Each item in the sequence is the product of the previous value and `2`: $a_n = 2 \cdot a_{n-1}$. The sequence starts with 1. The sequence should stop when the `limit` is reached.

Name your function `exp_seq`

??? info "Hint"
    The function should return the following values
    ```bash
    list(exp_seq(10))
    >>> [1, 2, 4, 8]

    sum(exp_seq(10))
    >>> 15

    sum(exp_seq(1000_1000))
    >>> 16777215
    ```



??? info "Hint"
    You can use a [`while` loop](https://docs.python.org/3/tutorial/introduction.html#first-steps-towards-programming) for this task.



??? warning "Solution"
    ```py
    def exp_seq(limit):
        v = 1
        while v < limit:
            yield v
            v *= 2
    ```

### 4. Object Oriented Programming - Implement a 2D Vector

You can find the idea of object oriented programming in the auxiliary material of the [tutorial slides](https://indico.cern.ch/event/1145743/contributions/4809019/attachments/2426561/4154380/FTAG-algo-good-practices.pdf).

Implement a `Vector2D` class such that the following lines work

```py
a = Vector2D(4, 3)
a.x
>>> 4

a.y
>>> 3

a.length()
>>> 5.0

a.scale(3)
a.length()
>>> 15.0
```


??? info "Hint"
    The `math` package is useful to calculate the square root `math.sqrt()`.
    Start from `class Vector2D` and add `__init__` and `length` & `scale` functions.




??? warning "Solution"
    ```py
    import math
    class Vector2D:
        def __init__(self, x, y):
            self.x = x
            self.y = y
        def length(self):
            return math.sqrt(self.x**2 + self.y**2)
        def scale(self, factor):
            self.x *= factor
            self.y *= factor
    ```


### 5. Investigate example below with debugger
Investigate the example below. The desired outcome is that `cities` is the following set `{'Bern', 'Praque', 'London', 'Oslo', 'Paris'}`.

You can find it as a script already in the repository [debug_example.py](https://gitlab.cern.ch/mguth/good-code-practices/-/blob/master/debug_example.py)
```py
cities = {"London", "Paris", "Bern"}  # Unordered collection


def get_new_cities():
    new_cities = []
    new_cities.append("Oslo")
    new_cities.append("Praque")
    return set(new_cities)


cities.union(get_new_cities())

print(cities)  # Does not include Oslo, Praque!
>>> {'London', 'Paris', 'Bern'}
```

??? info "Hint"
    Add `breakpoint()` in the code and rerun the script.
    Now you can examine the different values of the variables etc.


??? warning "Solution"
    ```py
    cities = {"London", "Paris", "Bern"}  # Unordered collection


    def get_new_cities():
        new_cities = []
        new_cities.append("Oslo")
        new_cities.append("Praque")
        return set(new_cities)


    cities = cities.union(get_new_cities())

    print(cities)  # Does not include Oslo, Praque!
    >>> {'Bern', 'Praque', 'London', 'Oslo', 'Paris'}
    ```


### 6. Unit tests

A (failing) example is given for [palindromes](https://en.wikipedia.org/wiki/Palindrome) [inspired by [this tutorial](https://realpython.com/pytest-python-testing/)].

To run the unit tests, you need to go into the python-testing folder

```bash
cd python-testing
```

and then you can run the tests via

```bash
pytest -v -s test_palindrome.py
```

As a first exercise, please fix the unit tests by adapting the palindrome code [`python-testing/mymodule/palindrome.py`](https://gitlab.cern.ch/mguth/good-code-practices/-/blob/master/python-testing/mymodule/palindrome.py).

Are all the tests making sense?
You can edit the tests in [`python-testing/test_palindrome.py`](https://gitlab.cern.ch/mguth/good-code-practices/-/blob/master/python-testing/test_palindrome.py).


??? warning "Solution"
    You can find the solutions in a sparate branch of the repository `solutions-unit-tests`
    The script [`python-testing/test_palindrome.py`](https://gitlab.cern.ch/mguth/good-code-practices/-/blob/solutions-unit-tests/python-testing/test_palindrome.py) could then look like this and [`python-testing/mymodule/palindrome.py`](https://gitlab.cern.ch/mguth/good-code-practices/-/blob/solutions-unit-tests/python-testing/mymodule/palindrome.py) could be written like that.

    To see the differences, see the comparison of the two branches [here](https://gitlab.cern.ch/mguth/good-code-practices/-/compare/master...solutions-unit-tests?from_project_id=134005).

    There is no unique solution, though.


### 7. Integration tests

Write a small module (the structure is already provided in the folder [`python-testing/mymodule`](https://gitlab.cern.ch/mguth/good-code-practices/-/tree/master/python-testing/mymodule)) to generate two different random distributions (normal and exponential), generate a histogram and plot it with statistical uncertainties. They should look roughly like this


![exp hist](assets/exp-hist.png)
![normal](assets/normal-hist.png)


The idea is to do the following steps;

- you have three functions (classes)
    - `generate_data` (function)
    - `Histogram` (class)
    - `plot_histogram` (function)

They are already predefined in the folder [`mymodule`](https://gitlab.cern.ch/mguth/good-code-practices/-/tree/master/python-testing/mymodule).

#### 1. Write functionalities of these 3 functions
#### 2. In parallel, write unit tests for `generate_data` and `Histogram`  in the [`python-testing/test_unit_mymodule.py`](https://gitlab.cern.ch/mguth/good-code-practices/-/blob/master/python-testing/test_unit_mymodule.py) file.



??? info "Hint"
    A skeleton of the three functions are already implemented

    - [`generate_data`](https://gitlab.cern.ch/mguth/good-code-practices/-/blob/master/python-testing/mymodule/data_generation.py): `python-testing/mymodule/data_generation.py`
    - [`Histogram](https://gitlab.cern.ch/mguth/good-code-practices/-/blob/master/python-testing/mymodule/histogramming.py)`: `python-testing/mymodule/histogramming.py`
    - [`plot_histogram](https://gitlab.cern.ch/mguth/good-code-practices/-/blob/master/python-testing/mymodule/plotting.py)`: `python-testing/mymodule/plotting.py`


#### 3. Write an integration test, to test the full chain
    This will be implemented in [`python-testing/integration_test_my_module.py`](https://gitlab.cern.ch/mguth/good-code-practices/-/blob/master/python-testing/integration_test_my_module.py)

??? info "Hint"
    To run your test, simply execute the python script (you have to be in the folder `integration_test_my_module.py`)
    ```
    python integration_test_my_module.py
    ```


#### 4. Add the integration test to the gitlab CI in the [`.gitlab-ci.yml`](https://gitlab.cern.ch/mguth/good-code-practices/-/blob/master/.gitlab-ci.yml) file.

??? info "Hint"
    Have a look at the auxiliary material of the [tutorial talk](https://indico.cern.ch/event/1145743/#1-overview-good-coding-practic) which explains the gitlab CI.

    You can find in the [`.gitlab-ci.yml`](https://gitlab.cern.ch/mguth/good-code-practices/-/blob/master/.gitlab-ci.yml) file already a scheleton which you can uncomment and further adapt.


#### 5. Code passing linting.

??? info "Hint"
    Run `flake8` in the `python-testing` directory
    ```
    flake8 .
    ```

    Bonus:
    You can also check if your code is conform with `pylint`
    ```
    pylint mymodule/
    ```

??? warning "Solution"
    Again, there is not one correct solution.
    A possibile solution is implemented in the branch `solutions-integration-tests`.

    The full diff can be viewed [here](https://gitlab.cern.ch/mguth/good-code-practices/-/compare/master...solutions-integration-tests?from_project_id=134005).
