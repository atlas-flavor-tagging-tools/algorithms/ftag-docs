# Data Storage and GRID Usage

The flavour tagging group has own storage area on EOS. You can access it via `lxplus` at

```bash
/eos/atlas/atlascerngroupdisk/perf-flavtag
```

It is divided into several subfolders. Among them, please note that

```bash
/eos/atlas/atlascerngroupdisk/perf-flavtag/training
```

is mainly used by the algorithm subgroup, and

```bash
/eos/atlas/atlascerngroupdisk/perf-flavtag/calib
```

is dedicated to the calibration stuff.

However, this space is very limited and you should absolutely not use it for private work.
If you have further questions, reach out to the FTAG group space manager who is listed on the [FTAG documentation home page](https://ftag.docs.cern.ch/#disk-space-manager).


## Replicating datasets for permanent storage

You have [run the training-dataset-dumper on the grid](https://training-dataset-dumper.docs.cern.ch/grid/) to process datasets.
The resulting output files in `h5` format will be stored on temporary storage, so called scratch disks at the grid sites which hosted the respective jobs. 
These files will be open to deletion after a grace period of two weeks.

To make the output stored more permanently, you should replicate the datasets to local groupdisks.
Typically, every CERN user has a certain quota assigned on local groupdisks of grid resources affiliated with the user's institute.

Below is a guide how to replicate datasets using the rucio web interface.

### Step 0: who should replicate datasets?

The people with access to the flavor tagging Rucio Storage Elements (RSEs) [are listed in the atlas-auth dashboard](https://atlas-auth.cern.ch/dashboard#!/group/a02f3b3c-2bc0-4c7e-9227-d14b9ab8ce84). You should either ask one of these people or ask for one of the managers (disk manager and/or FTAG conveners) to add you.

### Step 1: Obtain the names of the output datasets
You can obtain the names of the output datasets using the rucio client. The example below assumes that the user who submitted the jobs to the grid has the user name `<username>`  and tagged when submitting (`-t <tag>` argument) with the tag `<tag>`.

```bash
# set up rucio
setupATLAS
lsetup rucio
# obtain grid proxy
voms-proxy-init -voms atlas
# list datasets for user "<username>" produced with dataset dumper with tag "<tag>"
rucio ls "user.<username>:user.<username>.*<tag>*.h5" --short | sort
```

### Step 2: Open the rucio web interface
Make sure you have your grid certificate loaded in your web browser (see [here](https://atlassoftwaredocs.web.cern.ch/ASWTutorial/basicSetup/grid_vo/) for instructions) and open the website [https://rucio-ui.cern.ch](https://rucio-ui.cern.ch). Navigate to the data transfer (R2D2) to request new rules for dataset replication and click on "Request new rule".

![R2D2 web interface: request new rule](../assets/rucio/rucio0_web.jpg)

### Step 3: Enter list of datasets to be replicated
Add the list of the datasets. Note that there is a dataset identifier search. Because you already have found the datasets in the first step, you can copy the list of datasets in the tab "List of DIDs" (on the right). Click on "Search" and confirm the selected datasets to proceed.

![R2D2 web interface: enter datasets](../assets/rucio/rucio1_web.jpg)

### Step 4: Select destination for replication

[perf-flavtag]: https://lcg-voms2.cern.ch:8443/voms/atlas/group/edit.action?groupId=32

Select a RSE (Rucio Storage Element) for replication. Make sure you select a localgroupdisk and not a scratchdisk to ensure permanent storage.
Typically, this is the disk of your institute or a big computing centre affiliated with your grid user account.
For FTAG studies, this could also be one the FTAG RSEs:
- `CERN-PROD_PERF-FLAVTAG` - group disk at CERN, 100 TB, eos aware from lxplus;
- `IN2P3-CC_PERF-FLAVTAG` - group disk at Lyon T1 site, 250 TB, possible interacive remote access from lxplus, see [Interactive remote access](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/DistributedAnalysis#Interactive_remote_access_to_dat).

You should ask someone with rights to [`perf-flavtag`][perf-flavtag] to replicate here. In case you do not have a quota on `perf-flavtag`, you need to select the option "Ask for approval" in order to let group space managers approve your request.
Alternatively, you could ask for `atlas/perf-flavtag` group membership as shown
[here](https://atlassoftwaredocs.web.cern.ch/analysis-software/ASWTutorial/basicSetup/grid_vo/#add-group-or-roles).

![R2D2 web interface: enter destination RSE](../assets/rucio/rucio2_web.jpg)

### Step 5: Select a sufficiently large lifetime
Select a lifetime which you think is suitable for the samples before they get flagged for deletion.
If you are replicating samples which will be centrally used by FTAG, we suggest a **maximum lifetime of 3 years**.
If you need a long or infinite lifetime, it's recommended to replicate to `perf-flavtag` and get in touch with FTAG conveners and/or the disk manager.

![R2D2 web interface: lifetime](../assets/rucio/rucio3_web.jpg)

### Step 6: Check and confirm
Have a final look that everything is correct and confirm the creation of the rules.

![R2D2 web interface: confirm](../assets/rucio/rucio4_web.jpg)

A summary of the created rules for the dataset replication is shown. You have completed the dataset replication!

![R2D2 web interface: done](../assets/rucio/rucio5_web.jpg)


## Uploading datasets to the grid

It is possible to upload datasets to the grid from a local machine.

### Step 1: Set up rucio

First, set up rucio and authentificate with your grid certificate (see [instructions here](https://atlassoftwaredocs.web.cern.ch/ASWTutorial/basicSetup/grid_vo/) how to get a grid certificate).

```bash
# set up rucio
setupATLAS
lsetup rucio
# obtain grid proxy
voms-proxy-init -voms atlas
```


### Step 2: Upload files to temporary resource

```bash
mkdir GridUpload
# copy all files in GridUpload
cp <files you want to upload> ./GridUpload
# upload files to dataset for user "<username>"
rucio upload --rse CERN-PROD_SCRATCHDISK user.<username>:user.<username>.MyFirstDataset ./GridUpload
# check that it worked
rucio ls user.<username>:user.<username>.MyFirstDataset
rucio list-files user.<username>:user.<username>.MyFirstDataset
```

### Step 3: Transfer to final destination

Follow the instructions for [replicating datasets to local groupdisks for permanent storage](https://ftag-docs.docs.cern.ch/software/grid/#replicating-datasets-to-local-groupdisks-for-permanent-storage) (after step 2) how to transfer the dataset from the temporary resource to a localgroupdisk using the rucio webinterface [https://rucio-ui.cern.ch](https://rucio-ui.cern.ch).
