# Derivation deletion campaigns

In regular intervals, [obsolete derivation datasets](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/DerivationProductionTeam#Data_deletion_policy_for_obsolet) are automatically deleted.

The table below holds links to the pages showing datasets scheduled for imminent deletion.

| TYPE | FTAG1 derivation | FTAG2 derivation |
| ---- | ---------------- | ---------------- |
| data | [FTAG1 data](https://prodtask-dev.cern.ch/ng/gp-deletion/data/DAOD_FTAG1) | [FTAG2 data](https://prodtask-dev.cern.ch/ng/gp-deletion/data/DAOD_FTAG2) |
| MC | [FTAG1 MC](https://prodtask-dev.cern.ch/ng/gp-deletion/mc/DAOD_FTAG1) | [FTAG2 MC](https://prodtask-dev.cern.ch/ng/gp-deletion/mc/DAOD_FTAG2) |

You can also see the list of datasets that will be [secondarized](https://adc-mon.cern.ch/lifetime-model/results/latest/beyond-lifetime-centrally-managed/)!

The lifetime of the datasets can be extended by different methods:

1. Using the RUCIO command line interface:
```bash
rucio add-lifetime-exception --inputfile INPUTFILE --reason REASON --expiration EXPIRATION
```
2. [RUCIO Web UI](https://rucio-ui.cern.ch/lifetime_exception)
3. Automatic extension of lifetime [link to gitlab project](https://gitlab.cern.ch/wwalko/drrsubscriptions#adding-automatic-extension-request-submission-for-obsolescence-mechanism-only)

## Obsolescence vs DDM lifetime model

Please have a look here: [Exception Requests](https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/DDMLifetimeModel#Exception_Requests).

- The obsolescence system affects only DAODs, and it is completely orthogonal to the [DDM lifetime model](https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/DDMLifetimeModel)!
- HITS files are also deleted under this lifetime model management.
- The CLI method is now the preferred method for requesting exceptions.
