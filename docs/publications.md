# Publications

## Noteworthy Papers


??? abstract "Flavour tagging and particle-physics related machine learning papers"


    | Title  | Preprint link |
    | ------ | ------------- |
    | Machine Learning in High Energy Physics: A review of heavy-flavor jet tagging at the LHC | [2404.01071](https://arxiv.org/abs/2404.01071) |
    | Uncertainty Aware Learning for High Energy Physics | [2105.08742](https://arxiv.org/abs/2105.08742) |
    | GNN for Deep Full Event Interpretation | [2304.08610](https://arxiv.org/abs/2304.08610) |
    | Topological Reconstruction of Particle Physics Processes using GNNs | [2303.13937](https://arxiv.org/abs/2303.13937) |
    | An Efficient Lorentz Equivariant Graph Neural Network for Jet Tagging | [2201.08187](https://arxiv.org/abs/2201.08187) |
    | Does Lorentz-symmetric design boost network performance in jet physics? | [2208.07814](https://arxiv.org/abs/2208.07814) |
    | Particle Transformer for Jet Tagging | [2202.03772](https://arxiv.org/abs/2202.03772) |
    | ParticleNet: Jet Tagging via Particle Clouds | [1902.08570](https://arxiv.org/abs/1902.08570) |


??? abstract "General machine learning papers"

    | Title  | Preprint link |
    | ------ | ------------- |
    | Robust and Provably Monotonic Networks | [2112.00038](https://arxiv.org/abs/2112.00038) |
    | Feature Set Embedding for Incomplete Data | [Grangier, David & Melvin, Iain. (2010)](https://proceedings.neurips.cc/paper/2010/file/5f0f5e5f33945135b874349cfbed4fb9-Paper.pdf) |
    | Attention is All you Need | [Vaswani, Shazeer, Parmar, Uszkoreit, Jones, Gomez, Kaiser, Polosukhin (2017)](https://papers.nips.cc/paper_files/paper/2017/file/3f5ee243547dee91fbd053c1c4a845aa-Paper.pdf) |
    

## ATLAS FTAG Public documents

Please refer to the 
[FlavourTaggingPublicResultsCollisionData](https://twiki.cern.ch/twiki/bin/view/AtlasPublic/FlavourTaggingPublicResultsCollisionData)
 twiki page for a complete list of ATLAS FTAG publications.



??? abstract "2023"

    | Title  | Type | Presenter | Conference |
    | ------ | ---- | --------- | ---------- |
    | [Flavour Tagging with Graph Neural Network with the ATLAS Detector]() | poster | Alexander Froch | EPS2023 |
    | [Boosted Higgs Boson Tagging with Transformers with the ATLAS Detector](https://cds.cern.ch/record/2867456) | poster | Jackson Barr | BOOST 2023 |
    | [Heavy flavour jet tagging algorithms in ATLAS](https://indico.cern.ch/event/1274182/contributions/5458302/) | talk | Philipp Gadow | CMS BTV workshop |
    | [Flavour tagging with the ATLAS detector at the HL-LHC](https://cds.cern.ch/record/2864274) | poster | Sebastien Rettie | LP2023 |
    | [Flavor tagging in ATLAS](https://cds.cern.ch/record/2865906) | talk | Bingxuan Liu | HHHWS2023 |
    | [Boosted flavour tagging algorithms in ATLAS](https://cds.cern.ch/record/2865904) | talk | Osamu Karkout | HHHWS2023 |
    | [(Heavy) Flavour Tagging in ATLAS and CMS](https://cds.cern.ch/record/2861546) | talk | Alexander Froch | HADRON2023 |
    | [Flavor tagging and boosted objects](https://cds.cern.ch/record/2857428) | talk | Thomas Strebler | LHCP2023 |
    | [Flavour Tagging with Graph Neural Network with the ATLAS Detector](https://arxiv.org/abs/2306.04415) | proceedings | Arnaud Duperrin | DIS2023 |
    | [Flavour Tagging with Graph Neural Network with the ATLAS Detector](https://cds.cern.ch/record/2852404) | talk | Arnaud Duperrin | DIS2023 |
    | [Flavour Tagging with Graph Neural Networks @ ATLAS](https://indico.cern.ch/event/1232499/) | seminar | Sam Van Stroud | EP-IT Data Science Seminars  |

??? abstract "2022"

    | Title  | Type | Presenter | Conference |
    | ------ | ---- | --------- | ---------- |
    | [Flavour tagging with the ATLAS and CMS at the HL-LHC](https://cds.cern.ch/record/2841199) | talk | Nilotpal Kakati | HIGGS2022 |
    | [DeXTer: Deep Sets based Neural Networks for Low-pT X->bbbar Identification in ATLAS](https://cds.cern.ch/record/2824878) | talk | Yuan-Tang Chou | BOOST2022 | 
    | [Graph Neural Network (GNN) based Truth-tagging Tool in ATLAS](https://cds.cern.ch/record/2824190) | poster | Lakmin Wickremasinghe | BOOST2022 |
    | [Recent Developments in ATLAS Flavor Tagging: Algorithm and Calibration](https://cds.cern.ch/record/2813305) | talk | Andy Chisholm | ICHEP2022 |
    | [New ATLAS b-tagging algorithm for Run-3](https://cds.cern.ch/record/2813621/) | poster | Martino Tanasini | ICHEP2022 |
    | [Jet Flavor Tagging Using Graph Neural Networks](https://indico.cern.ch/event/1103637/contributions/4821873/) | talk | Samuel Van Stroud, Nilotpal Kakati | Connecting the dots 2022 |
    | [New ATLAS b-tagging Algorithm for Run3](https://indico.cern.ch/event/1109611/contributions/4821348/) | poster | Alexander Froch | LHCP 2022 |

??? abstract "2021"

    | Title  | Type | Presenter | Conference |
    | ------ | ---- | --------- | ---------- |
    | [Dips based b-Tagging](https://indico.cern.ch/event/1091653/) | poster | Alexander Froch | LHCC open session 2021 |
    | [b-tagging perf. at HL-LHC](https://indico.cern.ch/event/1091653/contributions/4606898/attachments/2346293/4001013/ATL-COM-PHYS-2021-939.pdf) | poster | Neelam Kumari | LHCC open session 2021 |
    | [Jet flavour tagging for the ATLAS Experiment](https://cds.cern.ch/record/2779117) | poster | Martino Salomone Centonze | PANIC 2021 |
    | [Jet flavour tagging for the ATLAS Experiment](https://cds.cern.ch/record/2779978) | talk | Alexander Khanov | ICNFP2021 |
    | [Jet flavour tagging for the ATLAS Experiment](https://cds.cern.ch/record/2782011) | talk | Jonathan Shlomi | SUSY2021 |
    | [Flavour-tagging / Jet / Met performance in ATLAS and CMS](https://cds.cern.ch/record/2766930) | talk |  Jonathan Bossio | LHCP 2021 |

??? abstract "2020"

    | Title  | Type | Presenter | Conference |
    | ------ | ---- | --------- | ---------- |
    | [ML for jet tagging and event classification (ATLAS+CMS)](https://cds.cern.ch/record/2722115) | talk | Michael Kagan | DM@LHC2020  |
    | [Tracking and flavor-tagging performance at ATLAS](https://cds.cern.ch/record/2727841) | talk | Carlo Varni | ICHEP2020 |
    | [Tracking, alignment, and flavor-tagging performance in ATLAS and CMS](https://cds.cern.ch/record/2719882) | talk | Salvador Marti I Garcia | LHCP2020 |
    | [Tracking, alignment, and flavor-tagging performance in ATLAS and CMS](https://cds.cern.ch/record/2719570) | poster | Manuel Guth | LHCP2020 |
    | [Machine Learning for identifying heavy-flavor jets with the ATLAS detector](https://cds.cern.ch/record/2706702) | talk | Philipp Windischhofer | ML4Jets2020 |
    | [Flavour-tagging performance with the ATLAS detector](https://cds.cern.ch/record/2707367) | talk | Bingxuan Liu | FCC2020 |

??? abstract "Older"

    | Title  | Type | Presenter | Conference |
    | ------ | ---- | --------- | ---------- |
    | [Flavour tagging at ATLAS](https://cds.cern.ch/record/2691592) | talk | Chris Pollard | CMSFTag |
    | [Machine learning for flavour tagging in ATLAS](https://cds.cern.ch/record/2654335) | talk | Geoffrey Gilles | CLICWEEK2019 |
    | [Flavor tagging](https://cds.cern.ch/record/2319762) | talk | Valerio Dao | Higgs Toppings |
    | [Deep Neural Network based tagger for Flavour Tagging at the ATLAS experiment](https://cds.cern.ch/record/2657668) | poster | Manuel Guth | LHCC 2019 |
    | [Deep-neural-network based b-tagging as basis for improvements in top analyses (YSF)](https://cds.cern.ch/record/2693088) | talk | Manuel Guth | Top2019 |
    | [Tagging of jets initiated by individual b/c-quarks or b-quark pairs, and soft b-tagging techniques in SUSY searches in ATLAS](https://cds.cern.ch/record/2684462) | talk | Ruth Magdalena Jacobs | BOOST2019 |
    | [Soft b-hadron tagging with the ATLAS detector](https://cds.cern.ch/record/2683956) | poster | Tomohiro Yamazaki | EPS-HEP2019 |
    | [b-tagging in ATLAS](https://cds.cern.ch/record/2646592) | talk | Matthew Carl Feickert | MLJets2018 |
    | [Tracking, alignment and b-tagging performance and prospects in ATLAS](https://cds.cern.ch/record/2319915) | talk | Nicholas Styles | LHCP2018 |
    | [b-tagging in ATLAS and CMS](https://cds.cern.ch/record/2632466) | talk | Alessandro Calandri | Top LHC |
    | [Machine Learning Algorithms for b-jet tagging](https://cds.cern.ch/record/2280861) | talk | Michela Paganini | ACAT2017 |
    | [Identification of Ultra-Boosted Higgs-->bb Jets Using Subjet B-Tagging](https://cds.cern.ch/record/2276641) | poster | Samuel Meehan | BOOST2017 |
    | [Algorithmic improvements and calibration measurements for flavour tagging](https://cds.cern.ch/record/2274067) | talk | Marco Battaglia | EPS-HEP 2017 |
    | [Optimisation of the b-tagging algorithms for the 2017 LHC data-taking](https://cds.cern.ch/record/2276362) | poster | Francesco Di Bello | EPS-HEP 2017 |
    | [Topological b-hadron decay reconstruction and application for heavy-flavour jet tagging](https://cds.cern.ch/record/2275033), poster | Geoffrey Gilles | EPS-HEP 2017 |
    | [The secondary vertex finding algorithm](https://cds.cern.ch/record/2276698) | poster | Sebastian Heer | EPS-HEP 2017 |
    | [Deep Learning in Flavour Tagging](https://cds.cern.ch/record/2274065) | poster | Marie Lanfermann | EPS-HEP 2017 |
    | [The Soft Muon Tagger for the identification of b-jets](https://cds.cern.ch/record/2274254) | poster | Andrea Sciandra | EPS-HEP 2017 |


## Theses on flavour tagging
| Title  | Date | Student |  Type |
| ------ | ---- | ------- | ----- |
| [Graph Neural Network Flavour Tagging and Boosted Higgs Measurements at the LHC ](https://discovery.ucl.ac.uk/id/eprint/10173308/).  |      | Van Stroud, Sam | PhD |
| [Deep Neural Networks applications in experimental physics data analyses](https://cds.cern.ch/record/2862569) | June 2023 | Pires, João |Master |
| [Identification of bb-Jets Using a Deep-Sets-Based Flavour-Tagging Algorithm at the ATLAS Experiment](https://cds.cern.ch/record/2864615) | Dec 2022 | Birk, Joschka | Master |
| [Charming Higgs bosons: constraint on the Higgs-charm coupling from a search for Higgs boson decays to charm quarks with the ATLAS detector](https://cds.cern.ch/record/2803924) | March 2022 | Stamenkovic, Marko | PhD |
| [Search for tt H(H→bb) Production in the Lepton + Jets Channel and Quark Flavour Tagging with Deep Learning at the ATLAS Experiment](https://cds.cern.ch/record/2765038) | March 2021 | Guth, Manuel | PhD |
| [Trackless flavour-tagging at high jet transverse momentum and its impact on the physics reach of the ATLAS experiment at LHC](https://cds.cern.ch/record/2760095) | Oct 2020 | Tanasini, Martino | Master |


## CMS results

- [B-tagging performance of the CMS Legacy dataset 2018](https://cds.cern.ch/record/2759970)
- [Jet Flavour Classification Using DeepJet](https://arxiv.org/abs/2008.10519)
- [Heavy flavour identification at CMS with deep NN (public plots)](https://cds.cern.ch/record/2255736)
- [Run 2 results (c-tagging): Identification of c-quark jets with the CMS experiment](http://cms-results.web.cern.ch/cms-results/public-results/preliminary-results/BTV-16-001/index.html)
- [Run 2 results: Identification of b-quark jets with the CMS experiment in the LHC Run 2](http://cms-results.web.cern.ch/cms-results/public-results/preliminary-results/BTV-15-001/index.html)
- [Run 1 results: Identification of b-quarks with the CMS experiment](http://cms-results.web.cern.ch/cms-results/public-results/publications/BTV-12-001/index.html)
