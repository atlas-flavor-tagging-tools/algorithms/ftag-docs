# Trackless-b-tagging activities


## Introduction

Track-less b-tagging uses information about hits (or their absence) on detector layers to infer the presence of b-jets.


## Reconstruction tags 

We have used the following r-tags for AODs:

### mc23d (latest samples)
- [`r15310`](https://ami.in2p3.fr/?subapp=tagsShow&userdata=r15310): saves hits with dR(hit,jet)<0.2 and pt(jet)>250 GeV (used for Z')
- [`r15328`](https://ami.in2p3.fr/?subapp=tagsShow&userdata=r15328): saves hits with dR(hit,jet)<0.2 and pt(jet)>20 GeV (used for ttbar)

### mc20 (latest samples)
- [`r13258`](https://ami.in2p3.fr/?subapp=tagsShow&userdata=r13258): saves hits with dR(hit,jet)<0.4 and pt(jet)>300 GeV
- [`r14736`](https://ami.in2p3.fr/?subapp=tagsShow&userdata=r14736): saves hits with dR(hit,jet)<0.4 and pt(jet)>20 GeV


## Derivation tags 

We have used the following p-tags for DAODs:

- [`p6023`](https://ami.in2p3.fr/?subapp=tagsShow&userdata=p6023): should be used for trackless AODs

## Recommended samples for trackless-b-tagging studies

| name        | h5     | DxAOD  | AOD      | comments |
| ----------- | ------ | ------ |--------- | -------- |
| extended Z' | `user.rkusters.800030.e8514_e8528_s4159_s4114_r15310_r15319_p6023.tdd.TracklessEMPFlow.24_2_37.24-03-05_mc23d_output.h5` | `mc23_13p6TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.merge.DAOD_FTAG1.e8514_e8528_s4159_s4114_r15310_r15319_p6023` | `mc23_13p6TeV:mc23_13p6TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.recon.AOD.e8514_s4159_r15310` | Hits in h5 files are saved in a cone of dR=0.1 around the jet axis. |
| ttbar | `user.rkusters.601589.e8549_e8528_s4159_s4114_r15328_p6023.tdd.TracklessEMPFlow.24_2_37.24-03-07_mc23d_output.h5` | `mc23_13p6TeV.601589.PhPy8EG_A14_ttbar_hdamp258p75_nonallhadron.merge.DAOD_FTAG1.e8549_e8528_s4159_s4114_r15328_p6023`| `mc23_13p6TeV.601589.PhPy8EG_A14_ttbar_hdamp258p75_nonallhadron.merge.DAOD_FTAG1.e8549_e8528_s4159_s4114_r15328` | Hits in h5 files are saved in a cone of dR=0.1 around the jet axis. |


You can download the datasets from the grid using `rucio`: [ATLAS software tutorial about grid downloads with `rucio`](https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/rucio_download_files/).

## Information for Plotting
Some scripts exist for the plotting of Hit/Track Variables. They are located in a [git repo](https://gitlab.cern.ch/rkusters/hitscripts). Feedback/Comments/Ideas are appreciated!


## Adding Hit Information

This section documents the instructions for creating FTAG1 derivation that stores hit information. 

It contains two steps, the first is to generate AOD sample from HITS file, the second is to generate the FTAG1 derivation from the AOD.

A detailed description of the task can be found in [AFT-579](https://its.cern.ch/jira/browse/AFT-579) and [AFT-572](https://its.cern.ch/jira/browse/AFT-572?focusedCommentId=4038020&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-4038020).

#### HITS &rarr; AOD 

First an AOD that includes hit information needs to be created.

- To create an AOD with 100 events from a HITS file the following command can be used. 

- The RDO files need to be downloaded locally with `rucio`.

- `Reco_tf.py` used below is the ATLAS general purpose reconstruction transform. It is able to process RAW and RDO through to AODs.

??? abstract "Example for HITS to AOD conversion"
    ```
    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -c x86_64-centos7-gcc11-opt
    asetup --platform=x86_64-centos7-gcc11-opt Athena,22.0.104
    Reco_tf.py \
    --inputHITSFile="HITS.24937557._019634.pool.root.1" \
    --asetup="RDOtoRDOTrigger:Athena,21.0.20.12" \
    --multithreaded="True" \
    --postInclude "default:PyJobTransforms/UseFrontier.py" "all:PyJobTransforms/DisableFileSizeLimit.py" \
    --preExec "all:flags.BTagging.Trackless = True; from InDetPrepRawDataToxAOD import InDetDxAODJobProperties; InDetDxAODJobProperties.DumpTruthInfo=True; " \
    --preInclude "all:Campaigns/MC20a.py" \
    --skipEvents="0" --autoConfiguration="everything" \
    --conditionsTag "default:OFLCOND-MC16-SDR-RUN2-09" "RDOtoRDOTrigger:OFLCOND-MC16-SDR-RUN2-08-02a" \
    --geometryVersion="default:ATLAS-R2-2016-01-00-01" \
    --runNumber="800030" \
    --digiSeedOffset1="18231" \
    --digiSeedOffset2="18231" \
    --AMITag="r14233" \
    --outputAODFile="AOD.24937557._019634.pool.root.1" \
    --jobNumber="18231" \
    --steering "doRAWtoALL" \
    --triggerConfig="RDOtoRDOTrigger=MCRECO:dbf:TRIGGERDBMC:2283,35,327" \
    --inputRDO_BKGFile="RDO.26835812._026569.pool.root.1,RDO.26835812._026570.pool.root.1,RDO.26835812._026571.pool.root.1,RDO.26835812._026572.pool.root.1,RDO.26835812._026573.pool.root.1"
    --maxEvents="100" 
    ```



Jet and BTagging containers are built at reconstruction level but not saved in an AOD by default. If you want to save such containers, the following `preExec` should be included (in CA mode):

```
--preExec "all:ConfigFlags.Jet.WriteToAOD=True;"
```

Please note that `--AMITag` argument takes care of certain redundantly supplied arguments such as `triggerConfig`, `geometryVersion`, `conditionsTag`, possibly others.
The extra arguments should only be supplied if you want to test something non-standard. Often, certain non-standard arguments are incompatible together.

One can also use, say, `--AMIConfig r14159` to bypass passing the extra arguments; the corresponding tag info can be looked up [here](https://atlas-ami.cern.ch/?subapp=tagsShow&userdata=r14159) on the (A)tlas (M)etadata (I)nterface. 
For a template, please have a look [here](https://gitlab.cern.ch/atlas/athena/-/blob/24.0/PhysicsAnalysis/JetTagging/FlavourTaggingTests/test/test_FTAG_AOD_withtrackless_MC_grid.sh)

The `flags.BTagging.Trackless = True` setting in the `--preExec` adds the hit information (Pixel+SCT) to the AOD. Additional settings (e.g. the pT threshold for the jet-hit association can be set following the definitions in [BTaggingConfigFlags.py](https://gitlab.cern.ch/atlas/athena/-/blob/master/PhysicsAnalysis/JetTagging/JetTagConfig/python/BTaggingConfigFlags.py#L92)).  

The `InDetDxAODJobProperties.DumpTruthInfo=True` setting is necessary for saving the `truth_barcode` information for the hits.


#### AOD &rarr; FTAG1 derivation 

To create an FTAG1 derivation file from the file created above, the following commands can be used

```bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup Athena,25.0.6
export ATHENA_PROC_NUMBER=8
Derivation_tf.py --CA --inputAODFile input.AOD.pool.root --outputDAODFile output.pool.root --formats FTAG1
```

#### Running derivations on the grid for testing
Please do not use this method to produce derivations for full datasets, only use it for testing purposes.

```bash
setupATLAS -q
voms-proxy-init --voms atlas
lsetup rucio
lsetup panda
lsetup pyami
pathena --trf "Derivation_tf.py --CA --inputAODFile=%IN --outputDAODFile=%OUT.pool.root --maxEvents=5000 --skipEvents=0 --formats=PHYSLITE" \
 --inDS mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_s3681_r12960_r12963 \
 --outDS user.username.410470.PHYSLITE_CA_v2 
```
