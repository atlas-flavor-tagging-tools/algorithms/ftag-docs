# Ongoing activities within the Algorithm group

This section contains a list of tasks and ongoing activities for the optimisation of the b-tagging performance.
For information and expression of interest please contact the algorithm and FTAG conveners as well as people involved in each of the efforts. A detailed status of the various activities with results and slides shown at past Algorithm subgroup meetings is also provided for the ongoing efforts (no guarantee of completeness).


## Overview of all activities

| Activity | People involved | Material |
| ---------- | -------------- | ----------|
| GN2 | Nikita Pond, Samuel Van Stroud, Jackson Barr, Maxence Draguet | [link][gn2-tagger] |
| Extended labelling | Neelam Kumari, Tomke Schröer| [link](../labelling/jet_labels.md) |
| GN1 | Samuel Van Stroud, Nilotpal Kakati, Sebastien Rettie, Jackson Barr, Nikita Pond | [link][gn1-tagger] |
| DL1d r22 | Maxence Draguet | [link][dl1-tagger] |
| DIPS r22 | Alexander Froch |[link][dips-tagger] |
| MC-MC maps | Yusong Tian, Dmitrii Kobylianskii, Theo Maurin | [link](mcmc.md) |
| Data/MC comparisons| Marco Cristoforetti, Greta Brianti, Daniela Mascione | |
| Trackless b-tagging | Todd Huffman, Spyros Argyropoulos, Andrea Coccaro, Martino Tanasini, Roman Kuesters | [link](trackless.md) |
| PFlow inputs | Jackson Barr | |
| Soft electron tagger | Dmitrii Kobylianskii, Etienne Dreyer, Francesco Armando Di Bello, Robin Junwen Xiong, Tomohiro Yamazaki | [link](low-level-opt.md) |
| Soft muon tagger | Frederic Renner | [link](low-level-opt.md) |
| Truth tagging | Nilotpal Kakati, Francesco Armando Di Bello | |
| Large Radius tracking | Alexander Khanov, Soumyananda Goswami, Bingxuan Liu, Yohei Yamaguchi, Yunjian He | [link](lrt.md) |
| Track classification | Roman Küsters, Martino Salomone Centonze, Samuel Van Stroud | [docs](track_classification.md) |
| Charm tagging | Tomohiro Yamazaki, Zhuoran Feng, Marko Stamenkovic, Fabrizio Parodi  | |
| Secondary Vertexing algorithms | Vadim Kostyukhin, Katharina Voss, Yifan Yang, Martino Tanasini  | [docs](../taggers/sv.md) |
| b-tagging with VR Track jets| Daniela Mascione, Frederic Renner, Maxence Draguet, Jackson Barr, Sankalp Tipnis | |
| bb-rejection | Maggie Chen (DL1*) | |
| b-tagging for upgrade | Alexander Khanov, Thomas Strebler, Arnaud Duperrin, Maxime Fernoux, Jacob Edwin Crosby, Neelam Kumari | [docs](upgrade.md)|
