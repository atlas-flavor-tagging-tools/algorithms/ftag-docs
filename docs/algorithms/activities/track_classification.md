# Track classification tools

Several tools can be used to discriminate tracks based on their supposed origin.
Typical categories used for the classification of tracks are

- tracks originating from b-jets, c-jets, or light-jets
- tracks originating from fragmentation
- tracks originating from pile-up
- fake tracks


The following list contains supported track selection tools, which use multivariate information for the classification of tracks:

- Track Classification Tool (TCT)
- Fake Track Athena Tool (FTA)

A gitlab project is provided which contains these tools bundled together with the training-dataset-dumper to run them over tracks associated with jets and dump the output to h5 files.

The project is located here: <https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/track-classification-studies/-/tree/master>

<details><summary>Click to expand</summary>


**How to clone the code, set it up& run it**

To clone the code from gitlab and the submodules use the following command:

```bash
git clone --recursive ssh://git@gitlab.cern.ch:7999/atlas-flavor-tagging-tools/algorithms/track-classification-studies.git
```

The `source` directory and a `setup.sh` file are downloaded.

With

`source setup.sh`

you create a build and run directory. Furthermore athena version 22.0.60 is set up, the code is build and further dependecies are set up.
To finally run the code use the comand

`ca-dump-retag -c <path to configuration file> <paths to DxAOD(s)>`

</details>



## FakeTrack Athena

The FakeTrackAtheExample (FTA) uses a neural network (inference is done using [lwtnn](https://github.com/lwtnn/lwtnn)) to calculate for each track a weight, on which can be determined if the track is fake or not. Furthermore a true positive rate/false positive rate is provided (based on a "true/false" fake calculation of the FTA with the truthMatchProbability).

To implement the FTA in the [training-dataset-dumper](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/) (TDD) it is necessary first to change the athena version on which the FTA is compiled. 
Furthermore the FTA & TDD use a different way to associate tracks to jets (ghost association(FTA) vs BTagTrackToJetAssociation(TDD)) and have a different jet&track selection. To fully use the FTA in the training files of another neural network further down the processing pipeline (like DIPS), it is therefore necessary to change the way of track to jet association and remove the track & jet selection in the FTA. 

As the result of running FTA, each track is "decorated" with the neural network weight and a boolean value `passFake` which can be `true` or `false`, based on the cut value.

The default cut value for passFake is `0.1`.

Gitlab project [link](https://gitlab.cern.ch/svanstro/FakeTrackAthExample)



## Track Classification Tool

The TrackClassificationTool (TCT) is a trained Boosted Decision Tree, caclulating for each track 3 different output classifiers. These different output classifiers correspond to the following regions

- Signal: tracks coming from a b-jet -> tracks coming from b-hadron decay products
- Fragmentation: tracks originating from light jets -> tracks from fragmentation (identification: no HF and no garbage tracks)
- Garbage:  tracks corresponding to Pile Up jets -> tracks corresponding to Pile-Up tracks and tracks produced by GEANT (long lived particle decay and material interaction)

To implement the TCT into the training-dataset-dumper it is necessary to call the TCT as a function with the selected tracks of the training-dataset-dumper. 
Since the TCT uses some further cuts than the training-dataset-dumper, these cuts are removed within the TCT.

As the result of scheduling the TCT, each track is decorated with the 3 weights of the TCT.  

Please refer to the [TCT TWiki entry](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TrackClassificationTool) for further information.


## Neural Network Track Classification (NNTC)


- Use a NN model for track classification, according to track- categories defined in the `InDetTrackTruthOriginTool`.

- Select HF tracks inside a jet to be fed into flavour tagging tools
    - with higher efficiency than the shrinking cone (track-association)
    - Reject/accept fake-tracks (the latter being useful for creating rich fake tracks CRs).


More info in [this talk](https://indico.cern.ch/event/1131635/#6-multi-class-track-classifica)



The available taggers are listed under <https://ftag.docs.cern.ch/algorithms/available_taggers/#track-based-taggers>.