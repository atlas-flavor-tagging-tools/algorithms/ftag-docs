# Exclusive c-tagging using the reconstructed c-hadron decay $D^{+}\to K^{−} \pi^{+} \pi^{+}$

This part is based on QT of Zhuoran Feng. Detailed materials can be found on JIRA: [AFT-528](https://its.cern.ch/jira/browse/AFT-528).

## Introduction

$c$-hadron are formed at the Primary Vertex (PV), and decay at the Secondary Vertex (SV). There are several low-level algorithms reconstructing the SV kinematics, including SV1, JetFitter, etc. This new reconstruction is based on the JetFitter method and aims at improve the SV precision for better c-tagging performance.

## Reconstruction
JetFitter does not identify the type of track particles inside a jet, and all the tracks are asigned $\pi$ mass. As a result, while selecting $D^{+}\to K^{-}\pi^{+}\pi^{+}$ vertices, the mass distribution is shifted to a lower position, and the resolution of the peak is not ideal.

- Fig 1. Default JetFitter SV distribution
![Fig 1. Default JetFitter SV distribution](../../assets/exclusive-ctagging/DefaultSVMass.png)

The reconstruction, however, can give the charge correlation between the vertex and the tracks. This information can help finding the Kaon track in the decay. Assigning Kaon mass to this track with opposite charge w.r.t. the SV, an improved SV mass distribution can be expected.

- Fig 2. Improved SV mass distribution
![Fig 2. Improved SV mass distribution](../../assets/exclusive-ctagging/CorrectedSVmass.png)

## Test in $c$-tagger training

The reconstruction is used as an add-on variable to the inclusive charm-tagger training. The training is done with [Umami](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami) framework, and detailed settings can be found in the [QT report](https://indico.cern.ch/event/1123953/contributions/4736957/attachments/2393118/4091362/ctaggingImprovement-ZFeng.pdf).

The add-on variable, `ctag-decorator` (temporary name), is set to a default value same as the `JetFitterSecondaryVertex_mass` when the reconstruction of $D^{+}\to K^{-}\pi^{+}\pi^{+}$ is failed. If the jet has a SV that pass the reconstruction, the improved SV mass (where the Kaon track has the correct mass) is assigned. The training results are shown in the following plots. From the training, $b$-jet rejection is improved by 1-5\%, depending on the $c$-jet efficiency. The light-jet rejection is improved by 5-10\%.

- Fig 3. b- and light-jet rejection vs c-efficiency
![Fig 3. b- and light-jet rejection vs c-efficiency](../../assets/exclusive-ctagging/Rejection.png)

- Fig 4. c-tagging rate vs pT
![Fig 4. c-tagging rate vs pT](../../assets/exclusive-ctagging/EffVsPt.png)

## Implementation in Athena

The add-on variable is implemented into Athena software for uses in rel 22. See Merge Request: [!50798](https://gitlab.cern.ch/atlas/athena/-/merge_requests/50798).

Reconstructed channels in this implemention are enhanced:

1. $D^{+}\to K^{-}\pi^{+}\pi^{+}$
2. $D^{0}\to K^{-}\pi^{+}$
3. $D^{0}\to K^{-}\pi^{+}\pi^{-}\pi^{+}$

The main change of this MR is the `BTagJetAugmenter` container in the [FlavorTagDiscriminants](https://gitlab.cern.ch/zfeng/athena/-/tree/22.0.48-ctag-newvar/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/Root) package. Two variables are added here:

- `JetFitterSecondaryVertex_DmesonMass`: an improved SV mass using D meson reconstruction to increase the resolution.
- `JetFitterSecondaryVertex_isDmesonRecon`: a boolean to judge if the vertex passed the D meson reconstruction.

The variables are added only to the BTagging [ExpertContent](https://gitlab.cern.ch/zfeng/athena/-/blob/22.0.48-ctag-newvar/PhysicsAnalysis/DerivationFramework/DerivationFrameworkFlavourTag/python/BTaggingContent.py#L90). In the MR, this expert content is turned on for PFlow and VRTrack jets in the production of PHYS derivation. If other variables are to be added, the same process should work:

1. Step 1: Define and compute the variable in `BTagJetAugmenter`.
2. Step 2: Add the variable to the list `BTaggingHighLevelAux` or other correlated lists in the [BTaggingContent](https://gitlab.cern.ch/zfeng/athena/-/blob/22.0.48-ctag-newvar/PhysicsAnalysis/DerivationFramework/DerivationFrameworkFlavourTag/python/BTaggingContent.py).
3. Step 3: Test producing PHYS derivation to see if the implementation is successful.

- Fig 5. Distribution of the reconstructed D meson mass
![Fig 5. Distribution of the reconstructed D meson mass](../../assets/exclusive-ctagging/DmesonMass.png)

- Fig 6. Distribution of the boolean "isDmesonRecon"
![Fig 6. Distribution of the boolean "isDmesonRecon"](../../assets/exclusive-ctagging/isDmesonRecon.png)
