# Jet Labels

Some older information about the jet labels is available on the [Twiki](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/FlavourTaggingLabeling)

## Hadron-based labelling

Hadron based jet flavour labelling is commonly used in FTAG.
There are two different schemes for hadron-based labelling: delta-R matching and ghost matching.
For each scheme, additional information about the labelling particles is available, see [here](https://training-dataset-dumper.docs.cern.ch/vars_pflow/) for a complete list of saved variables.

### delta-R matching

!!!info "delta-R matching is the default scheme in FTAG"

The standard labelling is provided via the `HadronConeExclTruthLabelID` variable while an extended jet labelling is available via the `HadronConeExclExtendedTruthLabelID` variable. In comparison to the standard labelling the extended jet labelling `HadronConeExclExtendedTruthLabelID` also allows double matches to one jet. For matching all weakly-decaying b-/c-hadrons and tau-leptons with a transverse momentum larger than 5 GeV and all calo (track) jets with an uncalibrated transverse momentum of at least 0 (5) GeV are found. A hadron/tau-lepton is matched with a jet if the angular distance DeltaR is smaller than 0.3. In case of multiple jets laying in this cone the closest one is chosen. Afterwards all hadrons/tau-leptons that are daughter particles of another hadron/tau-lepton are removed. The remaining ones define the label of the jet. 

For the `HadronConeExclTruthLabelID` only the closest hadron/tau-lepton matched with the jet is used for the label. This means that the standard label combines the extended labels `5`, `54` and `55` into `5` and `4`and `44` into `4`. 

| value               | HadronConeExclExtendedTruthLabelID | HadronConeExclTruthLabelID |
| ------------------- | -----------------------------------| ---------------------------|
| 0                   | light jets                         | light jets                 |
| 4                   | single c-jets                      | inclusive c-jets           | 
| 5                   | single b-jets                      | inclusive b-jets           | 
| 15                  | tau-jets                           | tau-jets                   | 
| 44                  | double c-jets                      | -                          |
| 54                  | b-jets with c-hadron               | -                          |
| 55                  | double b-jets                      | -                          |
| 151511              | tautau, hadronic + electron decay  | -                          |
| 151513              | tautau, hadronic + muon decay      | -                          |
| 1515                | tautau, hadronic decay             | -                          |
| -99                 | jet failed pT cut                  | -                          |

The computation of the labels by using the [`ParticleJetDeltaRLabelTool`](https://gitlab.cern.ch/atlas/athena/-/blob/21.2/PhysicsAnalysis/AnalysisCommon/ParticleJetTools/Root/ParticleJetDeltaRLabelTool.cxx) can be found [here](https://gitlab.cern.ch/atlas/athena/-/blob/21.2/PhysicsAnalysis/AnalysisCommon/ParticleJetTools/python/DefaultTools.py#L64).  

### Ghost matching

!!!tip "Ghost matching is being investigated for use in upcoming taggers"

Instead of using the DeltaR matching it is also possible to use ghost associated hadrons for the jet labelling.
The corresponding labels are `HadronGhostTruthLabelID` and `HadronGhostExtendedTruthLabelID`.

???info "What is ghost matching?"
    For the ghost matching all hadrons are treated as 4-momentum vectors with an infinitesimally small magnitude.
    Afterwards the jet clustering is performed.
    This way the hadrons do not have a direct effect on the final result of the clustering.
    After the jets are clustered the 4-momenta are restored.


## Parton-based labelling

Instead of using hadron to label the jet, it is possible to use partons instead.
For parton-based labelling, only a ghost matching scheme is available with the `PartonTruthLabelID` label.
Parton based labelling 
For the definition of `PartonTruthLabelID`, the ghost matched partons associated with the corresponding jet are used and the parton with the largest energy is used to label the jet.
The `PartonTruthLabelID` can be used to distingush not only light, b- and c-jets but also the different light jets.

| PartonTruthLabelID | Category     |
| ------------------ | -------------|
| 1                  | d-jets       |
| 2                  | u-jets       |
| 3                  | s-jets       |
| 4                  | c-jets       |
| 5                  | b-jets       |
| 21                 | gluon jets   |


## Leptonic decay label

In order to investigate the leptonic decays of a b-hadron the `LeptonDecayLabel` provides information about the decay of the b-hadron and the decay of the resulting c-hadron in b-jets. The hadrons are associated with the jet using the DeltaR matching. The `LeptonDecayLabel` counts the electrons and muons from the b- and c-hadron decay. 1s in the `LeptonDecayLabel` represent electrons, 2s muons and 3s tau-leptons. The number of the corresponding lepton is added as soon as at least one lepton is part of the decay. In case of multiple electrons (muons, taus) in one of the decays the corresponding number of the electron (muon, taus) is only added once. This way double counting due to, for example, electrons bremming a photon and creating a "new" electron is avoided. This leads to the following labels:

| LeptonDecayLabel | Category                                                                           |
| ---------------- | ---------------------------------------------------------------------------------- |
| 0                | all hadronic b- and c-decays                                                       |
| 1                | electrons in either b- or  c-decay                                                 |
| 2                | muons in either b- or  c-decay                                                     |
| 3                | taus in either b- or  c-decay                                                      |
| 11               | electrons in both decays                                                           |
| 12               | electrons and muons in either b- or c-decay (not necessarily both in same decay)    |
| 13               | electrons and taus in either b- or c-decay (not necessarily both in same decay)     |
| 22               | muons in both decays                                                               |
| 23               | muons and taus in either b- or c-decay (not necessarily both in same decay)         |
| 33               | taus in both decays                                                                |
| 112              | electrons in both decay, muons in either b- or c-decay                              |
| 113              | electrons in both decay, taus in either b- or c-decay                               |
| 122              | muons in both decay, electrons in either b- or c-decay                              |
| 123              | all three leptons in either b- or c-decay (not necessarily both in same decay)      |
| 133              | taus in both decay, electrons in either b- or c-decay                               |
| 223              | muons in both decay, taus in either b- or c-decay                                   |
| 233              | taus in both decay, muons in either b- or c-decay                                   |
| 1122             | electrons and muons in both decays                                                 |
| 1123             | electrons in both decays, muons and taus in either b- or c-decay (not necessarily both in same decay)|
| 1133             | electrons and taus in both decays                                                  |
| 1223             | muons in both decays, electrons and electrons and taus in either b- or c-decay (not necessarily both in same decay)|
| 1233             | taus in both decays, electrons and electrons and muons in either b- or c-decay (not necessarily both in same decay)|
| 2233             | muons and taus in both decays                                                     |
| 11223            | electrons and muons in both decays, taus in either b- or c-decay                    |
| 11233            | electrons and taus in both decays, muons in either b- or c-decay                    |
| 12233            | muons and taus in both decays, electrons in either b- or c-decay                    |
| 112233           | all three leptons in both decays                                                   |


Other than the `HadronConeExclTruthLabelID`, `HadronConeExclExtendedTruthLabelID`, and `PartonTruthLabelID`, the `LeptonDecayLabel` is computed on-the-fly when dumping training samples with the [training-dataset-dumper](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper).

## HadronConeExclTruthLabelPdgId
PDGID definitions of the labelling particle used for HadronConeExclTruthLabelID. Particles are given by positive numbers, and if corresponding anti-particle exists it is denoted by negative numbers. The definitions are taken from [MC Particle Numbering Scheme](https://pdg.lbl.gov/2023/reviews/rpp2023-rev-monte-carlo-numbering.pdf).


| HadronConeExclTruthLabelPdgId | Category                  | HadronConeExclTruthLabelID |
| ----------------------------- | --------------------------| ---------------------------| 
|   0                           | light-quarks              | light-jets                 |
|   541                         | B meson (cb̅)              | inclusive b-jets           | 
|   531                         | B meson (sb̅)              | inclusive b-jets           | 
|   521                         | B meson (ub̅)              | inclusive b-jets           |
|   511                         | B meson (db̅)              | inclusive b-jets           |
|   551                         | bb̅ meson (𝜂-b)            | inclusive b-jets           |
|   553                         | bb̅ meson (𝛾)              | inclusive b-jets           |
|   555                         | bb̅ meson (Xb2)            | inclusive b-jets           |
|   5122                        | bottom baryons (udb)      | inclusive b-jets           |
|   5132                        | bottom baryons (ddb)      | inclusive b-jets           |
|   5232                        | bottom baryons (usb)      | inclusive b-jets           |
|   5332                        | bottom baryons (ssb)      | inclusive b-jets           | 
|   431                         | D meson (cs̄)              | inclusive c-jets           | 
|   421                         | D meson (cū)              | inclusive c-jets           | 
|   411                         | D meson (cđ)              | inclusive c-jets           | 
|   441                         | cc̄ meson (𝜂-c)            | inclusive c-jets           |
|   443                         | cc̄ meson (J/ψ)            | inclusive c-jets           |
|   445                         | cc̄ meson (Xc2)            | inclusive c-jets           |
|   4122                        | charmed baryons (udc)     | inclusive c-jets           |
|   4132                        | charmed baryons (dsc)     | inclusive c-jets           |
|   4232                        | charmed baryons (usc)     | inclusive c-jets           |
|   4332                        | charmed baryons (ssc)     | inclusive c-jets           |
|   4322                        | charmed baryons (usc)     | inclusive c-jets           |
|   15                          | tau-quarks (τ)            | tau-jets                   |

## ParticleOutCome label
For the tau-jets, another set of labels is available (if dumped). This is based on the truth particle [`ParticleOutCome`](https://gitlab.cern.ch/atlas/athena/-/blob/main/Generators/TruthUtils/TruthUtils/TruthClasses.h#L105). It gives the decay of the tau-lepton. With this, it can be differentiated between leptonically and hadronically decaying taus as well as the pronginess of the tau decay. The following table is taken from [here](https://gitlab.cern.ch/atlas/athena/-/blob/main/Generators/TruthUtils/TruthUtils/TruthClasses.h#L105-L118)

| ParticleOutCome | Category          |
| --------------- | ----------------- |
| 0               | NonDefinedOutCome |
| 1               | UnknownOutCome    |
| 2               | UnConverted       |
| 3               | Converted         |
| 4               | NonInteract       |
| 5               | NuclInteraction   |
| 6               | ElectrMagInter    |
| 7               | DecaytoElectron   |
| 8               | DecaytoMuon       |
| 9               | OneProng          |
| 10              | ThreeProng        |
| 11              | FiveProng         |