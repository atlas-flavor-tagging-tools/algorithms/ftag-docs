The tracks are decorated with truth information by the `FlavorTagDiscriminants::TrackTruthDecoratorAlg` ([`TrackTruthDecoratorAlg.h`](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/FlavorTagDiscriminants/TrackTruthDecoratorAlg.h) | [`TrackTruthDecoratorAlg.cxx`](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/src/TrackTruthDecoratorAlg.cxx)).

The truth particles which are used to decorate tracks with truth information are decorated by the `FlavorTagDiscriminants::TruthParticleDecoratorAlg` ([`TruthParticleDecoratorAlg.h`](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/FlavorTagDiscriminants/TruthParticleDecoratorAlg.h) | [`TruthParticleDecoratorAlg.cxx`](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/src/TruthParticleDecoratorAlg.cxx))

## Origin labels

Apart from the jet labelling, also track truth origin labels are available.
This label is obtained by examining the full ancestry of the truth particle linked to the track.
The corresponding variable is `ftagTruthOriginLabel` which is defined [here](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/TrackingID/InDetTrackSystematicsTools/InDetTrackSystematicsTools/InDetTrackTruthOriginDefs.h#L137).

| ftagTruthOriginLabel | Category         | Description
| -------------------- | ---------------- |  ---------------- |
| 0                    | pile-up          |  From a 𝑝𝑝 collision other than the primary interaction|
| 1                    | Fake             |  Created from the hits of multiple particles |
| 2                    | Primary          |  Does not originate from any secondary decay (labels 3-7)|
| 3                    | FromB            |  From the decay of a 𝑏-hadron|
| 4                    | FromBC           |  From a 𝑐-hadron decay, which itself is from the decay of a 𝑏-hadron |
| 5                    | FromC            |  From the decay of a 𝑐-hadron |
| 6                    | FromTau          |  From the decay of a 𝜏 |
| 7                    | OtherSecondary   |  From other secondary interactions |


## Type labels

Additionally, the truth type of the track itself is available.
This label is obtained by examining the truth particle linked to the track.
The corresponding variable is `ftagTruthTypeLabel` which is defined [here](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/FlavorTagDiscriminants/TruthDecoratorHelpers.h#L14).

For the values `ftagTruthTypeLabel > 1`, the sign of the value denotes the charge of the truth particle.

| ftagTruthTypeLabel | Category         | Description
| ------------------ | ---------------- |  ---------------- |
| 0                  | NoTruth          | no association with truth particle possible (e.g. PU) |
| 1                  | Other            | associated with truth particle but none of the types below |
| 2                  | Pion             | track associated with pion (PDG ID: 211) |
| 3                  | Kaon             | track associated with kaon |
| 4                  | Lambda           | track associated with lambda (PDG ID: 3122) |
| 5                  | Electron         | track associated with electron |
| 6                  | Muon             | track associated with muon |
| 7                  | Photon           | track associated with photon |


## Source labels

You can also obtain information about particles of secondary origins (interactions with material and decays of long living particles), which are handled by the detector simulation.
This label is based on the [`InDetTrackTruthOriginTool`](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/TrackingID/InDetTrackSystematicsTools/Root/InDetTrackTruthOriginTool.cxx?ref_type=heads).
The corresponding variable is `ftagTruthSourceLabel` which is defined [here](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/FlavorTagDiscriminants/TruthDecoratorHelpers.h#L27).

| ftagTruthSourceLabel | Category            | Description
| -------------------- | ------------------- | ---------------- |
| 0                    | NoTruth             | no association with truth particle possible (e.g. PU) |
| 1                    | NotSecondary        | associated with truth particle, but not a simulation particle |
| 2                    | HadronicInteraction | from a hadronic interaction |
| 3                    | StrangeMesonDecay   | from the decay of a strange meson |
| 4                    | StrangeBaryonDecay  | from the decay of a strange baryon|
| 5                    | GammaConversion     | from the conversion of a photon |
| 6                    | Other               | a simulation particle, but not in the above categories|

??? info "Derivations older than p6215"

    Before [the derivation with 25.0.5](https://its.cern.ch/jira/browse/ATLFTAGDPD-395), the source label was implemented differently and had different categories:

    You can also obtain information about the immediate parent particle/process of the track with the track's source label.
    This label is obtained by examining the parent of the truth particle linked to the track.

    | ftagTruthSourceLabel | Category         | Description
    | ------------------ | ---------------- |  ---------------- |
    | 0                  | NoTruth          | no association with truth particle possible (e.g. PU) |
    | 1                  | Other            | associated with truth particle but none of the types below |
    | 2                  | KaonDecay        | from the decay of a kaon |
    | 3                  | LambdaDecay      | from the decay of a lambda (PDG ID: 3122) |
    | 4                  | Conversion       | from the conversion of a photon |



## Vertex labels

`ftagTruthVertexIndex` truth vertex index of the track. 

* 0 is reserved for the truth PV (primary vertex) 
* any SVs are indexed arbitrarily with an int `>0` (the index uniquely identifies the vertex in the event[^index])
* Truth vertices within 0.1mm are merged.
* the label `-2` indicates that there is no truth link or truth vertex present (mostly for pile-up tracks)

[^index]: Note that indices are assigned per event, not per jet. This means that, within a single jet, you have have tracks with vertex indices `[0, 0, 3, 5, 5, 6]`, for example.


??? info "Mermaid Diagrams of the Labeling"

    To label the origins and sources a lot of the tools of the [`InDetTrackSystematicsTools`](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/TrackingID/InDetTrackSystematicsTools) are used. This is a general overview of how get these two labels:

    ```mermaid
    ---
    title: Track Origin Labels in FTAG
    ---
    %%{init: {'theme': 'neutral'} }%%
    flowchart LR

    TruthParticle --> getTruthOrigin
    getTruthOrigin --> get_source_type
    get_source_type --> Decorator\nSelection
    Decorator\nSelection --> ftagTruthSourceLabel
    ftagTruthSourceLabel -.- End

    TrackParticle --> getTrackOrigin

    subgraph getTrackOrigin
    direction TB

        GTO1["TruthParticle"]
        GTO1 --> GTO2
        GTO2["getTruthOrigin"]
        GTO2 --> GTO3
        GTO3["getTrackOrigin"]

    end

    getTrackOrigin --> getExclusiveOrigin
    getExclusiveOrigin --> ftagTruthOriginLabel
    ftagTruthOriginLabel -.- End

    End(["`Track in h5`"])

    InDetTrackSystematicsTools ~~~ FlavorTagDiscriminants

    classDef InDet fill:#81c784;
    class getTrackOrigin,getExclusiveOrigin,getTruthOrigin,InDetTrackSystematicsTools InDet;

    classDef FTAG fill:#e57373;
    class get_source_type,Decorator\nSelection,ftagTruthOriginLabel,ftagTruthSourceLabel,End,FlavorTagDiscriminants FTAG;
    ```

    The getTruthOrigin is a central method and works like this:

    ```mermaid
    ---
    title: getTruthOrigin(TruthParticle)
    ---
    %%{init: {'theme': 'neutral'} }%%
    flowchart TB


    Start(["`Initialize bit wise flag **origin** to 0 and get truth particle`"])
    Exp["`Each origin category has an integer value representing its bit position in **origin**`"]
        
    Start --> DecayChains
        
        subgraph DecayChains["Decay Chains of X (BHadron, DHadron or Tau)"]
            direction LR
            DC1{"Is X?"}
            DC1  -- Yes --> DC2
            DC2["`**XDecay to 1 in origin**`"]
            DC1  -- No --> DC3
            DC3{"Any of the parents X?"}
            DC3 -- Yes --> DC2
            DC3 -- No --> DC4
            DC4["For each parent \n as long as link valid"]
            %% DC4 --> DC1
            DC4 --> DC3
        end

    DecayChains --> isSec
    isSec{"Is simulation \n particle?"}
    isSec -- No --> frag
    isSec -- Yes --> Secondaries[["`_Secondary origins classification_`"]]

    Secondaries --> frag
    frag{"Any above \n category set \n to 1?"}
    frag -- Yes --> End
    frag -- No --> Fragmentation
    Fragmentation["`**Fragmentation to 1 in origin**`"]
    Fragmentation --> End


    End(["`Return **origin**`"])

    ```

    The secondary origin classification inside the getTruthOrigin method follows this scheme:

    ```mermaid
    %%{init: {'theme': 'neutral'} }%%
    flowchart TB
    subgraph Secondaries["Secondary origins classification"]
    direction TB
            
            OO1{"Is parent \n nullptr?"}
            OO1 -- Yes --> OO2["`**OtherOrigin to 1 in origin**`"]
            OO1 -- No --> GC1
            
            GC1{"Is electron &\nparent photon?"}
            GC1 -- Yes --> GC2["`**GammaConversion to 1 in origin**`"]
            GC1 -- No --> SM1
            
            subgraph DecayInFlight
                SM1{"Is parent strange meson & \n parent has two children?"}
                SM1 -- Yes --> SM2["`**StrangeMesonDecay to 1 in origin**`"]
                SM2 --> SM3{"Is pion+- & \n parent is Kshort"}
                SM3 -- Yes --> SM4["`**KshortDecay to 1 in origin**`"]

                SM1 -- No --> SB1

                SB1{"Is parent strange baryon & \n parent has two children?"}
                SB1 -- Yes --> SB2["`**StrangeBaryonDecay to 1 in origin**`"]
                SB2 --> SB3{"Is pi+- or proton & \n parent is Lambda"}
                SB3 -- Yes --> SB4["`**LambdaDecay to 1 in origin**`"]
                SB1 -- No --> OD1

                OD1{"Is parent hadron & \n parent has two children?"}
                OD1 -- Yes --> OD2["`**OtherDecay to 1 in origin**`"]
                
                
            end
            
            OD1 -- No --> HI1
            HI1{"Parent more than \n two children?"}
            HI1 -- Yes --> HI2["`**HadronicInteraction to 1 in origin**`"]
            HI1 -- No --> OS["`**OtherSecondary to 1 in origin**`"]
            
    end

    %% Styling
    classDef subgraphstyle margin-left:10cm
    class DecayInFlight subgraphstyle
    ```

    The getTrackOrigin method tries to use the origin of the TruthParticle, if possible, and adds categories like Fake and Pileup (The routine for full pileup truth samples is not shown here):

    ```mermaid
    ---
    title: getTrackOrigin(TrackParticle)
    ---
    %%{init: {'theme': 'neutral'} }%%
    flowchart TB

    Start(["`Initialize bit wise flag **origin** to 0, get track particle and truth match probability of track particle`"])

    Start --> TP{"Truth link \n present?"}

    TP -- No --> PU1["`**Pileup to 1 in origin**`"]
    TP -- Yes --> FPT1{"Full pileup \n truth sample?"}

    FPT1 -- Yes --> FPT2[["`_Routine for full pileup truth samples_`"]]
    FPT1 -- No --> TMP1

    PU1 --> TMP1
    FPT2 --> TMP1

    TMP1{"Truth match probability \n under cut value?"}

    TMP1 -- Yes --> TMP2["`**Fake to 1 in origin**`"]
    TMP2 --> TLP1
    TMP1 -- No --> TLP1

    TLP1{"Truth link \n present?"}

    TLP1 -- Yes --> TLP2["`**Combine track origin with particle origin from getTruthOrigin(TruthParticle)**`"]
    TLP1 -- No --> End
    TLP2 --> End

    End(["`Return **origin**`"])

    ```

    The ftagTruthOriginLabel is equivalent to the ExclusiveOrigin also found in [`InDetTrackSystematicsTools`](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/TrackingID/InDetTrackSystematicsTools):

    ```mermaid
    ---
    title: getExclusiveOrigin(int origin)
    ---
    %%{init: {'theme': 'neutral'} }%%
    flowchart TB

    Origins["`ExclusiveOrigins:
        **Pileup**        = 0,
        **Fake**          = 1,
        **Primary**        = 2,
        **FromB**          = 3,
        **FromBC**         = 4,
        **FromC**          = 5,
        **FromTau**        = 6,
        **OtherSecondary** = 7`"]

    Start(["`**origin**`"])

    Start --> PU1

    PU1{"Is Pileup?"}
    PU1 -- No --> F1
    PU1 -- Yes --> PU2(["`**return Pileup**`"])

    F1{"Is Fake?"}
    F1 -- No --> BND1
    F1 -- Yes --> F2(["`**return Fake**`"])

    BND1{"Is BHadronDecay and \n is not DHadronDecay?"}
    BND1 -- Yes --> BND2(["`**return FromB**`"])
    BND1 -- No --> DB1

    DB1{"Is BHadronDecay and \n is DHadronDecay?"}
    DB1 -- Yes --> DB2(["`**return FromBC**`"])
    DB1 -- No --> DNB1

    DNB1{"Is not BHadronDecay and \n is DHadronDecay?"}
    DNB1 -- Yes --> DNB2(["`**return FromC**`"])
    DNB1 -- No --> TAU1

    TAU1{"Is TauDecay?"}
    TAU1 -- Yes --> TAU2(["`**return FromTau**`"])
    TAU1 -- No --> SEC1

    SEC1{"Is StrangeMesonDecay, \n StrangeBaryonDecay, GammaConversion,\n HadronicInteraction, OtherDecay \n or OtherSecondary?"}
    SEC1 -- Yes --> SEC2(["`**return OtherSecondary**`"])
    SEC1 -- No --> OO1

    OO1{"Is OtherOrigin?"}
    OO1 -- No --> P(["`**return Primary**`"])
    OO1 -- Yes --> SEC2

    ```

    As the ftagTruthSourceLabel is focusing on the secondaries it uses the following scheme. But as opposed to the ftagTruthOriginLabel (which works with the tracks) the ftagTruthSourceLabel (based on particles) has some additional conditions to pass or be set to NoTruth, which is done [here](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/src/TruthParticleDecoratorAlg.cxx#L94).

    ```mermaid
    ---
    title: get_source_type
    ---
    flowchart TB
        A(["`**origin**`"])-->B{"isSecondary"}
        B -- True --> D{"isHadronicInteraction"}
        B -- False --> C["`return **NoSecondary**`"]
        D -- True --> E["`return **HadronicInteraction**`"]
        D -- False --> F{"isStrangeMesonDecay"}
        F -- True --> G["`return **StrangeMesonDecay**`"]
        F -- False --> H{"isStrangeBaryonDecay"}
        H -- True --> I["`return **StrangeBaryonDecay**`"]
        H -- False --> J{"isGammaConversion"}
        J -- True --> K["`return **GammaConversion**`"]
        J -- False --> L["`return **Other**`"]
    ```
