# Deploying Models in Athena

After you have trained your tagger, you will want to deploy it in Athena. This page explains how to do that.
There are a few steps to follow:

1. Serialise your model to a deployable format
2. Add the model to the group area
3. Schedule the model to run during derivations

!!! warning "Please follow the steps listed here carefully"
    
    The deployment of models in Athena is a critical step in the process of using them in the ATLAS software framework.
    It is important to follow the steps listed here carefully to ensure that the models are deployed correctly and can be used by the rest of the collaboration.

## 0. Timeline

Plan ahead.

ATLAS reproduces the standard data formats `DAOD_PHYS` and `DAOD_PHYSLITE` every 3 months. To avoid a 3 month delay it's imperative that you know when the next production will happen.

Also keep in mind that the steps below involve several different people and a file system that only updates several times a day.

### Technical constraints

Deploying a model requires coordination between:

- the FTAG conveners,
- subgroup conveners,
- Athena merge review shifters,
- Athena release coordinators, and
- AMG conveners.

Assuming all these people are available and attentive, a model can be deployed to the standard `DAOD_PHYS` data format in about 4 days. This also assumes the model is already fully validated.

**The optimistic timeline will not happen!** Your model will have quirks to work out, and most of the required people in this chain have downtime. **Realistically you should plan:**

- **1-5 days** to export the model, move it to a standard area, and test fidelity (see below).
- **0.5-3 days** to make a merge request and have it reviewed by the ftag group[^1].
- **2-5 days** for ATLAS review, specifically for level 1 shifters and AMG coordinators.
- **1-2 days** for the model to be merged into the official release.

Note that these are working days (weekends and holidays are excluded), and that many of these steps are out of our hands: no amount of willpower on your side will keep you on the lower side of these ranges.

### Recommended plan

Taking the above into account, we recommend the following timeline:

- **6 weeks in advance:** let everyone involved know that you'll be deploying a model to plan for possible absences.
- **4 weeks in advance:** Complete the technical validation described below
- **2 weeks in advance:**
    - Freeze the model,
    - upload it to the standard calibration area,
    - Open a merge request in Athena.
- **1 week in advance:** If the required changes aren't already merged, you should start nagging subgroup conveners, you are likely to miss your deadline.

[^1]: Note that a model which uses standard inputs can be integrated in a few days, but adding any new inputs can take several months depending on your famillearity with Athena software.

## 1a. Model validation

We support adding more experimental models to standard ATLAS data formats: in general the overhead of a new model is quite low. That said, every new variable creates room for confusion. Please do some checks to ensure the model will be useful to someone.

Specifically, you should present a simulation-based plots, showing the rejection of each background, with a fixed efficiency (e.g. 77%) in each bin. You should show this as a function of several variables:

- Jet $p_{\rm T}$ and $\eta$
- Pileup, both local ($\Delta R$ to nearest jet) and global (e.g. `averageInteractionsPerCrossing`).
- Data period: run 2, 2022, 2023, 2024 (MC20, MC23a, MC23d, MC23d)
- ROC curves for $t\bar{t}$

You should also prepare a plot showing the fidelity between the model as run in the training framework and as run in Athena, as [explained in the salt docs][tv] and below.

[tv]: https://ftag-salt.docs.cern.ch/export/#athena-validation

## 1b. Model Serialisation

Older models (including DIPS and DL1x) are serialised to [`lwtnn`](https://github.com/lwtnn/lwtnn) format.
These models are generally trained with Umami, and you can find more info
about the export process [here](https://umami-docs.web.cern.ch/trainings/LWTNN-conversion/).

Newer models trained with Salt (including GN2) are exported to [`ONNX`](https://onnx.ai).
A guide on exporting trained models is available [here](https://ftag-salt.docs.cern.ch/export/),
please follow it closely.

## 2. Add Models to Group Areas

The trained models are hosted in the ATLAS FTAG group areas.

There are two main areas used by the FTAG group:

1. `perf-flavtag` on EOS, where (amongst other things) the pytorch checkpoint and training configs are stored
2. `asg-calib` on EOS (`GroupData` on CVMFS), where the ONNX models are stored and used for inference in Athena


??? info "Paths to the Group Space"

    The `perf-flavtag` is located at:
    ```
    /eos/atlas/atlascerngroupdisk/perf-flavtag/
    ```

    The paths to the `asg-calib` spaces are below:

    === "EOS"
        Base path on EOS:
        ```
        /eos/atlas/atlascerngroupdisk/asg-calib/
        ```

        === "FTAG Group Space EOS"
            Group space EOS:
            ```
            /eos/atlas/atlascerngroupdisk/asg-calib/BTagging/
            ```
            Web interface: [https://atlas-groupdata.web.cern.ch/atlas-groupdata/BTagging/](https://atlas-groupdata.web.cern.ch/atlas-groupdata/BTagging/)

        === "FTAG Dev Space EOS"
            Dev space EOS:
            ```
            /eos/atlas/atlascerngroupdisk/asg-calib/dev/BTagging/
            ```
            Web interface: [https://atlas-groupdata.web.cern.ch/atlas-groupdata/dev/BTagging/](https://atlas-groupdata.web.cern.ch/atlas-groupdata/dev/BTagging/)

    === "CVMFS"
        Base path on CVMFS:
        ```
        /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/
        ```

        === "FTAG Group Space CVMFS"
            Group space CVMFS:
            ```
            /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/BTagging/
            ```

        === "FTAG Dev Space CVMFS"
            Dev space CVMFS:
            ```
            /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/BTagging/
            ```

### 2a: Copy to `perf-flavtag`

To deploy your model to the group space, you need to first make the relevant files available on EOS so that they can be copied to the `perf-flavtag` group space by the flavour tagging [algorithm sub-group conveners](mailto:atlas-cp-flavtag-algorithms-conveners@cern.ch):

- The exported `network.onnx` model, which contains all necessary metadata
- Your pytorch checkpoint file (`.ckpt`) used in the export
- Your YAML training config and metadata files

To start, the convener will the folder containing the above files to the `algs/models/` directory in the `perf-flavtag` space on EOS:

```
/eos/atlas/atlascerngroupdisk/perf-flavtag/algs/models/
```


### 2b: Deploy to `asg-calib`

For inference in Athena, the ONNX model **only** is then copied (again by a convener) from `perf-flavtag` to the `asg-calib` group space on EOS.

For testing within the FTAG group, the model is copied to the `dev/BTagging/` space on EOS (by an algo convener).
Then, when the model is ready for deployment in `BTagging/` for the whole collaboration it is copied to the main `BTagging/` space from the dev area by a **group convener**.

**Before copying**, ensure the following

1. The model path must be formatted correctly:

    ```
    BTagging/<timestamp>/<algorithm name>/<jetcollection>/network.onnx
    ```

2. The file permissions must be set correctly:

    ```bash
    find <dir> -type f -exec chmod 444 {} +
    ```

!!! info "Conveners: info on copying group to area can be found [here](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/ASGCalibArea)"

    Remember that only the serialised model should be copied to the group area.
    Other files, such as training configs, should remain in the `algs/models/` dir on the `perf-flavtag` EOS space.

The convener will then copy the dir containing **only** the `network.onnx` model file to the **correct path** in the group area.

!!! info "The synchronisation of the group area on `/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/` with the files on `/eos/atlas/atlascerngroupdisk/asg-calib/` runs 4 times a day at 6h, 12h, 18h, and 24h"


## 3. Schedule the Model to Run During Derivations


??? info "How does my model actually run in Athena?"

    https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/FlavorTagDiscriminants/GNN.h

    The networks are scheduled using [`DL2`](https://gitlab.cern.ch/atlas/athena/-/tree/master/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants#inputs-and-outputs-from-dl2).

    ??? info "Resolving paths using `PathResolver`"

        Using the `PathResolver`, the path of the network is searched for (in the following order) 

        - in the local directory where you start the job
        - `/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/` (mirror of `/eos/atlas/atlascerngroupdisk/asg-calib/`)
        - on https://atlas-groupdata.web.cern.ch/atlas-groupdata/ (works in docker images)


You need to do two things to run the tagger in derivations:

1. Schedule the tagger to run for the relevant jet collection in [`BTagConfig.py`](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/JetTagAlgs/BTagging/python/BTagConfig.py)
2. Add the tagger outputs to the listed of saved variables in [`BTaggingContent.py`](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/DerivationFramework/DerivationFrameworkFlavourTag/python/BTaggingContent.py)

Some noteable examples of models being added to derivations are:

- GN1 atlas/athena!54403
- GN2v00 atlas/athena!61571
- GN2v01 atlas/athena!67592

??? info "Multifold models are handled differently"

    Take a look at GN2v01 as an example GN2v01: atlas/athena!67592.
