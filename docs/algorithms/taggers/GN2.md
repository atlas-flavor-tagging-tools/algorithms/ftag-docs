# GN2 Tagger

The state-of-the-art ATLAS flavour tagging algorithm for Run-3 physics analysis is GN2, a continuation of the [GN1][gn1-tagger] developements.

A good description is provided in the CERN EP-IT seminar talk [ATLAS GNN Flavour Tagging](https://indico.cern.ch/event/1232499).

## Code

The training code can be found here, using the [Salt](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/salt) framework.

The preprocessing can be done with [UPP: Umami PreProcessing](https://github.com/umami-hep/umami-preprocessing).

