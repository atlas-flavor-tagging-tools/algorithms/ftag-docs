# GN3 Tagger

GN3 is the next iteration on the GN2 tagger, and is still under development.
GN3 will use more information from the detector, including information about neutral flow constituents and reconstructed leptons.

You can track the development progress here: https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/planning/-/milestones/1

## Code

The training code can be found here, using the [Salt](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/salt) framework.

The preprocessing can be done with [UPP: Umami PreProcessing](https://github.com/umami-hep/umami-preprocessing).

