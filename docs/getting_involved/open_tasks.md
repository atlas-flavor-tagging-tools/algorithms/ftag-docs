Open Tasks in Flavor Tagging
============================

Open tasks in flavor tagging are [tracked as issues in gitlab][1]. Please go there to learn more.

[1]: https://gitlab.cern.ch/atlas-flavor-tagging-tools/open-tasks/-/issues
