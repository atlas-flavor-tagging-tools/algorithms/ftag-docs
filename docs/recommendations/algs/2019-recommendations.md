# (2019) FTAG Algorithms In r21


## Algorithm Structure

The flavour tagging algorithms are divided into two types: the low level algorithms and the high level algorithms. The low level algorithms utilise different types of information that come directly from low level reconstructed objects, such as tracks and inner detector hits clusters. The high level taggers use the outputs from the low level taggers, such as reconstructed secondary vertices (in the case of SV1 and JetFitter), or jet flavour probabilities (such as IP3D, RNNIP and SMT), to calculate a final decision on the likelihood that a given jet is a b, c or light-flavour jet.

There are currently two types of high level algorithms: MV2 and DL1. They differ mainly on their architecture - MV2 is based on a boosted decision tree while DL1 is based on a deep neural network. More recently, DL1 has been optimised to include new input variables with respect to DL1. Another difference is that DL1 outputs three different probabilities (pb, pc and pu) that are combined to define a final discriminant:

$$
D_{DL1} = \log\frac{p_b}{f_c \cdot p_c + (1-f_c)\cdot p_u},
$$

where $f_{c}$ controls the importance of c-jets discrimination in the tagger. $f_{c}$ is optimised to ensure improvements in performance for both light and c-jets rejection.


The baseline DL1 tagger includes inputs from the following low level taggers: SV1, JetFitter, IP2D and IP3D. Two other variants exist:

- *DL1r* adds the outputs of RNNIP as inputs
- *DL1rmu* adds the outputs of RNNIP and SMT as inputs

The tagger structure is described pictographically in the following scheme:

![Tagger structure](../../assets/DL1-tagger-structure.png)

## Training Setup

The FTAG algorithms have been re-optimised in 2019 in order to maximise the performance on the jet collections recommended for use in ATLAS, Particle Flow (PFlow) jets and Variable-Radius Track (VR) jets, and to extend the algorithms performance to very high jet $p_{T}$. The description below is valid for algorithms found in the 201903 timestamped jet collections.

### Training Samples

The algorithms have been re-trained with mc16d samples on particle flow jets, with a mixture of tt (non all hadronic) and a Z' sample with a branching ratio equally split between b, c and light-flavor jets. The Z' sample effectively offers a flat $p_{T}$ spectrum of b, c and light jets that extends up to 5 TeV. This provides enough statistics to maintain the performance of the algorithms for a much larger range than the previous trainings. This mixture is referred to as the extended hybrid sample, as opposed to the standard hybrid sample, with a jet $p_{T}$ range that extended only up to 1 TeV.


The extended hybrid sample is built using $t\bar{t}$ jets when these jets have $p_{T}$ < 250 GeV, for c and light-flavor jets, or when the B-hadron $p_{T}$ is below 250 GeV, for b-jets. Jets above these $p_{T}$ ranges are taken from the Z' sample. These criteria refer to the extended hybrid sample of particle flow jets. For VR jets, the B-hadron $p_{T}$ is required to be below 125 GeV, for b-jets, since VR track jets carry about half of the B hadron energy in this kinematic range.
The specific datasets used in this campaign are listed below. The mc16d samples were the nominal training samples, which mc16a and mc16e were used for cross checking the performance in different pile-up scenarios:

- mc16a: mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_FTAG1.e6337_s3126_r9364_p3703
- mc16d: mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_FTAG1.e6337_s3126_r10201_p3703
- mc16e: mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_FTAG1.e6337_s3126_r10724_p3703
- mc16d: mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e6928_e5984_s3126_r10201_p3703

### Jet Selection

The following selection is used for particle flow jets:

- $p_{T} > $ 20 GeV
    - The training is performed using pre-GSC calibrated jets.
- $|\eta| < 2.5$
- PFlow optimised JVT:
    - For jets between 20 and 60 GeV, with $|\eta| < 2.4$, require [JVT](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JVT) > 0.5, as described [here](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PileupJetRecommendations#Central_region_recommended_worki).
Jet cleaning

For VR jets, the following selection is applied:

- $p_{T} > $ 10 GeV
- $|\eta| < 2.5$
- At least two tracks in track jet
- VR overlap check: only use VR jets if it doesn't overlap with any other VR jet. Two VRs overlap if their $\Delta R$ distance is less than the the radius of the highest $p_{T}$ jet being compared. The impact of this overlap removal is small due to the topology being studied (isolated jets).


## Algorithms Optimisations

### IP3D
The IP3D algorithm was optimised with the new samples described above. The improvements seen on light rejection have been found to be of the order of 10% to 20%. However, once those changes were included in the high level taggers (particularly MV2), the improvements were not shown to be significant. For simplicity, the IP3D updates were not included in this round of retraining.

- IP tuning for VR track jets ([B. Dong et al](https://indico.cern.ch/event/816014/contributions/3406214/attachments/1834190/3004757/IPTuningVRjets.pdf))

### RNNIP
On top of the improvements coming from the new training dataset, the RNNIP architecture was also re-optimized, ensuring improvements in performance both at low and high pT. A summary of these improvements can be found in the following presentations:

- PFlow optimisation ([N. Hartman et al](https://indico.cern.ch/event/799597/contributions/3322980/attachments/1799992/2935594/Hartman_Feb_21_19.pdf))
- VR optimisation ([N. Hartman et al](https://indico.cern.ch/event/816014/contributions/3407697/attachments/1834313/3004809/Hartman_Apr_25_19.pdf))

### SMT
The SMT algorithm is now based on a neural network, instead of a boosted decision tree. The number of input variables has also been increased to include the impact significance of the muon ID track, as defined by the IP-based algorithms (IP3D and RNNIP). The signing of the IP variables has also been changed to be consistent with the lifetime signing used by IP3D and RNNIP. These changes lead to significant improvements in performance, which can be seen in the following presentation:

- PFlow optimization ([R. Teixeira de Lima et al](https://indico.cern.ch/event/788976/contributions/3312710/attachments/1792383/2920568/FTAGAlgos_SMT_Rafael_20190207.pdf))
- The tagger was not optimised for VR track jets. The VR track jet SMT outputs are based on the PFlow training described above.


### MV2

MV2 was retrained using the extended hybrid dataset. The relative fraction of c-jets in the training dataset had to be re-optimized in order to balance the higher number of c-jets coming from the extended Z' sample, and is now 2%. Due to the complexity of the boosted decision tree used by the tagger, it was not possible to deliver the MV2r and MV2rmu variants. A summary of the expected performance of the MV2 tagger on PFlow jets is summarized in the following presentation:

- PFlow optimisation ([B.Dong et al](https://indico.cern.ch/event/813868/contributions/3394771/attachments/1832163/3000749/PFlow.19.04.18.pdf))
- MV2 has not been retrained for VR track jets. The VR track jet MV2 outputs are based on the PFlow training described above.

### DL1

A new DL1 training infrastructure has been put in place, including support for GPU-based trainings, including the capability to run hyper parameter optimisation scans. The network architecture has been optimised with these scans, a result that can be seen in these [public plots](http://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PLOTS/FTAG-2019-001/). The parameter $f_{c}$ has also been optimised separately for the three DL1 variants with the following results:

- DL1 $f_{c} = 0.018$
- DL1r $f_{c} = 0.018$
- DL1rmu $f_{c} = 0.03$

*Note: If your analysis is particularly sensitive to this value, please give us feedback!*

The expected performance of the DL1 variants, with this choice of charm fractions, can be seen in the public plots for [PFlow](http://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PLOTS/FTAG-2019-005/) and for [VR track jets](http://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PLOTS/FTAG-2019-006). The expected 2019 performance is also compared to the previous 2018 recommendations.

## Algorithms Expected Performances in MC
A summary of the expected performance of DL1r (new recommendation) on PFlow jets for the different available working points can be found in the following plots:

- INTERNAL light jet rejection as a function of jet pt for WPs defined per pT bin

![light jet rejection as a function of jet pt for WPs defined per pT bin](../../assets/2019-recommendations/flat_rej_u_dl1r2019_wps.png)

- INTERNAL c jet rejection as a function of jet pt for WPs defined per pT bin

![c jet rejection as a function of jet pt for WPs defined per pT bin](../../assets/2019-recommendations/flat_rej_c_dl1r2019_wps.png)

- INTERNAL b jet efficiency as a function of jet pt for WPs defined per pT bin

![b jet efficiency as a function of jet pt for WPs defined per pT bin](../../assets/2019-recommendations/flat_eff_b_dl1r2019_wps.png)

- INTERNAL light jet rejection as a function of jet pT for inclusive WPs

![light jet rejection as a function of jet pT for inclusive WPs](../../assets/2019-recommendations/rej_u_dl1r2019_wps.png)

- INTERNAL c jet rejection as a function of jet pT for inclusive WPs

![c jet rejection as a function of jet pT for inclusive WPs](../../assets/2019-recommendations/rej_c_dl1r2019_wps.png)

- INTERNAL b jet efficiency as a function of jet pT for inclusive WPs

![b jet efficiency as a function of jet pT for inclusive WPs](../../assets/2019-recommendations/eff_b_dl1r2019_wps.png)


## Benchmark Performance

### EMPflow Jets

The OPs are defined using a ttbar sample, considering jets with 20 GeV < pT < 250 GeV. When quoting the tagger performance, one could use the following table whose mis-tag rates are evaluated using a same sample considering jets in the same kinematic region. They correspond to the newly trained flavour tagging algorithms using PFlow jets (with the time stamp `AntiKt4EMPFlowJets_BTagging201903 `).

| Tagger + OP | charm mis-tag | light mis-tag |
|-------------|---------------|---------------|
| MV2         |               |               |
| 60% OP      | 0.044         | 0.0008        |
| 70% OP      | 0.115         | 0.0031        |
| 77% OP      | 0.208         | 0.0093        |
| 85% OP      | 0.370         | 0.0428        |
| DL1         |               |               |
| 60% OP      | 0.028         | 0.0005        |
| 70% OP      | 0.095         | 0.0019        |
| 77% OP      | 0.196         | 0.0063        |
| 85% OP      | 0.366         | 0.0322        |
| DL1r        |               |               |
| 60% OP      | 0.025         | 0.0004        |
| 70% OP      | 0.084         | 0.0016        |
| 77% OP      | 0.177         | 0.0052        |
| 85% OP      | 0.346         | 0.0248        |


Source: [https://twiki.cern.ch/twiki/bin/view/AtlasProtected/FTAGAlgorithms2019Taggers](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/FTAGAlgorithms2019Taggers)
