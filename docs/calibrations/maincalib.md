# Calibration methods

The flavour-tagging group provides calibration data-to-mc scale factors for b-, c- and light-jets.
The calibrations provided in the CDI File use three different final states and methods summarised in the following

| Jet Flavour | Process | Method paper |
| ----------- | ------- | ---- |
| b-jets      | ttbar dilepton | [Eur. Phys. J. C79 (2019) 970](https://link.springer.com/article/10.1140/epjc/s10052-019-7450-8)|
| c-jets      | ttbar l+jets | [Eur. Phys. J. C 82 (2022) 95](https://link.springer.com/content/pdf/10.1140/epjc/s10052-021-09843-w.pdf)|
| light-jets  | Z+jets | [Eur. Phys. J. C 83 (2023) 728](https://link.springer.com/article/10.1140/epjc/s10052-023-11736-z)|

## GN2v01 (MC20/3_2024-10-17_GN2v01_v1) calibration results


??? example "GN2v01WP: 65-0% "

	- Scale factor and total uncertainty as function of jet pT

	![run2](../assets/SF_uncert_2024-10-17_GN2v01_v1/run2/SF_65-0.png)
	![run3](../assets/SF_uncert_2024-10-17_GN2v01_v1/run3/SF_65-0.png)

	- Relative uncertainty breakdown as function of jet pT

	![run2](../assets/SF_uncert_2024-10-17_GN2v01_v1/run2/Unc_65-0.png)
	![run3](../assets/SF_uncert_2024-10-17_GN2v01_v1/run3/Unc_65-0.png)

??? example "GN2v01WP: 70-65% "

	- Scale factor and total uncertainty as function of jet pT

	![run2](../assets/SF_uncert_2024-10-17_GN2v01_v1/run2/SF_70-65.png)
	![run3](../assets/SF_uncert_2024-10-17_GN2v01_v1/run3/SF_70-65.png)

	- Relative uncertainty breakdown as function of jet pT

	![run2](../assets/SF_uncert_2024-10-17_GN2v01_v1/run2/Unc_70-65.png)
	![run3](../assets/SF_uncert_2024-10-17_GN2v01_v1/run3/Unc_70-65.png)

??? example "GN2v01WP: 77-70% "

	- Scale factor and total uncertainty as function of jet pT

	![run2](../assets/SF_uncert_2024-10-17_GN2v01_v1/run2/SF_77-70.png)
	![run3](../assets/SF_uncert_2024-10-17_GN2v01_v1/run3/SF_77-70.png)

	- Relative uncertainty breakdown as function of jet pT

	![run2](../assets/SF_uncert_2024-10-17_GN2v01_v1/run2/Unc_77-70.png)
	![run3](../assets/SF_uncert_2024-10-17_GN2v01_v1/run3/Unc_77-70.png)

??? example "GN2v01WP: 85-77% "

	- Scale factor and total uncertainty as function of jet pT

	![run2](../assets/SF_uncert_2024-10-17_GN2v01_v1/run2/SF_85-77.png)
	![run3](../assets/SF_uncert_2024-10-17_GN2v01_v1/run3/SF_85-77.png)

	- Relative uncertainty breakdown as function of jet pT

	![run2](../assets/SF_uncert_2024-10-17_GN2v01_v1/run2/Unc_85-77.png)
	![run3](../assets/SF_uncert_2024-10-17_GN2v01_v1/run3/Unc_85-77.png)

??? example "GN2v01WP: 90-85% "

	- Scale factor and total uncertainty as function of jet pT

	![run2](../assets/SF_uncert_2024-10-17_GN2v01_v1/run2/SF_90-85.png)
	![run3](../assets/SF_uncert_2024-10-17_GN2v01_v1/run3/SF_90-85.png)

	- Relative uncertainty breakdown as function of jet pT

	![run2](../assets/SF_uncert_2024-10-17_GN2v01_v1/run2/Unc_90-85.png)
	![run3](../assets/SF_uncert_2024-10-17_GN2v01_v1/run3/Unc_90-85.png)

??? example "GN2v01WP: 100-90% "

	- Scale factor and total uncertainty as function of jet pT

	![run2](../assets/SF_uncert_2024-10-17_GN2v01_v1/run2/SF_100-90.png)
	![run3](../assets/SF_uncert_2024-10-17_GN2v01_v1/run3/SF_100-90.png)

	- Relative uncertainty breakdown as function of jet pT

	![run2](../assets/SF_uncert_2024-10-17_GN2v01_v1/run2/Unc_100-90.png)
	![run3](../assets/SF_uncert_2024-10-17_GN2v01_v1/run3/Unc_100-90.png)
