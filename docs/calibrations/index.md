# ATLAS Flavour Tagging Calibration Documentation


The calibration group is responsible for measurement of the FTAG algorithm performance in data and simulated MC events and provides corrections to the simulated events for Physics Analysis usage.

!!! tip "To find out what the latest and greatest recommendations for Physics Analyses are please consult [Recommendations](../recommendations/index.md)"

!!! info " We also offer support analyses that want to use the FTAG algorithms in regions where the application of the standard calibrations is not straightforward. If that is your case, please get in contact with us."

!!! question "Do you want to get involved? Consult the list of [open tasks][tasks] and get in contact with us"

[tasks]: https://gitlab.cern.ch/atlas-flavor-tagging-tools/open-tasks/-/issues

## General Information
### Conveners
- [Brian Moser](https://atlas-glance.cern.ch/atlas/membership/members/profile?id=10745)
- [Martino Tanasini](https://atlas-glance.cern.ch/atlas/membership/members/profile?id=14502)

To get in contact with the conveners, please email to [atlas-cp-flavtag-calibration-conveners@cern.ch](mailto:atlas-cp-flavtag-calibration-conveners@cern.ch)

### Group mailing List

To get in contact with the entire group, please email to [atlas-cp-flavtag-calibrations@cern.ch](mailto:atlas-cp-flavtag-calibrations@cern.ch)

### Meetings
The group meets regularly every Monday at 13:00 (CET) [Indico](https://indico.cern.ch/category/9120/).

The list of the past and future meetings can be found [here](../meetings/calibration.md).

### Mattermost

You can join our Mattermost channel 

[FATGCalibration](https://mattermost.web.cern.ch/fatgcalibration){ .md-button .md-button--primary }

## FTAG Calibration GitLab

The calibration analyses store their code in the following 

[GitLab](https://gitlab.cern.ch/atlas-ftag-calibration/){ .md-button}

