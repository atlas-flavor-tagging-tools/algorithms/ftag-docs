# The Calibration Data Interface

The **Calibration Data Interface** (CDI) is simultaneously a framework for the purpose of *calibrating flavour tagging algorithms*, as well as a way to store these calibrations in **CVMFS** as ROOT files (CDI files) for later usage by analysers who need to use them in their analysis.  **The recommendations for using flavour tagging algorithms, and their calibrations, are provided to analyses in the form of these CDI files**. You can find these calibrations (and there are many, these are just the recommended ones!) at `cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/`. 

These ROOT files store, within their [internal directory structure](structure/filestructure.md), the tagging algorithm [efficiency maps](efficiency-maps.md) and [data to MC scale factors](scale-factors.md) which are used by *any ATLAS analysis* that makes use of these flavour tagging algorithms. Several operating points are defined by applying cuts on the flavour tagging discriminant distribution. The efficiencies for the various jet flavours passing these cuts are evaluated and are parametrised as a function of their relevant kinematic variables, such as pT and |η|.

The user interface resides in the git project [`CalibrationDataInterface`](https://gitlab.cern.ch/atlas/athena/-/tree/master/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/CalibrationDataInterface). Within the xAOD framework, a wrapper class has been developed, which is provided in the git project [`xAODBTaggingEfficiency`](https://gitlab.cern.ch/atlas/athena/-/tree/master/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency).



## Summary of recent CDI files

A summary of recent recommended CDI files is provided [here](../../recommendations/index.md).


## Crash-course for Analysers
If you wish to use b-tagging, c-tagging, or any other tagging algorithm in your physics analysis, you'll inevitably need to include tagger calibration information in your analysis. This is needed because our MC modelling of the jet environments in the LHC isn't perfect. So naturally the tagger, which is trained on simulated data, would likely have very different performance when applied to real data, and this calls for a calibration of the algorithm.

##### So how do we calibrate a flavour tagging algorithm?

To get a handle on tagger performance in real data, a dedicated analysis is performed to extract the tagging efficiencies in different (accessible) regions of phase-space. These are all fully fledged physics analyses in their own right, typically performed using $t\bar{t}$ events that have a high b-jet purity, all to extract the **flavour-tagging data-to-MC efficiency scale-factor** (i.e. the performance on real b-jets compared to MC) of a particular flavour-tagging algorithm, alongside other nuisance parameters and related calibration uncertainties. If you want to learn more about the specifics, the following sections of this document will go into more detail.

##### How are these calibrations used in an analysis?

These data-to-MC scale-factor corrections are implemented in your physics analysis as a per jet weight - essentially reweighting it's importance in your simulation samples so that the *overall efficiency* more closely matches to the efficiencies seen in real data. Naturally, different MC simulations lead to generator-specific mismodelling, which in turn leads to different efficiencies of tagging algorithms, necessitating so-called [MC to MC Scale Factors](mctomc.md) that can correct the efficiency scale-factor derived for the so-called "nominal" (default) data-to-MC scale-factors. 

##### Practically, what does an analyser need to do?

For users (analysers), all of this usually entails setting up the **BTaggingEfficiencyTool** from the **xAODBTaggingEfficiency** package - detailing what MC(s) you use and what [systematic strategy](systematics/eigenvectordecomp.md) you'd like to use. This code is essentially a wrapper of the CDI code, which enables users to easily set up the CDI and use it to retrieve the data-to-MC scale-factors directly for **xAOD::Jet** objects. This tool also enables users to utilize [eigenvector variations](systematics/eigenvectordecomp.md) in their analysis. Latet sections of this document will go into greater detail.
