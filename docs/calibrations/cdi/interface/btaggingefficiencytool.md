# Using the BTaggingEfficiencyTool

The `BTaggingEfficiencyTool` is used retrieve flavour-tagging data-to-MC efficiency scale-factors, which you can use to re-weight events in your MC samples. This tool is necessary also for the *propagation of the uncertainties of your flavour-tagging algorithm* so that your final results can take into account the uncertainty on the b-tagging itself. 

These scale-factors can be retrieved when the tool (once properly configured) is fed a valid `xAOD::Jet` object, like so

``` c++
xAOD::Jet* jet; 
... // jet object initialized
double sf(0);  // scale-factor
ATH_CHECK(tool->getScaleFactor(*jet,sf)); // check that tool worked 
```


## Configuring the `BTaggingEfficiencyTool`
The above code snippet demonstrates how the tool can be used directly on a jet object, but the actual *output* of the tool depends on its configuration, i.e. what working-point and what flavour-tagger you'd like to consider. This configuration can be done directly in C++, i.e.

``` c++
BTaggingEfficiencyTool* tool = new BTaggingEfficiencyTool("BTagEffToolNameHere");
// configure the tool
ATH_CHECK(tool->setProperty("ScaleFactorFileName","xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root" )); // the CDI file you wish to use for SF computation
ATH_CHECK(tool->setProperty("TaggerName", "DL1dv01")); // your tagger
ATH_CHECK(tool->setProperty("OperatingPoint", "FixedCutBEff_70")); // your working-point
ATH_CHECK(tool->setProperty("JetAuthor", "AntiKt4EMPFlowJets")); // use jet collection available in your CDI file
ATH_CHECK(tool->setProperty("SystematicStrategy", "SFEigen")); // setting the EV scheme
...

// once configured, initialize tool
ATH_CHECK(tool->initialize());

```

As you can see, the tool has a somewhat complex configuration. You can really customize the configuration of the tool to fit the purposes of your analysis. For example, you can configure the tool to work from multiple CDI files for efficiency and efficiency scale-factor computations separately. Commonly analysers configure the eigenvector decomposition systematic strategy to exclude some uncertainties from PCA for all jet flavours, or only for a certain *set* of jet flavours, and also setting the *reduction* of the set of retained eigenvector variations for each jet flavour separately as well.

 The following box has a list of all the configurable properties, where each is described in brief. **Note: the properties marked with an asterisk (*) are part of the minimal set of properties you should set when using this tool.**

??? note "BTaggingEfficiencyTool configurable properties"
    | Property Name |  Date Type | Description |
    | ------------ | ------------- | --------------- |
    | TaggerName* | String | Specify the flavour tagging algorithm that you are using. | 
    | OperatingPoint* | String | The tagger operating point. | 
    | JetAuthor* | String | The jet collection name as recognised in the calibration ROOT file (this may differ from the collection name as specified in the xAOD file) | 
    | ScaleFactorFileName* | String | The name (and location) of the "official" scale factor CDI file. Note that the [PathResolver](https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/PathResolver) package is used to construct the full path name from the string provided (using the `CALIBPATH` environment variable. First, a search without any prefix is attempted: this will allow analysers to provide their own file (assuming that there is a good reason for overriding the official one) in whatever location they prefer. If this is unsuccessful, a second search is attempted with the prefix "xAODBTaggingEfficiency". This will allow the file to be retrieved from its official location. | 
    | EfficiencyFileName | String | The name (and location) of the optional (user-provided) ROOT file containing custom efficiency maps. Again the PathResolver is used to construct the full pathname; however, the second step above is omitted (since by definition there is no "official" location for this file) | 
    | UseDevelopmentFile | Boolean | If set to true, this instructs the PathResolver to look for the file in the repository's "dev" area. |
    | ScaleFactorBCalibration | String | Name of the b-jet efficiency scale factor calibration object (default: "default") | 
    | ScaleFactorCCalibration | String | Name of the c-jet efficiency scale factor calibration object (default: "default") | 
    | ScaleFactorTCalibration | String | Name of the tau-"jet" efficiency scale factor calibration object (default: "default") | 
    | ScaleFactorLightCalibration | String | Name of the light-flavour jet efficiency scale factor calibration object (default: "default") | 
    | EfficiencyBCalibrations | String | Semicolon-separated list of names of the b-jet efficiency map objects (see below for more detail; default: "default") | 
    | EfficiencyCCalibrations | String | Semicolon-separated list of names of the c-jet efficiency map objects (default: "default") | 
    | EfficiencyTCalibrations | String | Semicolon-separated list of names of the tau-"jet" efficiency map objects (default: "default") | 
    | EfficiencyLightCalibrations | String | Semicolon-separated list of names of the light-flavour jet efficiency map objects (default: "default") |
    | ConeFlavourLabel | Boolean | If switched on, cone-based flavour labelling will be used (this is the present default; otherwise ghost association based flavour labelling is used). | 
    | OldConeFlavourLabel | Boolean | If switched on, the Run-1 style (parton level) cone label will be used (the default is to use the Run-2 style label -- hadron level and exclusive). | 
    | SystematicsStrategy* | String | Uncertainty model to be used. In both models, all uncertainties listed by the `affectingSystematics()` method should be evaluated to ensure that one is covering all systematic uncertainty contributions. | 
    | EigenvectorReductionB* | String | Specification of eigenvector nuisance parameter reduction scheme (default is `Loose`; other choices are `Medium` and `Tight`) | 
    | EigenvectorReductionC* | String | Specification of eigenvector nuisance parameter reduction scheme (default is `Loose`; other choices are `Medium` and `Tight`) | 
    | EigenvectorReductionLight* | String | Specification of eigenvector nuisance parameter reduction scheme (default is `Loose`; other choices are `Medium` and `Tight`) | 
    | ExcludeFromEigenVectorTreatment | String | Semicolon-separated list of names of uncertainties to be excluded from the eigenvector decomposition for all flavours | 
    | ExcludeFromEigenVectorBTreatment | String | Semicolon-separated list of names of uncertainties to be excluded from the eigenvector decomposition for b jets (in addition to the common list) |
    | ExcludeFromEigenVectorCTreatment | String | Semicolon-separated list of names of uncertainties to be excluded from the eigenvector decomposition for c jets (in addition to the common list) |
    | ExcludeFromEigenVectorLightTreatment | String | Semicolon-separated list of names of uncertainties to be excluded from the eigenvector decomposition for light-flavour jets (in addition to the common list) |
    | ExcludeRecommendedFromEigenVectorTreatment | Boolean | If switched on, a pre-determined set of uncertainties will be excluded be from the eigenvector decomposition (and will then need to be accounted for as "named" uncertainties). The idea is that this offers a way to implement the dominant correlations between calibration uncertainties for different jet flavours, with minimal in-depth knowledge (and intervention) from the user side required. |
    | UncertaintyBSuffix | String | If non-null, the specified suffix (preceded by an underscore) will be added for all b-jet calibration variations as seen by the user. |
    | UncertaintyCSuffix | String | If non-null, the specified suffix (preceded by an underscore) will be added for all c-jet calibration variations as seen by the user. |
    | UncertaintyTSuffix | String | If non-null, the specified suffix (preceded by an underscore) will be added for all tau-jet calibration variations as seen by the user. |
    | UncertaintyLightSuffix | String | If non-null, the specified suffix (preceded by an underscore) will be added for all light-flavour-jet calibration variations as seen by the user. |


## Commonly used methods

Perusing the [BTaggingEfficiencyTool](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency/xAODBTaggingEfficiency/BTaggingEfficiencyTool.h) interface, you can see a number of public methods that are available once the tool is setup and configured. Namely, a user can access a `xAOD::Jet` object's **tagging efficiency**, **inefficiency**, **MC efficiency map**, **data-to-MC scale-factor**, and **inefficiency scale-factor**.


| `BTaggingEfficiencyTool` methods | Description and Use-Cases |
| :-------------------- | ---------------- |
| **getScaleFactor**        | Retrieve the data-to-MC scale-factor for an `xAOD::Jet` object. An alternative (lower level) signature also allows retrieving the scale-factor for a given jet-flavour and `CalibrationDataVariables` object. Behaviour of this method is altered when `applySystematicVariation` is called.  |
| **getEfficiency**        | Retrieve the data efficiency for an `xAOD::Jet` object. An alternative (lower level) signature also allows retrieving the efficiency for a given jet-flavour and `CalibrationDataVariables` object. Behaviour of this method is altered when `applySystematicVariation` is called. |
| **getInefficiency**        | Retrieve the data inefficiency for an `xAOD::Jet` object. An alternative (lower level) signature also allows retrieving the inefficiency for a given jet-flavour and `CalibrationDataVariables` object. Behaviour of this method is altered when `applySystematicVariation` is called. |
| **getInefficiencyScaleFactor**        | Retrieve the data inefficiency scale-factor for an `xAOD::Jet` object. An alternative (lower level) signature also allows retrieving the inefficiency scale-factor for a given jet-flavour and `CalibrationDataVariables` object. Behaviour of this method is altered when `applySystematicVariation` is called. |
| **getMCEfficiency**        | Retrieve the MC efficiency for an `xAOD::Jet` object. An alternative (lower level) signature also allows retrieving the MC efficiency for a given jet-flavour and `CalibrationDataVariables` object. Behaviour of this method is altered when `applySystematicVariation` is called. |
| **applySystematicVariation(const CP::SystematicSet&)**        | This method applies a single systematic variation that the `BTaggingEfficiencyTool` *knows about* (has cached internally after construction and initialization). This method modifies all future calls to any of the above methods; e.g. after applying a systematic variation, `getScaleFactor` will return the scale-factor *with the variation applied*. The affect of this lasts until it is called again, either with a another systematic variation, or a "dummy" variation (default `SystematicSet` object) which resets the tool to return the central values rather than the varied ones. |
| **listAffectingSystematics**        | Return a list of all the systematic variations that the `BTaggingEfficiencyTool` knows about. This list depends both on what *systematic strategy* is used (whether you use eigenvector decomposition or not, etc.), as well as on what level of *reduction* is used for each jet-flavour, if indeed eigenvector decomposition is used. |
| **setMapIndex**        | This method is used whenever the `BTaggingEfficiencyTool` is configured to work with multiple efficiency maps (i.e. other MC generators) through the `EfficiencyXCalibrations` property discussed above, where `X` is "B", "C", "Light", or "T". Setting the efficiency map index for a given flavour, i.e. `setMapIndex("B", 1)`, means that the "B" efficiency map will be the second item in the semi-colon separated list of generators that you set at configuration. |
| **getTaggerName**        | Return the name of the tagging algorithm for which you configured the tool to retrieve calibrations. |
| **getJetAuthor**        | Return the name of the jet collection for which you configured the tool to retrieve calibrations. Options such as 'AntiKt4EMPFlowJets' and 'AntiKtVR30Rmax4Rmin02TrackJets' are commonly used. |
| **getOperatingPoint**        | Return the name of the working-point for which you configured the tool to retrieve calibrations. |

For a good example of how this is used, you can checkout the package's own [BTaggingToolsExample](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency/util/BTaggingToolsExample.cxx). 

