# Eigenvector Decomposition

For every jet flavour in a calibration file, there can be up to 150 uniquely named systematic uncertainties associated to it, which can be used to vary the kinematic bins of the central calibration by $\pm$ 1 $\sigma$. This process can be computationally expensive to perform during an analysis, especially when using the *smoothed calibration* (which can have ~300 bins to vary). **Note: the centrally available CDI files all provide smoothed calibrations by default. Unsmoothed CDI files are also available, but are deemed less useful for analysis than the smoothed efficiency distributions.**

A reduction of the number of systematic variations can be done via a principal component analysis (PCA), which can be performed on any sub-set of contributing uncertainties that you define (more on this further down in the *configurable properties* sections). The goal is to construct a set of so-called **eigenvector variations**, which are variation histograms that mix together the largest contributing systematic uncertainties. These eigenvectors are sorted and stored in decreasing variability, such that the leading eigenvector variation accounting for the most variation is first, the sub-leading eigenvector variation is second, and so on. 

Initially the number of eigenvector variations is equal to the number of kinematic bins of the calibration, but generally speaking the first 10-15 eigenvector variations account for nearly $100\%$ of the variation, with the remain ~290 accounting for a negligible fraction of the total variation. 

Thus, two reduction steps are performed - called **pruning** and **merging** respectively - to cut down on the number of eigenvector variations, keeping only the most significant:

1. In the pruning phase, any variations with *only* negligible contributions are removed. Very simply, it compares the absolute value of each variation bin to see if it exceeds a pre-defined threshold value. If even a single bin exceeds this threshold, then it is kept. All remaining eigenvector variations are then passed to the merging phase.

2. In the merging phase, all eigenvector variations above a certain index are merged, combining all the "relatively negligible" variations into one significant eigenvector variation (which works as these variations are stored in a decreasing order of significance, characterized by the absolute value of the corresponding eigenvalue). **Note: the contribution of each eigenvector variation that survives pruning can be anywhere from $1\%$ up to $50\%$ of the total variation.** The "severity" of this merging can be configured by the user, as laid out below. By default a `Loose` merging scheme is adopted, which will keep all eigenvariations that survive the pruning, whereas the `Medium` and `Tight` merging schemes merge all eigenvariations above the 3rd and 2nd index into the eigenvariation at the 3rd and 2nd index respectively. 


##### How to configure eigenvector decomposition systematic strategies

**There are currently two eigenvector decomposition strategies implemented in the `CalibrationDataInterface`, both of which can be configured by the same Python-configurable properties via the `BTaggingEfficiencyTool`.** The following lists are common to both systematic strategies.




??? note "Basic configurable properties"
    | Property Name |  Date Type | Description |
    | ------------ | ------------- | --------------- |
    | TaggerName | String | Specify the flavour tagging algorithm that you are using. | 
    | OperatingPoint | String | The tagger operating point. | 
    | JetAuthor | String | The jet collection name as recognised in the calibration ROOT file (this may differ from the collection name as specified in the xAOD file) | 
    | ScaleFactorFileName | String | The name (and location) of the "official" scale factor CDI file. Note that the [PathResolver](https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/PathResolver) package is used to construct the full path name from the string provided (using the `CALIBPATH` environment variable. First, a search without any prefix is attempted: this will allow analysers to provide their own file (assuming that there is a good reason for overriding the official one) in whatever location they prefer. If this is unsuccessful, a second search is attempted with the prefix "xAODBTaggingEfficiency". This will allow the file to be retrieved from its official location. | 
    | EfficiencyFileName | String | The name (and location) of the optional (user-provided) ROOT file containing custom efficiency maps. Again the PathResolver is used to construct the full pathname; however, the second step above is omitted (since by definition there is no "official" location for this file) | 
    | UseDevelopmentFile | Boolean | If set to true, this instructs the PathResolver to look for the file in the repository's "dev" area. | 


??? note "Properties for calibration and/or efficiency map specification"
    | Property Name |  Date Type | Description |
    | ------------ | ------------- | --------------- |
    | ScaleFactorBCalibration | String | Name of the b-jet efficiency scale factor calibration object (default: "default") | 
    | ScaleFactorCCalibration | String | Name of the c-jet efficiency scale factor calibration object (default: "default") | 
    | ScaleFactorTCalibration | String | Name of the tau-"jet" efficiency scale factor calibration object (default: "default") | 
    | ScaleFactorLightCalibration | String | Name of the light-flavour jet efficiency scale factor calibration object (default: "default") | 
    | EfficiencyBCalibrations | String | Semicolon-separated list of names of the b-jet efficiency map objects (see below for more detail; default: "default") | 
    | EfficiencyCCalibrations | String | Semicolon-separated list of names of the c-jet efficiency map objects (default: "default") | 
    | EfficiencyTCalibrations | String | Semicolon-separated list of names of the tau-"jet" efficiency map objects (default: "default") | 
    | EfficiencyLightCalibrations | String | Semicolon-separated list of names of the light-flavour jet efficiency map objects (default: "default") | 


??? note "Flavour labelling"
    | Property Name |  Date Type | Description |
    | ------------ | ------------- | --------------- |
    | ConeFlavourLabel | Boolean | If switched on, cone-based flavour labelling will be used (this is the present default; otherwise ghost association based flavour labelling is used). | 
    | OldConeFlavourLabel | Boolean | If switched on, the Run-1 style (parton level) cone label will be used (the default is to use the Run-2 style label -- hadron level and exclusive). | 


??? note "Further properties regarding systematic strategies"
    | Property Name |  Date Type | Description |
    | ------------ | ------------- | --------------- |
    | SystematicsStrategy | String | Uncertainty model to be used. In both models, all uncertainties listed by the `affectingSystematics()` method should be evaluated to ensure that one is covering all systematic uncertainty contributions. | 
    | EigenvectorReductionB | String | Specification of eigenvector nuisance parameter reduction scheme (default is `Loose`; other choices are `Medium` and `Tight`) | 
    | EigenvectorReductionC | String | Specification of eigenvector nuisance parameter reduction scheme (default is `Loose`; other choices are `Medium` and `Tight`) | 
    | EigenvectorReductionLight | String | Specification of eigenvector nuisance parameter reduction scheme (default is `Loose`; other choices are `Medium` and `Tight`) | 
    | ExcludeFromEigenVectorTreatment | String | Semicolon-separated list of names of uncertainties to be excluded from the eigenvector decomposition for all flavours | 
    | ExcludeFromEigenVectorBTreatment | String | Semicolon-separated list of names of uncertainties to be excluded from the eigenvector decomposition for b jets (in addition to the common list) |
    | ExcludeFromEigenVectorCTreatment | String | Semicolon-separated list of names of uncertainties to be excluded from the eigenvector decomposition for c jets (in addition to the common list) |
    | ExcludeFromEigenVectorLightTreatment | String | Semicolon-separated list of names of uncertainties to be excluded from the eigenvector decomposition for light-flavour jets (in addition to the common list) |
    | ExcludeRecommendedFromEigenVectorTreatment | Boolean | If switched on, a pre-determined set of uncertainties will be excluded be from the eigenvector decomposition (and will then need to be accounted for as "named" uncertainties). The idea is that this offers a way to implement the dominant correlations between calibration uncertainties for different jet flavours, with minimal in-depth knowledge (and intervention) from the user side required. |
    | UncertaintyBSuffix | String | If non-null, the specified suffix (preceded by an underscore) will be added for all b-jet calibration variations as seen by the user. |
    | UncertaintyCSuffix | String | If non-null, the specified suffix (preceded by an underscore) will be added for all c-jet calibration variations as seen by the user. |
    | UncertaintyTSuffix | String | If non-null, the specified suffix (preceded by an underscore) will be added for all tau-jet calibration variations as seen by the user. |
    | UncertaintyLightSuffix | String | If non-null, the specified suffix (preceded by an underscore) will be added for all light-flavour-jet calibration variations as seen by the user. |

## Excluding uncertainties

As can be seen in the list of configurable properties pertaining to the eigenvector decomposition systematic strategies, it is possible to *exclude systematic uncertainties from being considered*, either for individual flavours, or for all flavours inclusively. The method by which this is configured by the analyser is relatively simple - just provide the list of uncertainties to exclude. But what then? And why would one want to do this in the first place?

To answer the first question, the "excluded" uncertainties are only excluded in the sense that they won't contribute to the eigenvector variations that these systematic strategies produce. Instead, they are handled as so-called "named uncertainties" (possibly originating from the fact that you named them a second ago).

The reason any given analysis might want to do this is many-fold, but one common reason is so that uncertainties which apply to other physics objects in addition to the flavour-tagged objects in an event, can be handled and correlated outwith the CDI (??)


## The `SFEigen` strategy

As discussed above, the `SFEigen` systematic strategy essentially performs a PCA on the systematic uncertainties (i.e. the systematic variation histograms) of **individual jet flavours**, as stored in the CDI files. The manner by which this happens is implemented in the `CalibrationDataEigenVariations` object and called once per jet flavour internally in the `CalibrationDataInterfaceROOT`. 

A total covariance matrix of these uncertainties is constructed - that is, a sum of all the covariance matrices of all the individual, *included* uncertainties is made. This total covariance matrix is then diagonalized, and the eigenvector variations constructed and stored inside each `CalibrationDataEigenVariations` object.


??? note "Example of a total covariance matrix (unsmoothed), as produced in the `SFEigen` strategy for a b-jet."
    Here you can see the calibration scale-factor as a function of $p_T$. You may notice, however, that while the calibration bins are quite discrete, the error bands appear *smoothed*.
    ![Scale Factor](../../../assets/cdi/scale-factor.png)

    This is because, while the original calibration bins have this discrete nature, the CDI files store a *smoothed version* of the central calibration and the systematic uncertainties. 
    
    The green bands represent the envelope of systematic uncertainties around each calibration central value. The `SFEigen` systematic strategy performs a PCA finds a reduced set of the systematics. Below you can see the total correlation matrix - made trivially from the total covariance matrix of the unsmoothed calibration. The construction of this total covariance matrix is an intermediate step in the PCA performed in the `SFEigen` strategy.

    ![Covariance Matrix](../../../assets/cdi/cov-matrix.png)

    You can read more on how calibration smoothing is performed - and how it affects the statistical validity of the calibration analysis results - later on in this document. (Perhaps Eleonora could write up a section on this?). The following shows a comparison of the unsmoothed vs. smooth bin-to-bin correlations - the latter is used in real analyses.

    ![Smoothing](../../../assets/cdi/smoothing.png)



??? note "Total covariance matrices in the `SFEigen` scheme."
    Below you can see the total bin-to-bin correlation matrices, which were constructed from the total covariance matrices resulting from two different **efficiency working points**. 

    The first shows a **fixed-cut working point** (specifically, the $85\%$ working point). You may notice it's been smoothed into 300 $p_T$ bins, resulting in a 300 $\times$ 300 covariance matrix.

    ![Correlation Matrix 1](../../../assets/cdi/correlation1.png)

    The second correlation matrix was constructed from the total covariance matrix of the so-called **"Continuous" working point**. This clearly shows the full tag-weight distribution (5 sections) each containing the unsmoothed $p_T$ distribution calibrations.

    ![Correlation Matrix 2](../../../assets/cdi/correlation2.png)

    Note the reduced number of bins in total - this is due to the fact taht the $p_T$ distribution of the continuous calibration remains unsmoothed. Despite the reduced number of bins, due to the relatively complicated structure of the bin-to-bin correlations, the "Continuous" working point sees far less reduction in the PCA reduction schemes that are presently available.


##### Technical implementation

One can configure the systematic strategy via the interface provided by the `BTaggingEfficiencyTool`. This tool constructs a single instance of the `CalibrationDataInterfaceROOT` for every jet-tagger you wish to use in your analysis, which is the main interface by which this package interacts with the `CalibrationDataInterface`. The former is needed as a sort of "translation layer" to access jet scale-factors and such when working within the **xAOD framework**, whilst the latter is where all of the machinery is set up and utilized.

Internally, for the `SFEigen` strategy, four separate instances of the `CalibrationDataEigenVariations` class are instantiated (one for each jet flavour). These objects perform the PCA and reduction steps on their jet flavour, and internally store the final eigenvector variations that the interface object accesses when requested by the analyser.

??? note "Diagramatic representation of the code"
    ![SFEigen](../../../assets/cdi/SFEigen.png)



## The `SFGlobalEigen` strategy

The `SFGlobalEigen` strategy works in much the same way as the `SFEigen` strategy, yet there is some additional functionality and some other important differences to consider when using this strategy over the other.


#### Technical implementation

From the outside, this systematic strategy makes no changes to the external interface that analysers interact with - the implementation details are hidden within the `CalibrationDataInterfaceROOT` class, which instantiates a derived class, `CalibrationDataGlobalEigenVariations`. Whereas the base-class (`CalibrationDataEigenVariations`) needs to be instantiated *once for each jet flavour* (given a tagger, working-point, and jet collection to look at), the global eigenvariations method pulls all of the jet flavours together all at once.

??? note "Diagramatic representation of the code"
    ![SFEigen](../../../assets/cdi/SFGlobalEigen.png)


## Which method should I use?

The decision to use one method over the other comes down to what mixture of jet-flavours your analysis needs to be able to calibrate simultaneously. Should your analysis only need b-jet calibrations, then the `SFEigen` strategy is sufficient. However, if your analysis combines b-jets and c-jets, you may want to consider using the `SFGlobalEigen` strategy, as it more correctly handles any **cross-flavour correlations** that may exist between the various jet flavours of your analysis. Such correlations exist as the various flavour calibration measurements can be affected by the *same source of uncertainty* (for example the jet energy scale (JES) uncertainty), and as such the `SFGlobalEigen` strategy handles any shared uncertainties in the construction of the eigenvector variations. 