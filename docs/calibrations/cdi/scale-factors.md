# CDI data-to-MC scale factors

??? Danger "Fixed-cut scale factors are no longer supported for `GN2`"
    As of `GN2v00` we only support "pseudo-continuous" scale factors.
        These should cover every situation where fixed cut scale factors were used, see the section [pseudo-continuous tagging](pcbt.md)

The efficiency of a tagger, as discussed in the previous section, can be very easily measured and validated while training a tagger on simulated data. The data itself is generated from Monte Carlo $t\bar{t}$ samples - but there's no reason to suspect that the tagger will perform the exact same way on real collider data, by the simple argument that we don't trust our simulation to be able to capture all of the hard and soft-physics to an arbitrary degree of accuracy. The sample distributions themselves differ between the various MC event generators, and so there's a clear ambiguity about whether we can trust that our tagger, which is *trained* on simulated data, will perform well on *real* data.

In order to trust that our tagger performs well on real data, we can perform a *calibration* of the tagger. A calibration is a physics analysis which seeks to extract the tagging efficiency by e.g. performing a fit on $t\bar{t}$ samples of high b-jet purity. 

More information on the calibration procedure itself can be found in these references [[1]](https://cds.cern.ch/record/2638455) [[2]](https://cds.cern.ch/record/2652195) [[3]](https://cds.cern.ch/record/2306649) [[4]](https://cds.cern.ch/record/2314418) [[5]](https://cds.cern.ch/record/1741020), but the end result of a calibration will always be stored in the CDI files themselves in the form of **efficiency distributions** (as in, distributions of the probability to tag a b-, c-, or light-jet as a b-jet), with corresponding systematic uncertainty variation histograms.


## Applying calibrated flavour tagging to analysis

The data-derived tagging efficiency distributions, and their systematics variations, can be combined into a kinematic-dependent **efficiency scale-factor**

$$
SF_{b}(p_{T}) = \frac{\epsilon_{b}^{data} (p_{T})}{ \epsilon_{b}^{MC} (p_{T})}
$$

which, in an analysis, can be used as a **weight on b-tagged jets in your MC sample**, i.e. each b-tagged jet in your MC event gets a weight $w_{jet} = SF_{b}(p_{T})$. For completeness, every non-tagged jet in your MC event is reweighted by a so-called **inefficiency scale-factor**

$$
w_{jet} = \frac{1 - \epsilon_{b}^{data} (p_{T})}{ 1- \epsilon_{b}^{MC} (p_{T})} = \frac{1 - SF_{b}(p_{T})\epsilon_{b}^{MC} }{1 - \epsilon_{b}^{MC}}
$$

so that, overall, the event's overall weight will match more closely to the calibrated efficiency distribution.  **Note: The overall b-tagged event weight is calculated as the product of the jet weights.`**

The following plot (see below) should give you an idea of what a typical efficiency scale-factor distribution looks like in terms of the most common kinematic variable, the jet transverse momentum ($p_T$). The original binning of the calibration analysis is somewhat coarse - typically containing 5-9 bins in jet $p_{T}$, and 1 bin in $|\eta|$. If statistics allow it then the calibration would ideally include a binning in $|\eta|$ as well, but this is not typically the case. 

Note that, before being stored in the central CDI files, the calibration scale-factors are *smoothed* into a larger number of $p_{T}$ bins (typically $O(300)$), as you can see in the plot below. 




![Scalefactor](../../assets/cdi/scale-factor.png)

## Interpreting flavour-tagging calibrations

You may want to know *why* these scale-factors are necessary to begin with. To understand *why*, you can simply look to the plot above, and note that the efficiency to tag a jet in real data *differs* to that of your MC with respect to the $p_T$ of your jet. This essentially tells you that the jet has less (or more) of a chance to have been tagged correctly, at least with respect to the data used in the calibration analysis. This can be thought of as putting a weight on the jet's "importance", which are altogether aggregated into the event's "importance" (taken as the product of all the jet weights).

Even if this entire distribution were precisely 1 (i.e. the tagger has an identical efficiency in real data as it does in your MC), the *systematics* need to be taken into account, as taking these can also vary the relative importance of your jet at the $\pm 1 \sigma$ variation of a given source of uncertainty. You can read more about how systematic variations are utilized later on in another section of this document, but know for now that taking them into account, and propagating the uncertainties correctly, is necessary in order to trust your analysis results.
