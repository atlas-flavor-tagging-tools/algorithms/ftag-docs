# Info for calibrators

Here a series of useful informations of interest of the calibration teams

## Latest Software
Versions of AthAnalysis are built roughly everyweek. 
You can find the latest version and the relative changes at the following link [AnalysisBase Release 25.2 twiki](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AnalysisBaseReleaseNotes25pt2) or join the e-group **atlas-sw-pat-releaseannounce** were new releases are announced.

## Derivations and MC Requests

The calibration analyses are currently using DAOD_PHYS (c-jets) and DAOD_FTAG2 (b-jets and light jets). 
The lastest stable derivation campaign is documented in this [twiki](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PhysPhysliteProductionRun3#StableRelease24p0p2)

GN2v01 Tagger has been included in the derivation cache >= 24.0.18.

### MC23 Open Requests

| Process | JIRA | Generation status | DAOD |
| ------- | ---- | ------ | ---- | 

### MC23 Recently completed requests

| Process | JIRA | Generation status | ptag |
| ------- | ---- | ------ | ---- | 
| ttbar W->cb filter | [ATLMCPROD-10848](https://its.cern.ch/jira/browse/ATLMCPROD-10848) | Done  | p5921  |
| tW DS dynamic scale | [ATLMCPROD-10862](https://its.cern.ch/jira/projects/ATLMCPROD/issues/ATLMCPROD-10862) | Done  | p5855 |
| Flat pT Z' samples (Nominal) | [ATLMCPROD-10852](https://its.cern.ch/jira/browse/ATLMCPROD-10852) | Done | N/A |
| Flat pT Z' samples (Herwig) | [ATLMCPROD-10852](https://its.cern.ch/jira/browse/ATLMCPROD-10852) | Done | N/A |
| VV Sherpa 2.2.14 | [ATLMCPROD-10804](https://its.cern.ch/jira/browse/ATLMCPROD-10804) | Done | N/A |
| pThard=1 variation ttbar and stop | [ATLMCPROD-10881](https://its.cern.ch/jira/browse/ATLMCPROD-10881) | Done  | N/A |
| tW Powheg+Herwig | [ATLMCPROD-10951](https://its.cern.ch/jira/browse/ATLMCPROD-10951) | Done| N/A |
| tW DR ptHard=1 | [ATLMCPROD-10881](https://its.cern.ch/jira/browse/ATLMCPROD-10881) | Done | N/A |
| di-jet muon filter | [ATLMCPROD-10871](https://its.cern.ch/jira/browse/ATLMCPROD-10871) | Done on MC23d | p5855|


## Storage

Calibration teams can store the ntuples from flattening DAOD_PHYS either on EOS or on a Rucio Storage Element (RSE).

See also: [the "Samples" page](../samples/grid.md#step-0-who-should-replicate-datasets) on how to replicate datasets.

### Acces to FTAG EOS

The FTAG group eos space is

```bash
/eos/atlas/atlasgroupdisk/perf-flavtag/
```

To have access this EOS space subscribe to the general FTAG e-group: `hn-atlas-flavourTagPerformance`.
It is possible to check the remaining space on EOS via [Grafana monitoring](https://ascigmon.web.cern.ch/d/ClOCuVkmz/eos-quota-atlascerngroupdisk?orgId=1&var-QuotaNode=%2Feos%2Fatlas%2Fatlascerngroupdisk%2Fperf-flavtag%2F).
For problems related this EOS space please contact the space manager [Leonardo Toffolin](mailto:leonardo.toffolin@cern.ch).

### Samples on RSE

To overcome the problem of having no space where to store files a solution is proposed where the tuples are saved in a RSE (Rucio Storage Element). When files are store in an RSE it can be accessed both interactively and by HTCondor. Using this method also removes the download step of your tuples once these have been produced on the GRID. 

#### Which RSE?

You can use the group one CERN-PROD_PERF-FLAVTAG or your local institute one LOCALGROUPDISK (which is usually more suitable for long term storage).

#### Check RSE quota

``` bash
rucio list-account-limits $USER | grep LOCALGROUPDISK
rucio list-account-usage $USER | grep LOCALGROUPDISK
```

#### How do I get my ntuples on a RSE?

Unless specified, your grid outputs are stored in a SCRATCHDISK (short term storage) and after 14 days deleted and lost forever. You have to options to have your grid outputs in a LOCALGROUPDISK:  
-  When submitting JOBs on the grid you can specify --destSE MY-FAVORITE_LOCALGROUPDISK
- You can manually request a rucio rule via the web interface [Rucio-UI](https://rucio-ui.cern.ch/r2d2/request)
    - Step 0. Be sure you loaded you certificate on the browser otherwise accessing this webpage is difficult (I found that FireFox works better than Safari or Chrome for this)
    - Step 1 (Select Data Identifiers section). Find the files for which you want to ask the replication and select them
    - Step 2 (Select Rucio Storage Elements). Find your RSE and check the available quota
    - Step 3 (Options). Specify a lifetime for your files and submit the request 

#### Accessing files on RSE

Once your files on a RSE you can access them with rootxd for example running 

``` bash
setupATLAS
lsetup rucio 
voms-proxy-init -voms atlas
rucio list-file-replicas --protocol root --pfns --rse MY-FAVORITE_LOCALGROUPDISK MY-REPLICATED-CONTAINER
```
If you want to create a list of input files you can find [here](https://gitlab.cern.ch/atlas-ftag-calibration/ftagutils/-/tree/master/RunHTCondoronRSE) one example written in python: 

``` bash
python Remotefile_rucio.py alldataset.txt
```

### Running HTCondor on samples on RSE

After you created the lists of root files existing in a RSE you can your code over them in HTCondor. To do so:
- Copy your proxy in the directory from which you run your code $my_path 
```bash
id -u (returns your personal proxy id number PID )
cp /tmp/x509up_uPID $my_path
```
-  Include in your HTCondor submission file two lines
``` bash
use_x509userproxy = true
x509userproxy     = $my_path/x509up_$myPID
```
Again you can find an example [here](https://gitlab.cern.ch/atlas-ftag-calibration/ftagutils/-/tree/master/RunHTCondoronRSE) where CondorRSE.sub has the structure you want your HTCondor submission script to have. 

#### Known Issues

Apparently if in your code you open a file saved in an RSE using 
``` bash
TFile("my_amazing_rootfile")
```
ROOT will cut out the first part of the file name and therefore complain that the file does not exists (see more about "How to create your own problems"). This issue is solved when the equivalent syntax is used 

``` bash
TFile *f =TFile::Open("my_amazing_rootfile")
```

### Linking CERN-PROD_PERF-FLAVTAG to EOS  

1. Transfer your output dataset to CERN-PROD_PERF-FLAVTAG (Following the instructions in the above drop menu, changethe sitename to CERN-PROD_PERF-FLAVTAG, also tick the request approval box and leave the lifetime entry empty which will permenantly store the samples.) 
2. After the transfer is done, run
``` bash
rucio list-datasets-rse CERN-PROD_PERF-FLAVTAG | sort
```
You should be able to see the dataset you just transfered

3. Do to a directory on eos or afs, create a directory with name data_set_X: 
```bash
mkdir data_set_X
cd data_set_X
rucio list-file-replicas --rse CERN-PROD_PERF-FLAVTAG --link /eos/:/eos/  scope:dataset (for instance, user.lapereir:user.lapereir.data16_13TeV.periodB.physics_Main.PhysCont.DAOD_FTAG2.grp16_v01_p3802.ATop.v15.pflow6_output.root)
```
This creates symbolic links to all the files in this dataset in the current directoy, then you can use this new directory to access your dataset

### Archiving Samples to grid storage (and TAPE)

When a calibration analysis is done, in particular, after the corresponding publication is finished, it is good to archive samples to tape. Here is a detailed recipes to do so:

1. Preprocessing the samples so that individual file sizes and sample sizes are approperiate. The following workflow is smooth if the file size is around a few GBs and the entire dataset is around a few hundreds GBs. You can use [this script](https://gitlab.cern.ch/atlas-flavor-tagging-tools/ftagquickcheck/-/blob/master/StorageTools/splitFiles.py) to split the datasets.

It will split for instance /eos/atlas/atlascerngroupdisk/perf-flavtag/calib/negtag/Rel21_data15161718_mc16ade/NewTaggers/MC16d/RES_D0_MEAS into /eos/atlas/atlascerngroupdisk/perf-flavtag/calib/negtag/Rel21_data15161718_mc16ade/NewTaggers/MC16d/RES_D0_MEAS_0,1,2,....

The above script does not delete the source directory. Please only do this when the samples have been transderred to a reliable site, see below.  

2. Once you have split the datasets, you can use a dedicated package to upload the datasets to a given RSE. Check it out [here](https://gitlab.cern.ch/biliu/archivetape/-/tree/python3?ref_type=heads). A [MR](https://gitlab.cern.ch/mswiatlo/archivetape/-/merge_requests/7) is being reviewed.

The command is something like the following:
```
export RUCIO_ACCOUNT=perf-flavtag
voms-proxy-init -voms atlas --valid 120:00:00
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup rucio
python3 registerToRucio.py -d /eos/atlas/atlascerngroupdisk/perf-flavtag/calib/negtag/Rel21_data15161718_mc16ade/NewTaggers/MC16d/RES_D0_MEAS_0 -s CERN-PROD_SCRATCHDISK -a group.perf-flavtag -r 1 -o ljet_rel21_mc16d_trksys_res_d0_meas_0
```

As you can see, you will need to be a perf-flavtag account holder. Make sure that the datasets contain the files after the uploading is done:
```
rucio list-files group.perf-flavtag:ljet_rel21_mc16d_trksys_res_d0_meas_0
```

3. Once the RSEs are available on an RSE, you can move them around following the recipes above. So far, we have: CERN-PROD_PERF-FLAVTAG (accessible via eos, in high demand) and IN2P3-CC_PERF-FLAVTAG (not accessible locally but more space, good for intermediate archiving). We are also requesting a dedicated tape storage for long-term archiving done projects.

### Discussion on FTAG RSE and EOS quota

For any additional question or comment on our FTAG disk space, you are kindly advised to join the dedicated [Mattermost channel](https://mattermost.web.cern.ch/fatgcalibration/channels/eos--ftag-rses).
