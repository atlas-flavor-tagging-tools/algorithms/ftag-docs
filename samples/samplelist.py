import os

import yaml
from utils import (
    dump_aod_information_md,
    dump_daod_information_md,
    dump_sample_information_md,
    get_changelog_entry,
    sample_to_admonition,
)


class MDPage:
    def __init__(self, output_path):
        self.content = []
        self.output_path = output_path

    def add(self, element):
        self.content.append(element)

    def write(self, append=False):
        mode = "a" if append else "w"
        with open(self.output_path, mode) as output_file:
            for element in self.content:
                output_file.write(element.render())


class MDObject:
    def __init__(self, content=""):
        self.content = content

    def render(self):
        return self.content


class Heading(MDObject):
    def __init__(self, title, level=1):
        self.content = "#" * level + f" {title}\n\n"


class TextFromFile(MDObject):
    def __init__(self, file_path):
        with open(file_path) as f:
            self.content = f.read() + "\n\n"


class SampleList(MDObject):
    def __init__(self, file_path, level=3, infobox=False, warningbox=False, selection=set([])):
        name = os.path.splitext(os.path.basename(file_path))[0].replace("_", " ")
        # improve name formatting nicer
        name = name.replace("pflow", "EMPflow jets")
        name = name.replace("vr", "VR track jets")
        with open(file_path, "r") as stream:
            data = yaml.safe_load(stream)
            sample_info = dump_sample_information_md(name, data, level, selection)
        if infobox:
            self.content = sample_to_admonition(sample_info, style="info") + "\n\n"
        elif warningbox:
            self.content = sample_to_admonition(sample_info, style="warning") + "\n\n"
        else:
            self.content = sample_info + "\n\n"


class ChangeLog(MDObject):
    def __init__(self, directory_path):
        # collect sample lists
        changelog_data = []
        for path, directories, files in os.walk(directory_path):
            for file in files:
                name = os.path.splitext(os.path.basename(file))[0].replace("_", " ")
                with open(os.path.join(path, file), "r") as stream:
                    data = yaml.safe_load(stream)
                    changelong_entry = get_changelog_entry(name, data)
                    changelog_data.append(changelong_entry)
        # sort samples by name to get most recent sample first
        changelog_data = list(reversed(sorted(changelog_data)))

        # build changelog
        self.content = "| p-tag | Comments | Jira |\n| ----- | -------- | ---- |\n"
        for entry in changelog_data:
            self.content += f"{entry}\n"
        self.content += "\n\n"


class SampleInfo(MDObject):
    def __init__(self, sample_infobase):
        self.content = "| Sample | DSID | Description |\n| ------ | ---- | ---------- |\n"
        for sample in sample_infobase:
            dsid_entry = ""
            if isinstance(sample.dsid, list):
                for dsid, joboption in zip(sample.dsid, sample.joboption):
                    dsid_entry += f"[{dsid}]({joboption}) "
            else:
                dsid_entry += f"[{sample.dsid}]({sample.joboption})"
            self.content += f"| {sample.name} | {dsid_entry} | {sample.description} |\n"
        self.content += "\n\n"


class AODList(MDObject):
    def __init__(self, file_path):
        with open(file_path, "r") as stream:
            data = yaml.safe_load(stream)
            sample_info = dump_aod_information_md(data)
        self.content = sample_info + "\n\n"


class DAODList(MDObject):
    def __init__(self, file_path):
        with open(file_path, "r") as stream:
            data = yaml.safe_load(stream)
            sample_info = dump_daod_information_md(data)
        self.content = sample_info + "\n\n"
