import yaml


class Sample:
    def __init__(self, name, data):
        self.name = name
        self.dsid = data["dsid"]
        self.description = data["description"]
        self.joboption = data["joboption"]
        self.use_for_training = data["use_for_training"]
        self.use_for_workingpoint = data["use_for_workingpoint"]
        self.use_for_mcmc = data["use_for_mcmc"]

    def __repr__(self):
        return f"{self.name}"


class SampleInfoBase:
    def __init__(self, file_path):
        self.samples = set()
        with open(file_path, "r") as stream:
            data = yaml.safe_load(stream)
            for key, value in data.items():
                self.samples.add(Sample(key, value))

    def __iter__(self):
        return iter(self.samples)

    def get_samples(self, choice="training", jet_collections=["pflow", "vr"]):
        if choice in ["training", "workingpoint", "mcmc"]:
            return set([s.name for s in self.samples if (set(jet_collections) & set(getattr(s, f"use_for_{choice}")))])
        else:
            return set([s.name for s in self.samples])
