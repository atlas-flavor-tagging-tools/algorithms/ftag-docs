from os.path import join
from pathlib import Path

from sample import SampleInfoBase
from samplelist import AODList, Heading, MDObject, MDPage, SampleInfo


def main():
    # get paths
    this_dir = Path(__file__).parent
    docs_dir = this_dir.parent / "docs"
    output_path = docs_dir / "algorithms" / "samples" / "aod_sample_list.md"

    # path to aod sample list
    aod_list_mc23 = join(this_dir / "aod" / "mc23.yaml")
    aod_list_mc21 = join(this_dir / "aod" / "mc21.yaml")
    aod_list_mc20 = join(this_dir / "aod" / "mc20.yaml")

    # assemble sample-list markdown page
    document = MDPage(output_path)
    document.add(Heading("List of AOD Samples", level=1))
    document.add(MDObject("This page contains a list of available AOD samples for FTAG use.\n\n"))
    document.add(
        MDObject(
            "Click on the links for more information about [derivations](../../samples/daod.md) "
            "and [H5 training datasets](h5_sample_list.md) produced from these samples.\n\n"
        )
    )

    document.add(Heading("Avaliable MC23 AOD samples", level=2))
    document.add(AODList(aod_list_mc23))

    document.add(Heading("Avaliable MC21 AOD samples", level=2))
    document.add(AODList(aod_list_mc21))

    document.add(Heading("Avaliable MC20 AOD samples", level=2))
    document.add(AODList(aod_list_mc20))

    # add global sample information
    document.add(Heading("Sample Description", level=2))
    samples = SampleInfoBase(this_dir / "samples.yaml")
    document.add(SampleInfo(samples))

    # create output file
    document.write()
    print(f"Created {output_path}")


if __name__ == "__main__":
    main()
