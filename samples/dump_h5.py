import os
from os.path import join
from pathlib import Path

from sample import SampleInfoBase
from samplelist import ChangeLog, Heading, MDPage, SampleList, TextFromFile


def main():
    # get paths
    this_dir = Path(__file__).parent
    docs_dir = this_dir.parent / "docs"
    output_path = docs_dir / "algorithms" / "samples" / "h5_sample_list.md"

    # load global sample information
    samples = SampleInfoBase(this_dir / "samples.yaml")

    # collect sample-lists
    campaigns = ["mc23", "mc21", "mc20", "mc16"]
    sample_lists_pflow = {}
    sample_lists_vr = {}
    for campaign in campaigns:
        sample_lists_pflow[campaign] = []
        sample_lists_vr[campaign] = []

        for path, directories, files in os.walk(this_dir / "training_samples" / campaign):
            for file in files:
                if "pflow" in file:
                    sample_lists_pflow[campaign].append(join(path, file))
                if "vr" in file:
                    sample_lists_vr[campaign].append(join(path, file))
        sample_lists_pflow[campaign] = list(reversed(sorted(sample_lists_pflow[campaign])))
        sample_lists_vr[campaign] = list(reversed(sorted(sample_lists_vr[campaign])))

    # assemble sample-list markdown page
    document = MDPage(output_path)
    document.add(Heading("H5 Samples for Flavour Tagging Algorithms"))

    document.add(Heading("Recommended MC23 Training Samples", level=2))
    document.add(SampleList(sample_lists_pflow["mc23"][0], level=3))

    document.add(Heading("Recommended MC20 Training Samples", level=2))
    training_samples = samples.get_samples("training")
    document.add(SampleList(sample_lists_pflow["mc20"][0], level=3, selection=training_samples))
    training_samples = samples.get_samples("training")

    document.add(Heading("Recommended MC20 other samples", level=2))
    other_samples = samples.get_samples("all") - training_samples
    document.add(SampleList(sample_lists_pflow["mc20"][0], level=3, selection=other_samples))
    other_samples = samples.get_samples("all") - training_samples

    document.add(Heading("Changelog", level=2))
    document.add(ChangeLog(this_dir / "training_samples"))
    document.add(Heading("Older Training Samples", level=2))
    for sample_list in sample_lists_pflow["mc23"][1:]:
        document.add(SampleList(sample_list, level=3, infobox=True))
    for sample_list in sample_lists_pflow["mc20"][1:]:
        document.add(SampleList(sample_list, level=3, infobox=True))
    for sample_list in sample_lists_vr["mc20"][0:]:
        document.add(SampleList(sample_list, level=3, warningbox=True))
    for sample_list in sample_lists_vr["mc20"][0:]:
        document.add(SampleList(sample_list, level=3, warningbox=True, selection=other_samples))
    for sample_list in sample_lists_pflow["mc16"]:
        document.add(SampleList(sample_list, level=3, infobox=True))

    document.add(Heading("Superseded MC21 Samples", level=2))
    document.add(SampleList(sample_lists_pflow["mc21"][0], level=3, infobox=True))
    document.add(SampleList(sample_lists_vr["mc21"][0], level=3, warningbox=True))

    document.add(Heading("Release 21 Samples", level=2))
    document.add(TextFromFile(this_dir / "r21.md"))

    # create output file
    document.write()
    print(f"Created {output_path}")


if __name__ == "__main__":
    main()
