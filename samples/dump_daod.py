from os.path import join
from pathlib import Path

from samplelist import DAODList, Heading, MDObject, MDPage


def main():
    # get paths
    this_dir = Path(__file__).parent
    docs_dir = this_dir.parent / "docs"
    output_path = docs_dir / "algorithms" / "samples" / "daod_sample_list.md"

    # path to daod sample lists
    aod_list_mc23a_FTAG2 = join(this_dir / "daod" / "mc23a_FTAG2.yaml")
    aod_list_mc20d_FTAG2 = join(this_dir / "daod" / "mc20d_FTAG2.yaml")

    # assemble sample-list markdown page
    document = MDPage(output_path)
    document.add(Heading("List of DAOD Samples", level=1))
    document.add(MDObject("This page contains a list of available derivation samples for FTAG use.\n\n"))
    document.add(MDObject("Click on the links for more information about [derivations](../../samples/daod.md)\n\n"))

    document.add(Heading("Avaliable MC23a FTAG2 samples", level=2))
    document.add(DAODList(aod_list_mc23a_FTAG2))

    document.add(Heading("Avaliable MC20d FTAG2 samples", level=2))
    document.add(DAODList(aod_list_mc20d_FTAG2))

    # create output file
    document.write()
    print(f"Created {output_path}")


if __name__ == "__main__":
    main()
